-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema rhinocyt
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema rhinocyt
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `rhinocyt` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `rhinocyt` ;

-- -----------------------------------------------------
-- Table `rhinocyt`.`cell_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rhinocyt`.`cell_type` (
  `cell_type_id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`cell_type_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

INSERT INTO `cell_type` (`cell_type_id`, `type`) VALUES (1,'epithelium'),(2,'neutrophil'),(3,'eosinophil'),(4,'mastocyte'),(5,'lymphocyte'),(6,'mucipara'),(7,'other');

-- -----------------------------------------------------
-- Table `rhinocyt`.`doctor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rhinocyt`.`doctor` (
  `doctor_id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(30) NOT NULL,
  `surname` VARCHAR(30) NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `password` CHAR(128) NOT NULL,
  `twoFactorAuthenticationCode` VARCHAR(65) NULL DEFAULT NULL,
  PRIMARY KEY (`doctor_id`),
  UNIQUE INDEX `email` (`email` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `rhinocyt`.`family_anamnesis`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rhinocyt`.`family_anamnesis` (
  `anamnesis_id` INT NOT NULL AUTO_INCREMENT,
  `parents_food_allergy` TINYINT(1) NULL DEFAULT NULL,
  `parents_inhalant_allergy` TINYINT(1) NULL DEFAULT NULL,
  `parents_polyposis` TINYINT(1) NULL DEFAULT NULL,
  `sibling_polyposis` TINYINT(1) NULL DEFAULT NULL,
  `parents_asthma` TINYINT(1) NULL DEFAULT NULL,
  `silibing_asthma` TINYINT(1) NULL DEFAULT NULL,
  `silibing_food_allergy` TINYINT(1) NULL DEFAULT NULL,
  `silibing_inhalant_allergy` TINYINT(1) NULL DEFAULT NULL,
  `family_medical_notes` VARCHAR(500) NULL DEFAULT NULL,
  PRIMARY KEY (`anamnesis_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `rhinocyt`.`symptomatology`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rhinocyt`.`symptomatology` (
  `symptomatology_id` INT NOT NULL AUTO_INCREMENT,
  `obstruction` TINYINT(1) NULL DEFAULT NULL,
  `hypoacusis` TINYINT(1) NULL DEFAULT NULL,
  `rhinorrhea` TINYINT(1) NULL DEFAULT NULL,
  `sneezing` TINYINT(1) NULL DEFAULT NULL,
  `olfactory_problems` TINYINT(1) NULL DEFAULT NULL,
  `auricular_padding` TINYINT(1) NULL DEFAULT NULL,
  `tinnitus` TINYINT(1) NULL DEFAULT NULL,
  `vertiginous__syndrome` TINYINT(1) NULL DEFAULT NULL,
  `doctor_notes` VARCHAR(500) NULL DEFAULT NULL,
  `tearing` TINYINT(1) NULL DEFAULT NULL,
  `photophobia_` TINYINT(1) NULL DEFAULT NULL,
  `conjunctive_itching` TINYINT(1) NULL DEFAULT NULL,
  `conjunctiva_burning` TINYINT(1) NULL DEFAULT NULL,
  `nasal_itching` TINYINT(1) NULL DEFAULT NULL,
  `medicine_use` TINYINT(1) NULL DEFAULT NULL,
  `fever` TINYINT(1) NULL DEFAULT NULL,
  PRIMARY KEY (`symptomatology_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `rhinocyt`.`prick_test`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rhinocyt`.`prick_test` (
  `prick_test_id` INT NOT NULL AUTO_INCREMENT,
  `positive` TINYINT(1) NULL DEFAULT NULL,
  `perennial_allergen` TINYINT(1) NULL DEFAULT NULL,
  `poplar` TINYINT(1) NULL DEFAULT NULL,
  `hazel` TINYINT(1) NULL DEFAULT NULL,
  `common_ash` TINYINT(1) NULL DEFAULT NULL,
  `willow` TINYINT(1) NULL DEFAULT NULL,
  `alder` TINYINT(1) NULL DEFAULT NULL,
  `cupressacee` TINYINT(1) NULL DEFAULT NULL,
  `oak_tree` TINYINT(1) NULL DEFAULT NULL,
  `black_hornbeam` TINYINT(1) NULL DEFAULT NULL,
  `plane_tree` TINYINT(1) NULL DEFAULT NULL,
  `grasses` TINYINT(1) NULL DEFAULT NULL,
  `flowering_ash` TINYINT(1) NULL DEFAULT NULL,
  `pinaceae` TINYINT(1) NULL DEFAULT NULL,
  `buckwheat` TINYINT(1) NULL DEFAULT NULL,
  `urticaceae` TINYINT(1) NULL DEFAULT NULL,
  `plantain` TINYINT(1) NULL DEFAULT NULL,
  `birch` TINYINT(1) NULL DEFAULT NULL,
  `chestnut` TINYINT(1) NULL DEFAULT NULL,
  `absinthe` TINYINT(1) NULL DEFAULT NULL,
  PRIMARY KEY (`prick_test_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `rhinocyt`.`medical_exam`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rhinocyt`.`medical_exam` (
  `medical_exam_id` INT NOT NULL AUTO_INCREMENT,
  `nasal_pyramid` TINYINT(1) NULL DEFAULT NULL,
  `nasal_valve` TINYINT(1) NULL DEFAULT NULL,
  `nasal_septum` TINYINT(1) NULL DEFAULT NULL,
  `turbinates` TINYINT(1) NULL DEFAULT NULL,
  `nasal_polyposis_sx` TINYINT(1) NULL DEFAULT NULL,
  `nasal_polyposis_dx` TINYINT(1) NULL DEFAULT NULL,
  `exudate` TINYINT(1) NULL DEFAULT NULL,
  `adenoid_hypertrophy` TINYINT(1) NULL DEFAULT NULL,
  `alteration_notes` VARCHAR(500) NULL DEFAULT NULL,
  `ear_exam` VARCHAR(500) NULL DEFAULT NULL,
  `rino_base_sx` FLOAT NULL DEFAULT NULL,
  `rino_base_dx` FLOAT NULL DEFAULT NULL,
  `rino_base_sx_dx` FLOAT NULL DEFAULT NULL,
  `decong_base_sx` FLOAT NULL DEFAULT NULL,
  `decong_base_dx` FLOAT NULL DEFAULT NULL,
  `decong_base_sx_dx` FLOAT NULL DEFAULT NULL,
  `conclusions` VARCHAR(500) NULL DEFAULT NULL,
  PRIMARY KEY (`medical_exam_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `rhinocyt`.`patient`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rhinocyt`.`patient` (
  `fiscal_code` VARCHAR(16) NOT NULL,
  `name` VARCHAR(30) NOT NULL,
  `surname` VARCHAR(30) NOT NULL,
  `gender` CHAR(1) NOT NULL,
  `birth_date` DATE NOT NULL,
  `anamnesis_id` INT NULL DEFAULT NULL,
  `doctor_id` INT NOT NULL,
  `symptomatology_id` INT NULL DEFAULT NULL,
  `prick_test_id` INT NULL DEFAULT NULL,
  `medical_exam_id` INT NULL DEFAULT NULL,
  PRIMARY KEY (`fiscal_code`),
  INDEX `patient_ibfk_1` (`anamnesis_id` ASC) VISIBLE,
  INDEX `patient_ibfk_2` (`doctor_id` ASC) VISIBLE,
  INDEX `patient_ibfk_3` (`symptomatology_id` ASC) VISIBLE,
  INDEX `patient_ibfk_4` (`prick_test_id` ASC) VISIBLE,
  INDEX `patient_ibfk_5` (`medical_exam_id` ASC) VISIBLE,
  CONSTRAINT `patient_ibfk_1`
    FOREIGN KEY (`anamnesis_id`)
    REFERENCES `rhinocyt`.`family_anamnesis` (`anamnesis_id`),
  CONSTRAINT `patient_ibfk_2`
    FOREIGN KEY (`doctor_id`)
    REFERENCES `rhinocyt`.`doctor` (`doctor_id`),
  CONSTRAINT `patient_ibfk_3`
    FOREIGN KEY (`symptomatology_id`)
    REFERENCES `rhinocyt`.`symptomatology` (`symptomatology_id`),
  CONSTRAINT `patient_ibfk_4`
    FOREIGN KEY (`prick_test_id`)
    REFERENCES `rhinocyt`.`prick_test` (`prick_test_id`),
  CONSTRAINT `patient_ibfk_5`
    FOREIGN KEY (`medical_exam_id`)
    REFERENCES `rhinocyt`.`medical_exam` (`medical_exam_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `rhinocyt`.`exam`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rhinocyt`.`exam` (
  `exam_id` INT NOT NULL AUTO_INCREMENT,
  `exam_date` DATE NOT NULL,
  `diagnosis` VARCHAR(500) NULL DEFAULT NULL,
  `doctor_id` INT NOT NULL,
  `fiscal_code` VARCHAR(16) NULL DEFAULT NULL,
  PRIMARY KEY (`exam_id`),
  INDEX `fiscal_code` (`fiscal_code` ASC) VISIBLE,
  INDEX `exam_ibfk_1` (`doctor_id` ASC) VISIBLE,
  CONSTRAINT `exam_ibfk_1`
    FOREIGN KEY (`doctor_id`)
    REFERENCES `rhinocyt`.`doctor` (`doctor_id`),
  CONSTRAINT `exam_ibfk_2`
    FOREIGN KEY (`fiscal_code`)
    REFERENCES `rhinocyt`.`patient` (`fiscal_code`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `rhinocyt`.`field_exam`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rhinocyt`.`field_exam` (
  `field_id` INT NOT NULL AUTO_INCREMENT,
  `photo` VARCHAR(400) NOT NULL,
  `exam_id` INT NOT NULL,
  PRIMARY KEY (`field_id`),
  INDEX `field_exam_ibfk_1` (`exam_id` ASC) VISIBLE,
  CONSTRAINT `field_exam_ibfk_1`
    FOREIGN KEY (`exam_id`)
    REFERENCES `rhinocyt`.`exam` (`exam_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `rhinocyt`.`cells`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `rhinocyt`.`cells` (
  `cell_id` INT NOT NULL AUTO_INCREMENT,
  `cell_photo` VARCHAR(400) NOT NULL,
  `cell_type_id` INT NOT NULL,
  `field_id` INT NOT NULL,
  PRIMARY KEY (`cell_id`),
  INDEX `cells_ibfk_1_idx` (`field_id` ASC) VISIBLE,
  INDEX `cells_ibfk_1_idx1` (`cell_type_id` ASC) VISIBLE,
  CONSTRAINT `cells_ibfk_1`
    FOREIGN KEY (`field_id`)
    REFERENCES `rhinocyt`.`field_exam` (`field_id`),
  CONSTRAINT `cells_ibfk_2`
    FOREIGN KEY (`cell_type_id`)
    REFERENCES `rhinocyt`.`cell_type` (`cell_type_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
