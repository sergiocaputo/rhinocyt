import {Request, Response} from 'express';
import {getConnection, EntityManager, getManager} from 'typeorm';
import {Exam} from '../entities/Exam';
import RequestWithUser from '../interfaces/RequestWithUser.interface';
import { Patient } from '../entities/Patient';


export const getPatientExams = async (req: RequestWithUser, res: Response) => {
    try{
        const examRepository =  getConnection().getRepository(Exam);

        if(req.query.fiscalcode == null){
            // return all the exams of the doctor's patients
            const exams = await examRepository.find( {select: ['examId','fiscalCode','examDate','diagnosis'],
            where: {doctorId : req.user.doctorId}}).catch((err)=> {console.log(err);});

            if(JSON.stringify(exams) === '[]') res.status(200).json({ ' message ' : 'empty set'});
            else res.status(200).json(exams);
        }
        else{
            // return only the exams of the patient with fiscalcode given
            const exams = await examRepository.find( {select: ['examId','examDate','diagnosis'],
            where: {doctorId : req.user.doctorId, fiscalCode: req.query.fiscalcode}}).catch((err)=> {console.log(err);});

            if(JSON.stringify(exams) === '[]') res.status(200).json({ ' message ' : 'empty set for exams'});
            else res.status(200).json(exams);
        }
    }
    catch(error) {
        res.status(403).json({ ' message ': 'error' });
    }
}

export const searchExams = async (req: RequestWithUser, res: Response) => {
    try{
        const exams =  await getManager().createQueryBuilder(Exam,'exam')
        .select(['exam.fiscalCode','exam.examId','exam.examDate','exam.diagnosis'])
        .innerJoin('exam.fiscalCode2','patient')
        .where(`exam.doctorId = ${req.user.doctorId}`)
        .andWhere(`patient.name = "${req.query.name}"`)
        .andWhere(`patient.surname = "${req.query.surname}"`)
        .getMany();

        if(JSON.stringify(exams) === '[]') res.status(200).json({ ' message ' : 'no exams found'});
        else res.status(200).json(exams);
    }
    catch(error) {
        console.log(error)
        res.status(403).json({ ' message ': 'error' });
    }
}

export const newExam = async (req: RequestWithUser, res: Response) => {
    try{
        const examRepository =  getConnection().getRepository(Exam);
        const patientRepository = getConnection().getRepository(Patient);

        // check if patient is a real patient of doctorId and if the fiscalcode is registered as patient
        if(req.body.fiscalCode != null) {
            const result = await patientRepository.find({select: ['doctorId'],where:{fiscalCode: req.body.fiscalCode}}).catch((err)=> {
                console.log(err);});
            if(JSON.stringify(result)=== '[]')  return res.status(403).json( {' message ': 'fiscal code not registered'});
            if(result[0].doctorId != req.user.doctorId) return res.status(403).json({ ' message ' : 'is not allowed to save other patient\'s exams'});
        }

        const exam = await examRepository.create(
            {examDate: req.body.examDate,
             diagnosis: req.body.diagnosis,
             doctorId: req.user.doctorId,
             fiscalCode: req.body.fiscalCode
            });
        await examRepository.save(exam).catch((err)=> {
            console.log(err);
            return res.status(403).json( {' message ': 'wrong data sent'})
        });

        return res.status(200).json( {' message ': 'new exam added'})
    }
    catch(error) {
        res.status(403).json({ ' message ': 'error' });
    }
}

export const deleteExam = async (req: RequestWithUser, res: Response) => {
    try{
        const examRepository =  getConnection().getRepository(Exam);
        const examid :number= +req.query.examid;

        const result = await examRepository.find({select: ['doctorId'],where:{examId: examid}}).catch((err)=> {console.log(err);});
        if(JSON.stringify(result) === '[]') return res.status(403).json( {' message ': 'exam doesn\'t exist'});
        if(result[0].doctorId != req.user.doctorId) return res.status(403).json({ ' message ' : 'deleting other patient\'s exams is not allowed'});
        const results = await examRepository.delete({examId: examid, doctorId: req.user.doctorId}).catch((err)=> {console.log(err);});

        // check if exam was deleted
        if(Object(results).affected > 0) return res.status(200).json({' message ': 'exam deleted'});
        else return res.status(403).json({' message ': 'error'})
    }
    catch(error) {
        res.status(403).json({ ' message ': 'error' });
    }
}
