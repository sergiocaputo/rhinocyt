"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteCellType = exports.newCellType = exports.getCellTypes = void 0;
var typeorm_1 = require("typeorm");
var CellType_1 = require("../entities/CellType");
exports.getCellTypes = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var celltype, cellTypesRepository, types, types, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 5, , 6]);
                celltype = +req.query.celltypeid;
                cellTypesRepository = typeorm_1.getConnection().getRepository(CellType_1.CellType);
                if (!(celltype.toString() == NaN.toString())) return [3 /*break*/, 2];
                return [4 /*yield*/, cellTypesRepository.find({ select: ['cellTypeId', 'type'] })
                        .catch(function (err) { console.log(err); })];
            case 1:
                types = _a.sent();
                if (JSON.stringify(types) === '[]')
                    res.status(403).json({ 'message ': 'cell types not registered yet' });
                else
                    res.status(200).json(types);
                return [3 /*break*/, 4];
            case 2: return [4 /*yield*/, cellTypesRepository.find({ select: ['cellTypeId', 'type'],
                    where: { cellTypeId: celltype } }).catch(function (err) { console.log(err); })];
            case 3:
                types = _a.sent();
                if (JSON.stringify(types) === '[]')
                    res.status(403).json({ 'message ': 'cellTypeId not registered' });
                else
                    res.status(200).json(types);
                _a.label = 4;
            case 4: return [3 /*break*/, 6];
            case 5:
                error_1 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}); };
exports.newCellType = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var celltype, cellTypesRepository, result, type, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 6, , 7]);
                celltype = req.body.type;
                cellTypesRepository = typeorm_1.getConnection().getRepository(CellType_1.CellType);
                if (!(celltype != null)) return [3 /*break*/, 5];
                return [4 /*yield*/, cellTypesRepository.find({ select: ['cellTypeId', 'type'],
                        where: { type: celltype } }).catch(function (err) { console.log(err); })];
            case 1:
                result = _a.sent();
                if (!(JSON.stringify(result) === '[]')) return [3 /*break*/, 4];
                return [4 /*yield*/, cellTypesRepository.create({
                        type: celltype,
                    })];
            case 2:
                type = _a.sent();
                return [4 /*yield*/, cellTypesRepository.save(type).catch(function (err) {
                        console.log(err);
                        return res.status(403).json({ 'message ': 'wrong data sent' });
                    })];
            case 3:
                _a.sent();
                return [2 /*return*/, res.status(200).json({ 'message ': 'new cell type saved' })];
            case 4: return [2 /*return*/, res.status(403).json({ 'message ': 'cell type already saved' })];
            case 5: return [2 /*return*/, res.status(403).json({ 'message ': 'cell type not provided' })];
            case 6:
                error_2 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 7];
            case 7: return [2 /*return*/];
        }
    });
}); };
exports.deleteCellType = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var celltypeid, cellTypesRepository, result, results, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 4]);
                celltypeid = +req.query.celltypeid;
                cellTypesRepository = typeorm_1.getConnection().getRepository(CellType_1.CellType);
                if (celltypeid.toString() == NaN.toString())
                    return [2 /*return*/, res.status(403).json({ 'message ': 'cell type not provided' })];
                return [4 /*yield*/, cellTypesRepository.find({ select: ['cellTypeId', 'type'],
                        where: { cellTypeId: celltypeid } }).catch(function (err) { console.log(err); })];
            case 1:
                result = _a.sent();
                if (JSON.stringify(result) === '[]')
                    return [2 /*return*/, res.status(403).json({ 'message ': "celltype doesn't exist" })];
                return [4 /*yield*/, cellTypesRepository.delete({ cellTypeId: celltypeid }).catch(function (err) { console.log(err); })];
            case 2:
                results = _a.sent();
                // check if celltype was deleted
                if (Object(results).affected > 0)
                    return [2 /*return*/, res.status(200).json({ 'message ': 'celltype deleted' })];
                else
                    return [2 /*return*/, res.status(403).json({ 'message ': 'error' })];
                return [3 /*break*/, 4];
            case 3:
                error_3 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
