import {Request, Response} from 'express';
import {getConnection, EntityManager, getManager, createQueryBuilder} from 'typeorm';
import {FieldExam} from '../entities/FieldExam';
import RequestWithUser from '../interfaces/RequestWithUser.interface';
import { Cells } from '../entities/Cells';
import { Exam } from '../entities/Exam';
import { selectCellType } from '../controllers/fieldexam';
import { CellType } from '../entities/CellType';
const fs = require('fs');

export const getCells = async (req: RequestWithUser, res: Response) => {
    try{
        const fieldid :number= +req.query.fieldid;

        if(fieldid.toString() != NaN.toString()){
            // check if exam's fieldId is from doctorId's patient
            const result = await getManager().createQueryBuilder(FieldExam,'fieldexam')
            .select(['fieldexam.fieldId','exam.doctorId'])
            .innerJoin('fieldexam.exam','exam')
            .where(`fieldexam.fieldId = '${fieldid}'`)
            .getMany().catch((err)=> {console.log(err);});

            if(JSON.stringify(result) === '[]') return res.status(403).json({ 'message ' : 'field not registered'});
            if(result[0].exam.doctorId != req.user.doctorId) return res.status(403).json({ 'message ' : `is not allowed to view other patient's field cells`});         

            // return the cells of the fieldid given 
            const cells = await getManager().createQueryBuilder(Cells,'cells')
            .select(['cells.cellId','cells.cellPhoto','celltype.type'])
            .innerJoin('cells.cellType','celltype')
            .where(`cells.fieldId = '${fieldid}'`)
            .getMany().catch((err)=> {console.log(err);});

            if(JSON.stringify(cells) === '[]') res.status(200).json({ 'message ' : `field doesn't have cells`});
            else res.status(200).json(cells);
        }
        else{
            return res.status(403).json({ 'message ' : 'fieldid not provided'});
        }
    }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}

export const getTable = async (req: RequestWithUser, res: Response) => {
    try{
        const examid :number= +req.query.examid;

        // check if exam's fieldId is from doctorId's patient
        if(examid.toString() != NaN.toString()) {
            const result = await getManager().createQueryBuilder(FieldExam,'fieldexam')
            .select(['fieldexam.fieldId','exam.doctorId'])
            .innerJoin('fieldexam.exam','exam')
            .where(`fieldexam.examId = '${examid}'`)
            .getMany().catch((err)=> {console.log(err);});

            if(JSON.stringify(result) === '[]') return res.status(403).json({ 'message ' : 'exam not registered'});
            if(result[0].exam.doctorId != req.user.doctorId) return res.status(403).json({ 'message ' : `is not allowed to view other patient's field cells`});

            // count for each cell type how many cells there are in the exam's fields provided
            const cells =  getManager().createQueryBuilder(Cells,'cells')
            .select(['COUNT (celltype.type) AS cellNumber','celltype.type','cells.fieldId']);
            cells.innerJoin('cells.cellType','celltype');
            let first = false;
            Object(result).forEach(element => {
                if(first == false) {
                    cells.where(`cells.fieldId = '${element.fieldId}'`);
                    first = true;
                }
                else cells.orWhere(`cells.fieldId = '${element.fieldId}'`);
            });
            cells.groupBy('celltype.type');

            const results = await cells.getRawMany().catch((err)=> {console.log(err);});
            if(JSON.stringify(results) === '[]') res.status(200).json({ 'message ' : `field doesn't have cells`});
            else return res.status(200).json( {results});
        }
        else{
            return res.status(403).json( {'message ': 'fieldid not provided'})
        }
    }
    catch(error) {console.log(error)
        res.status(403).json({ 'message ': 'error' });
    }
}

/*
export const newCells = async (req: RequestWithUser, res: Response) => {
    try{
        const cellsRepository =  getConnection().getRepository(Cells);
        const fieldid :number= +req.body.fieldId;

        //check if exam's fieldId is from doctorId's patient
        if(fieldid.toString() != NaN.toString()) {
            const result = await getManager().createQueryBuilder(FieldExam,'fieldexam')
            .select(['fieldexam.fieldId','exam.doctorId'])
            .innerJoin('fieldexam.exam','exam')
            .where(`fieldexam.fieldId = '${fieldid}'`)
            .getMany().catch((err)=> {console.log(err);});

            if(JSON.stringify(result) === '[]') return res.status(403).json({ 'message' : 'field not registered'});
            if(result[0].exam.doctorId != req.user.doctorId)
                return res.status(403).json({ 'message' : `is not allowed to save other patient's field cells`});

            //new cells saved

            //photos sent with array
            //cells type sent with array
            if(req.body.photo.length != req.body.cellType.length)
                return res.status(403).json({ 'error' : 'cell photo number is different from cell type number'});

            //check if cell types given in input are saved as cell types
            const cellTypesRepository =  getConnection().getRepository(CellType);
            for(let j = 0; j < req.body.cellType.length; j++){
                const result = await cellTypesRepository.find( {select: ['cellTypeId','type'],
                where: {type : req.body.cellType[j]}}).catch((err)=> {console.log(err);});
                if(JSON.stringify(result) === '[]')
                    return res.status(403).json(
                         {'error': `cell type ${req.body.cellType[j]} is not saved as cell type, save it before save cells with this type `})
            }

            let celltype  = await selectCellType(req.body.cellType);

            const photos = req.body.photo;  //['imm\\cell1.jpg','imm\\cell2.jpg']

            for(let i = 0; i < photos.length; i++){
                const cell =  cellsRepository.create({
                    cellPhoto : req.body.photo[i],
                    cellTypeId: celltype[i],
                    fieldId: fieldid,
                });
                await cellsRepository.save(cell).catch((err)=> {
                    console.log(err);
                    return res.status(200).json( {'message': 'wrong data sent'})
                });
            };
            return res.status(200).json( {'message': 'new cells saved'})
        }
        else{
            return res.status(200).json( {'message': 'fieldid not provided'})
        }
    }
    catch(error) {
        res.status(403).json({ 'message': 'error' });
    }
}
*/
export const deleteCell = async (req: RequestWithUser, res: Response) => {
    try{
        const cellid :number= +req.query.cellid;

        const fieldquery = await getManager().createQueryBuilder(Cells,'cells')
        .select(['cells.cellId','fieldexam.fieldId','cells.cellPhoto'])
        .innerJoin('cells.field','fieldexam')
        .where(`cells.cellId = '${cellid}'`)
        .getMany().catch((err)=> {console.log(err);});

        if(JSON.stringify(fieldquery) === '[]') return res.status(403).json( {'message ': `cell provided doesn't exist`});

        const fieldid = fieldquery[0].field.fieldId;
        const result = await getManager().createQueryBuilder(FieldExam,'fieldexam')
        .select(['fieldexam.fieldId','exam.doctorId'])
        .innerJoin('fieldexam.exam','exam')
        .where(`fieldexam.fieldId = '${fieldid}'`)
        .getMany().catch((err)=> {console.log(err);});

        if(result[0].exam.doctorId != req.user.doctorId) return res.status(403).json({ 'message ' : `deleting other patient's cells is not allowed`});
        const cellsRepository =  getConnection().getRepository(Cells);
        const results = await cellsRepository.delete({cellId: cellid}).catch((err)=> {console.log(err);});

        // cancel file
        const file = fieldquery[0].cellPhoto;
        fs.unlinkSync(file,(err) => {
            if (err) throw err;
          });

        // check if field was deleted
        if(Object(results).affected > 0) return res.status(200).json({'message ': 'cell deleted'});
        else return res.status(403).json({'message ': 'error'});
    }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}


export const changeCellClass = async (req: RequestWithUser, res: Response) => {
    try{
        const cellid :number= +req.body.cellId;

        const fieldquery = await getManager().createQueryBuilder(Cells,'cells')
        .select(['cells.cellId','fieldexam.fieldId'])
        .innerJoin('cells.field','fieldexam')
        .where(`cells.cellId = '${cellid}'`)
        .getMany().catch((err)=> {console.log(err);});

        if(JSON.stringify(fieldquery) === '[]') return res.status(403).json( {'message ': `cell provided doesn't exist`});

        const fieldid = fieldquery[0].field.fieldId;
        const result = await getManager().createQueryBuilder(FieldExam,'fieldexam')
        .select(['fieldexam.fieldId','exam.doctorId','fieldexam.examId'])
        .innerJoin('fieldexam.exam','exam')
        .where(`fieldexam.fieldId = '${fieldid}'`)
        .getMany().catch((err)=> {console.log(err);});

        if(result[0].exam.doctorId != req.user.doctorId) return res.status(403).json({ 'message ' : `changing other patient's cells type is not allowed`});

        const celltype = req.body.cellType;
        const cellTypeRepository = getConnection().getRepository(CellType);
        const results = await cellTypeRepository.find({select: ['cellTypeId'],where:{type: celltype}}).catch((err)=> {console.log(err);});
        if(JSON.stringify(results) === '[]')  return res.status(403).json( {'message ': `celltype given doesn't exist`});

        const cellsRepository = getConnection().getRepository(Cells);
        const cell_type = selectCellType(celltype);

        const cell = await cellsRepository.find({select: ['cellPhoto'],where:{cellId: cellid}}).catch((err)=> {console.log(err);});
        const file = cell[0].cellPhoto.split('/')[7];
        const newpath = `./src/services/cell_extraction/cells/cell_examid_${result[0].examId}/${celltype}/${file}`;

        await cellsRepository.update({cellId: cellid}, {cellTypeId: cell_type, cellPhoto: newpath});

        fs.rename(cell[0].cellPhoto, newpath, function(err){
            if(err) console.log(err);
        })

        return res.status(200).json({'message ': 'cell type updated'});
    }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}
