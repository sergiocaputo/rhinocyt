"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deletePatient = exports.newPatient = exports.searchPatient = exports.getPatientDetails = exports.getPatientByDoctorId = void 0;
var typeorm_1 = require("typeorm");
var Patient_1 = require("../entities/Patient");
exports.getPatientByDoctorId = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var patientRepository, patients, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                patientRepository = typeorm_1.getConnection().getRepository(Patient_1.Patient);
                return [4 /*yield*/, patientRepository.find({ select: ['name', 'surname', 'fiscalCode'],
                        where: { doctorId: req.user.doctorId } }).catch(function (err) { console.log(err); })];
            case 1:
                patients = _a.sent();
                if (JSON.stringify(patients) === '[]')
                    res.status(200).json({ 'message ': 'empty set' });
                else
                    res.status(200).json(patients);
                return [3 /*break*/, 3];
            case 2:
                error_1 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.getPatientDetails = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var patientRepository, patient, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                patientRepository = typeorm_1.getConnection().getRepository(Patient_1.Patient);
                return [4 /*yield*/, patientRepository.find({ select: ['fiscalCode', 'name', 'surname', 'gender', 'birthDate'],
                        where: { fiscalCode: req.query.fiscalcode, doctorId: req.user.doctorId } }).catch(function (err) { console.log(err); })];
            case 1:
                patient = _a.sent();
                if (JSON.stringify(patient) === '[]')
                    res.status(200).json({ 'message ': 'patient doesn\'t exist or has another doctor' });
                else
                    res.status(200).json(patient);
                return [3 /*break*/, 3];
            case 2:
                error_2 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.searchPatient = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var patientRepository, patient, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                patientRepository = typeorm_1.getConnection().getRepository(Patient_1.Patient);
                return [4 /*yield*/, patientRepository.find({ select: ['fiscalCode', 'name', 'surname', 'gender', 'birthDate'],
                        where: { name: req.query.name, surname: req.query.surname, doctorId: req.user.doctorId } }).catch(function (err) { console.log(err); })];
            case 1:
                patient = _a.sent();
                if (JSON.stringify(patient) === '[]')
                    res.status(403).json({ 'message ': 'patient doesn\'t exist' });
                else
                    res.status(200).json(patient);
                return [3 /*break*/, 3];
            case 2:
                error_3 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.newPatient = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var fiscalcodelenght, regularlenght, patientRepository, patient, new_patient, results, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 5, , 6]);
                fiscalcodelenght = JSON.stringify(req.body.fiscalCode).length - 2;
                regularlenght = 16;
                if (fiscalcodelenght != regularlenght)
                    return [2 /*return*/, res.status(403).json({ 'message ': 'fiscal code lenght error' })];
                patientRepository = typeorm_1.getConnection().getRepository(Patient_1.Patient);
                return [4 /*yield*/, patientRepository.find({ select: ['fiscalCode'],
                        where: { fiscalCode: req.body.fiscalCode } }).catch(function (err) { console.log(err); })];
            case 1:
                patient = _a.sent();
                if (JSON.stringify(patient) != '[]')
                    return [2 /*return*/, res.status(403).json({ 'message ': 'patient already registered' })];
                return [4 /*yield*/, patientRepository.create({ doctorId: req.user.doctorId,
                        fiscalCode: req.body.fiscalCode,
                        name: req.body.name,
                        surname: req.body.surname,
                        gender: req.body.gender,
                        birthDate: req.body.birthDate
                    })];
            case 2:
                new_patient = _a.sent();
                return [4 /*yield*/, patientRepository.save(new_patient).catch(function (err) {
                        console.log(err);
                        throw err;
                    })];
            case 3:
                _a.sent();
                return [4 /*yield*/, patientRepository.find({ select: ['fiscalCode'],
                        where: { fiscalCode: req.body.fiscalCode, doctorId: req.user.doctorId } }).catch(function (err) { console.log(err); })];
            case 4:
                results = _a.sent();
                return [2 /*return*/, res.status(200).json({ 'message ': 'new patient added', results: results })];
            case 5:
                error_4 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}); };
exports.deletePatient = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var patientRepository, result, results, error_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 4]);
                patientRepository = typeorm_1.getConnection().getRepository(Patient_1.Patient);
                return [4 /*yield*/, patientRepository.find({ select: ['doctorId'], where: { fiscalCode: req.query.fiscalcode } }).catch(function (err) {
                        console.log(err);
                    })];
            case 1:
                result = _a.sent();
                if (JSON.stringify(result) === '[]')
                    return [2 /*return*/, res.status(403).json({ 'message ': 'patient doesn\'t exist' })];
                if (result[0].doctorId != req.user.doctorId)
                    return [2 /*return*/, res.status(403).json({ 'message ': 'deleting other doctors\' patients is not allowed' })];
                return [4 /*yield*/, patientRepository.delete({ fiscalCode: (req.query.fiscalcode).toString(), doctorId: req.user.doctorId }).catch(function (err) {
                        console.log(err);
                    })];
            case 2:
                results = _a.sent();
                // check if patient was deleted
                if (Object(results).affected > 0)
                    return [2 /*return*/, res.status(200).json({ 'message ': 'patient deleted' })];
                else
                    return [2 /*return*/, res.status(200).json({ 'message ': 'error' })];
                return [3 /*break*/, 4];
            case 3:
                error_5 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
