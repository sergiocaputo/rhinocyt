"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.logout = exports.newDoctor = exports.login = exports.updateDoctor = exports.getDoctorById = void 0;
var typeorm_1 = require("typeorm");
var Doctor_1 = require("../entities/Doctor");
var bcrypt = require("bcrypt");
var authentication_service_1 = require("../services/authentication.service");
exports.getDoctorById = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var doctorRepository, results, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                doctorRepository = typeorm_1.getConnection().getRepository(Doctor_1.Doctor);
                return [4 /*yield*/, doctorRepository.find({ select: ['name', 'surname', 'email'],
                        where: { doctorId: req.params.iddoctor } }).catch(function (err) { console.log(err); })];
            case 1:
                results = _a.sent();
                if (JSON.stringify(results) === '[]')
                    res.status(200).json({ 'message ': 'empty set' });
                else
                    res.status(200).json(results);
                return [3 /*break*/, 3];
            case 2:
                error_1 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.updateDoctor = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var doctorRepository, hashed_password, results, results, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 9, , 10]);
                doctorRepository = typeorm_1.getConnection().getRepository(Doctor_1.Doctor);
                if (!(req.body.password != null)) return [3 /*break*/, 4];
                return [4 /*yield*/, bcrypt.hash(req.body.password, 10)];
            case 1:
                hashed_password = _a.sent();
                return [4 /*yield*/, doctorRepository.update({ doctorId: req.user.doctorId }, { password: hashed_password })];
            case 2:
                _a.sent();
                return [4 /*yield*/, doctorRepository.findOne(req.user.doctorId)];
            case 3:
                results = _a.sent();
                if (JSON.stringify(results) === '[]')
                    res.status(200).json({ 'message ': 'empty set' });
                return [2 /*return*/, res.status(200).json({ results: results, 'message ': 'password updated' })];
            case 4:
                if (!(req.body.email != null)) return [3 /*break*/, 7];
                return [4 /*yield*/, doctorRepository.update({ doctorId: req.user.doctorId }, { email: req.body.email })];
            case 5:
                _a.sent();
                return [4 /*yield*/, doctorRepository.findOne(req.user.doctorId)];
            case 6:
                results = _a.sent();
                if (JSON.stringify(results) === '[]')
                    res.status(200).json({ 'message ': 'empty set' });
                return [2 /*return*/, res.status(200).json({ results: results, 'message ': 'email updated' })];
            case 7: return [2 /*return*/, res.status(403).json({ 'message ': 'wrong parameters given' })];
            case 8: return [3 /*break*/, 10];
            case 9:
                error_2 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 10];
            case 10: return [2 /*return*/];
        }
    });
}); };
exports.login = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var doctorRepository, results, user, password, tokenData, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 5, , 6]);
                doctorRepository = typeorm_1.getConnection().getRepository(Doctor_1.Doctor);
                return [4 /*yield*/, doctorRepository.find({ select: ['doctorId', 'email', 'password'],
                        where: { email: req.body.email } }).catch(function (err) { console.log(err); })];
            case 1:
                results = _a.sent();
                if (!(JSON.stringify(results) === '[]')) return [3 /*break*/, 2];
                res.status(403).json({ 'message ': 'user not registered' });
                return [3 /*break*/, 4];
            case 2:
                user = results[0].doctorId;
                password = results[0].password;
                return [4 /*yield*/, bcrypt.compare(req.body.password, password)];
            case 3:
                if (!(_a.sent()))
                    res.status(403).json({ 'message ': 'incorrect password' });
                else {
                    tokenData = authentication_service_1.createToken(user);
                    res.setHeader('Set-Cookie', [authentication_service_1.createCookie(tokenData)]);
                    console.log('New session started by doctor with id ' + user);
                    return [2 /*return*/, res.status(200).json({ 'message ': 'doctor logged in' })];
                }
                _a.label = 4;
            case 4: return [3 /*break*/, 6];
            case 5:
                error_3 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}); };
exports.newDoctor = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var doctorRepository, doctor, hashed_password, new_doctor, results, doctorId, user, tokenData, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 6, , 7]);
                doctorRepository = typeorm_1.getConnection().getRepository(Doctor_1.Doctor);
                return [4 /*yield*/, doctorRepository.find({ select: ['email'],
                        where: { email: req.body.email } }).catch(function (err) { console.log(err); })];
            case 1:
                doctor = _a.sent();
                if (JSON.stringify(doctor) != '[]')
                    return [2 /*return*/, res.status(403).json({ 'message ': 'doctor already registered' })];
                return [4 /*yield*/, bcrypt.hash(req.body.password, 10)];
            case 2:
                hashed_password = _a.sent();
                req.body.password = hashed_password;
                return [4 /*yield*/, doctorRepository.create(req.body)];
            case 3:
                new_doctor = _a.sent();
                return [4 /*yield*/, doctorRepository.save(new_doctor).catch(function (err) {
                        console.log(err);
                    })];
            case 4:
                results = _a.sent();
                return [4 /*yield*/, doctorRepository.find({ select: ['doctorId'],
                        where: { email: req.body.email, password: hashed_password } }).catch(function (err) { console.log(err); })];
            case 5:
                doctorId = _a.sent();
                user = doctorId[0].doctorId;
                tokenData = authentication_service_1.createToken(user);
                res.setHeader('Set-Cookie', [authentication_service_1.createCookie(tokenData)]);
                console.log('New session started by doctor with id ' + user);
                return [2 /*return*/, res.status(200).send(results)];
            case 6:
                error_4 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 7];
            case 7: return [2 /*return*/];
        }
    });
}); };
exports.logout = function (req, res) {
    console.log('Session closed by doctor with id ' + req.user.doctorId);
    res.setHeader('Set-Cookie', ['Authorization=;Max-age=0']);
    res.status(200).json({ 'message ': 'logged out' });
};
