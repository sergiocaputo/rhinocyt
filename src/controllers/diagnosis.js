"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateDiagnosis = exports.downloadReport = exports.deleteDiagnosis = exports.newDiagnosis = exports.getDiagnosis = void 0;
var typeorm_1 = require("typeorm");
var Cells_1 = require("../entities/Cells");
var Exam_1 = require("../entities/Exam");
var FieldExam_1 = require("../entities/FieldExam");
var Patient_1 = require("../entities/Patient");
var fs = require('fs');
exports.getDiagnosis = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var examRepository, exams, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                examRepository = typeorm_1.getConnection().getRepository(Exam_1.Exam);
                return [4 /*yield*/, examRepository.find({ select: ['examId', 'fiscalCode', 'examDate', 'diagnosis'],
                        where: { doctorId: req.user.doctorId, fiscalCode: req.query.fiscalcode, examDate: req.query.examdate } }).catch(function (err) {
                        console.log(err);
                    })];
            case 1:
                exams = _a.sent();
                if (JSON.stringify(exams) === '[]')
                    res.status(200).json({ 'message ': 'empty set for diagnosis' });
                else
                    res.status(200).json(exams);
                return [3 /*break*/, 3];
            case 2:
                error_1 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.newDiagnosis = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var examRepository, result, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 4]);
                examRepository = typeorm_1.getConnection().getRepository(Exam_1.Exam);
                return [4 /*yield*/, examRepository.find({ select: ['doctorId'], where: { examId: req.body.examId } }).catch(function (err) { console.log(err); })];
            case 1:
                result = _a.sent();
                if (JSON.stringify(result) === '[]')
                    return [2 /*return*/, res.status(403).json({ 'message ': "exam doesn't exist" })];
                if (result[0].doctorId != req.user.doctorId)
                    return [2 /*return*/, res.status(403).json({ 'message ': " saving other patient's diagnosis is not allowed" })];
                return [4 /*yield*/, examRepository.update({ doctorId: req.user.doctorId, examId: req.body.examId }, { diagnosis: req.body.diagnosis })];
            case 2:
                _a.sent();
                return [2 /*return*/, res.status(200).json({ 'message ': 'new diagnosis added' })];
            case 3:
                error_2 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.deleteDiagnosis = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var examRepository, examid, result, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 4]);
                examRepository = typeorm_1.getConnection().getRepository(Exam_1.Exam);
                examid = +req.query.examid;
                return [4 /*yield*/, examRepository.find({ select: ['doctorId'], where: { examId: examid } }).catch(function (err) { console.log(err); })];
            case 1:
                result = _a.sent();
                if (JSON.stringify(result) === '[]')
                    return [2 /*return*/, res.status(403).json({ 'message ': "exam doesn't exist" })];
                if (result[0].doctorId != req.user.doctorId)
                    return [2 /*return*/, res.status(403).json({ 'message ': "deleting other patient's diagnosis is not allowed" })];
                return [4 /*yield*/, examRepository.update({ doctorId: req.user.doctorId, examId: examid }, { diagnosis: null }).catch(function (err) { console.log(err); })];
            case 2:
                _a.sent();
                ;
                return [2 /*return*/, res.status(200).json({ 'message ': 'diagnosis deleted' })];
            case 3:
                error_3 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.downloadReport = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var examRepository, exams, e_1, PDFDocument, doc_1, anamnesis, symptomatology, _loop_1, i, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 10, , 11]);
                examRepository = typeorm_1.getConnection().getRepository(Exam_1.Exam);
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(Exam_1.Exam, 'exam')
                        .select(['exam.examId', 'exam.examDate', 'exam.diagnosis', 'exam.fiscalCode', 'doctor.name', 'doctor.surname', 'doctor.email', 'patient.name', 'patient.surname', 'patient.birthDate'])
                        .innerJoin('exam.doctor', 'doctor')
                        .innerJoin('exam.fiscalCode2', 'patient')
                        .where("exam.fiscalCode = '" + req.query.fiscalcode + "'")
                        .andWhere("exam.examDate = '" + req.query.examdate + "'")
                        .andWhere("exam.doctorId = '" + req.user.doctorId + "'")
                        .getMany().catch(function (err) { console.log(err); })];
            case 1:
                exams = _a.sent();
                if (!(JSON.stringify(exams) === '[]')) return [3 /*break*/, 2];
                res.status(200).json({ 'message ': 'empty set for diagnosis' });
                return [3 /*break*/, 9];
            case 2:
                e_1 = 0;
                Object(exams).forEach(function (element) { e_1++; });
                PDFDocument = require('../services/pdf_tables');
                doc_1 = new PDFDocument();
                doc_1.font('Helvetica-Bold').text('Exams Report', { width: 510, align: 'center' });
                doc_1.moveDown(3);
                doc_1.font('Helvetica-Bold').text("Patient");
                doc_1.moveDown(0.5);
                doc_1.font('Helvetica').text("Fiscal Code: " + exams[0].fiscalCode);
                doc_1.text("Name: " + exams[0].fiscalCode2.name);
                doc_1.text("Surname: " + exams[0].fiscalCode2.surname);
                doc_1.text("Birth Date: " + exams[0].fiscalCode2.birthDate);
                doc_1.moveDown();
                doc_1.font('Helvetica-Bold').text('Doctor');
                doc_1.moveDown(0.5);
                doc_1.font('Helvetica').text("Name: " + exams[0].doctor.name);
                doc_1.text("Surname: " + exams[0].doctor.surname);
                doc_1.text("Email: " + exams[0].doctor.email);
                doc_1.moveDown();
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(Patient_1.Patient, 'patient')
                        .select(['patient.fiscalCode', 'anamnesis.parentsFoodAllergy', 'anamnesis.parentsInhalantAllergy', 'anamnesis.parentsPolyposis', 'anamnesis.siblingPolyposis', 'anamnesis.parentsAsthma', 'anamnesis.silibingAsthma', 'anamnesis.silibingFoodAllergy', 'anamnesis.silibingInhalantAllergy', 'anamnesis.familyMedicalNotes'])
                        .innerJoin('patient.anamnesis', 'anamnesis')
                        .where("patient.fiscalCode = '" + req.query.fiscalcode + "'")
                        .getMany().catch(function (err) { console.log(err); })];
            case 3:
                anamnesis = _a.sent();
                if (JSON.stringify(anamnesis) !== '[]') {
                    doc_1.font('Helvetica-Bold').text('Anamnesis', { width: 510, align: 'center' });
                    doc_1.moveDown(0.5);
                    if (anamnesis[0].anamnesis.parentsFoodAllergy != 1)
                        doc_1.font('Helvetica').text("Parents Food Allergy: no");
                    else
                        doc_1.font('Helvetica').text("Parents Food Allergy: yes");
                    if (anamnesis[0].anamnesis.parentsInhalantAllergy != 1)
                        doc_1.text("Parents Inhalant Allergy: no");
                    else
                        doc_1.text("Parents Inhalant Allergy: yes");
                    if (anamnesis[0].anamnesis.parentsPolyposis != 1)
                        doc_1.text("Parents Polyposis: no");
                    else
                        doc_1.text("Parents Polyposis: yes");
                    if (anamnesis[0].anamnesis.siblingPolyposis != 1)
                        doc_1.text("Sibling Polyposis: no");
                    else
                        doc_1.text("Sibling Polyposis: yes");
                    if (anamnesis[0].anamnesis.parentsAsthma != 1)
                        doc_1.text("Parents Asthma: no");
                    else
                        doc_1.text("Parents Asthma: yes");
                    if (anamnesis[0].anamnesis.silibingAsthma != 1)
                        doc_1.text("Silibing Asthma: no");
                    else
                        doc_1.text("Sibling Asthma: yes");
                    if (anamnesis[0].anamnesis.silibingFoodAllergy != 1)
                        doc_1.text("Silibing Food Allergy: no");
                    else
                        doc_1.text("Silibing Food Allergy: yes");
                    if (anamnesis[0].anamnesis.silibingInhalantAllergy != 1)
                        doc_1.text("Silibing Inhalant Allergy: no");
                    else
                        doc_1.text("Silibing Inhalant Allergy: yes");
                    if (JSON.stringify(anamnesis[0].anamnesis.familyMedicalNotes) != 'null')
                        doc_1.text("Family Medical Notes: " + anamnesis[0].anamnesis.familyMedicalNotes);
                    else
                        doc_1.text("Family Medical Notes: none");
                }
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(Patient_1.Patient, 'patient')
                        .select(['patient.fiscalCode', 'symptomatology.obstruction', 'symptomatology.hypoacusis', 'symptomatology.rhinorrhea', 'symptomatology.sneezing', 'symptomatology.olfactoryProblems', 'symptomatology.auricularPadding', 'symptomatology.tinnitus', 'symptomatology.vertiginousSyndrome', 'symptomatology.doctorNotes', 'symptomatology.tearing', 'symptomatology.photophobia', 'symptomatology.conjunctiveItching', 'symptomatology.conjunctivaBurning', 'symptomatology.nasalItching', 'symptomatology.medicineUse', 'symptomatology.fever'])
                        .innerJoin('patient.symptomatology', 'symptomatology')
                        .where("patient.fiscalCode = '" + req.query.fiscalcode + "'")
                        .getMany().catch(function (err) { console.log(err); })];
            case 4:
                symptomatology = _a.sent();
                if (JSON.stringify(symptomatology) !== '[]') {
                    doc_1.moveDown();
                    doc_1.font('Helvetica-Bold').text('Symptomatology', { width: 510, align: 'center' });
                    doc_1.moveDown(0.5);
                    if (symptomatology[0].symptomatology.obstruction != 1)
                        doc_1.font('Helvetica').text("Obstruction: no");
                    else
                        doc_1.font('Helvetica').text("Obstruction: yes");
                    if (symptomatology[0].symptomatology.hypoacusis != 1)
                        doc_1.text("Hypoacusis: no");
                    else
                        doc_1.text("Hypoacusis: yes");
                    if (symptomatology[0].symptomatology.rhinorrhea != 1)
                        doc_1.text("Rhinorrhea: no");
                    else
                        doc_1.text("Rhinorrhea: yes");
                    if (symptomatology[0].symptomatology.sneezing != 1)
                        doc_1.text("Sneezing: no");
                    else
                        doc_1.text("Sneezing: yes");
                    if (symptomatology[0].symptomatology.olfactoryProblems != 1)
                        doc_1.text("Olfactory Problems: no");
                    else
                        doc_1.text("Olfactory Problems: yes");
                    if (symptomatology[0].symptomatology.auricularPadding != 1)
                        doc_1.text("Auricular Padding: no");
                    else
                        doc_1.text("Auricular Padding: yes");
                    if (symptomatology[0].symptomatology.tinnitus != 1)
                        doc_1.text("Tinnitus: no");
                    else
                        doc_1.text("Tinnitus: yes");
                    if (symptomatology[0].symptomatology.vertiginousSyndrome != 1)
                        doc_1.text("Vertiginous Syndrome: no");
                    else
                        doc_1.text("Vertiginous Syndrome: yes");
                    if (JSON.stringify(symptomatology[0].symptomatology.doctorNotes) != 'null')
                        doc_1.text("Doctor Notes: " + symptomatology[0].symptomatology.doctorNotes);
                    else
                        doc_1.text("Doctor Notes: none");
                    if (symptomatology[0].symptomatology.tearing != 1)
                        doc_1.text("Tearing: no");
                    else
                        doc_1.text("Tearing: yes");
                    if (symptomatology[0].symptomatology.photophobia != 1)
                        doc_1.text("Photophobia: no");
                    else
                        doc_1.text("Photophobia: yes");
                    if (symptomatology[0].symptomatology.conjunctiveItching != 1)
                        doc_1.text("Conjunctive Itching: no");
                    else
                        doc_1.text("Conjunctive Itching: yes");
                    if (symptomatology[0].symptomatology.conjunctivaBurning != 1)
                        doc_1.text("Conjunctiva Burning: no");
                    else
                        doc_1.text("Conjunctiva Burning: yes");
                    if (symptomatology[0].symptomatology.nasalItching != 1)
                        doc_1.text("Nasal Itching: no");
                    else
                        doc_1.text("Nasal Itching: yes");
                    if (symptomatology[0].symptomatology.medicineUse != 1)
                        doc_1.text("Medicine Use: no");
                    else
                        doc_1.text("Medicine Use: yes");
                    if (symptomatology[0].symptomatology.fever != 1)
                        doc_1.text("Fever: no");
                    else
                        doc_1.text("Fever: yes");
                }
                doc_1.moveDown();
                doc_1.moveDown();
                doc_1.font('Helvetica-Bold').text('Exams', { width: 510, align: 'center' });
                doc_1.moveDown(0.5);
                _loop_1 = function (i) {
                    var exam_field, cells_1, first_1, results, epithelium_1, neutrophil_1, eosinophil_1, mastocyte_1, lymphocyte_1, mucipara_1, other_1, table;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                doc_1.font('Helvetica').text("Exam code: " + exams[i].examId);
                                doc_1.text("Exam Date: " + exams[i].examDate);
                                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(FieldExam_1.FieldExam, 'fieldexam')
                                        .select(['fieldexam.fieldId'])
                                        .innerJoin('fieldexam.exam', 'exam')
                                        .where("fieldexam.examId = '" + exams[i].examId + "'")
                                        .getMany().catch(function (err) { console.log(err); })];
                            case 1:
                                exam_field = _a.sent();
                                if (!(JSON.stringify(exam_field) !== '[]')) return [3 /*break*/, 4];
                                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(Cells_1.Cells, 'cells')
                                        .select(['COUNT (celltype.type) AS cellNumber', 'celltype.type', 'cells.fieldId'])];
                            case 2:
                                cells_1 = _a.sent();
                                cells_1.innerJoin('cells.cellType', 'celltype');
                                first_1 = false;
                                Object(exam_field).forEach(function (element) {
                                    if (first_1 == false) {
                                        cells_1.where("cells.fieldId = '" + element.fieldId + "'");
                                        first_1 = true;
                                    }
                                    else
                                        cells_1.orWhere("cells.fieldId = '" + element.fieldId + "'");
                                });
                                cells_1.groupBy('celltype.type');
                                return [4 /*yield*/, cells_1.getRawMany().catch(function (err) { console.log(err); })];
                            case 3:
                                results = _a.sent();
                                if (JSON.stringify(results) !== '[]') {
                                    epithelium_1 = 0;
                                    neutrophil_1 = 0;
                                    eosinophil_1 = 0;
                                    mastocyte_1 = 0;
                                    lymphocyte_1 = 0;
                                    mucipara_1 = 0;
                                    other_1 = 0;
                                    Object(results).forEach(function (element) {
                                        if (element.celltype_type == 'epithelium')
                                            epithelium_1 = element.cellNumber;
                                        if (element.celltype_type == 'neutrophil')
                                            neutrophil_1 = element.cellNumber;
                                        if (element.celltype_type == 'eosinophil')
                                            eosinophil_1 = element.cellNumber;
                                        if (element.celltype_type == 'mastocyte')
                                            mastocyte_1 = element.cellNumber;
                                        if (element.celltype_type == 'lymphocyte')
                                            lymphocyte_1 = element.cellNumber;
                                        if (element.celltype_type == 'mucipara')
                                            mucipara_1 = element.cellNumber;
                                        if (element.celltype_type == 'other')
                                            other_1 = element.cellNumber;
                                    });
                                    table = {
                                        headers: ['Cell Type', 'Number of cells'],
                                        rows: [
                                            ['Epithelium', "" + epithelium_1],
                                            ['Neutrophil', "" + neutrophil_1],
                                            ['Eosinophil', "" + eosinophil_1],
                                            ['Mastocyte', "" + mastocyte_1],
                                            ['Lymphocyte', "" + lymphocyte_1],
                                            ['Mucipara', "" + mucipara_1],
                                            ['Other', "" + other_1]
                                        ]
                                    };
                                    doc_1.moveDown(0.5);
                                    doc_1.table(table, {
                                        prepareHeader: function () { return doc_1.font('Helvetica-Bold'); },
                                        prepareRow: function (row, i) { return doc_1.font('Helvetica').fontSize(12); }
                                    });
                                    doc_1.moveDown(0.5);
                                }
                                _a.label = 4;
                            case 4:
                                if (JSON.stringify(exams[i].diagnosis) != 'null')
                                    doc_1.text("Diagnosis: " + exams[i].diagnosis);
                                else
                                    doc_1.text("Diagnosis: not indicated");
                                doc_1.moveDown(4);
                                return [2 /*return*/];
                        }
                    });
                };
                i = 0;
                _a.label = 5;
            case 5:
                if (!(i < e_1)) return [3 /*break*/, 8];
                return [5 /*yield**/, _loop_1(i)];
            case 6:
                _a.sent();
                _a.label = 7;
            case 7:
                i++;
                return [3 /*break*/, 5];
            case 8:
                doc_1.moveDown(4);
                doc_1.text('Doctor signature', { align: 'right' });
                doc_1.text(exams[0].doctor.name + " " + exams[0].doctor.surname, { align: 'right' });
                doc_1.end();
                res.setHeader('Content-Type', 'application/pdf');
                res.setHeader('Content-Disposition', "attachment; filename=" + req.query.fiscalcode + "_diagnosis_" + req.query.examdate + ".pdf");
                doc_1.pipe(res);
                _a.label = 9;
            case 9: return [3 /*break*/, 11];
            case 10:
                error_4 = _a.sent();
                console.log(error_4);
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 11];
            case 11: return [2 /*return*/];
        }
    });
}); };
exports.generateDiagnosis = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var exam_field, cells_2, first_2, results, cells_3, fiscal_code, fiscalcode, result, responseData_1, spawn, process_1, error_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 10, , 11]);
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(FieldExam_1.FieldExam, 'fieldexam')
                        .select(['fieldexam.fieldId'])
                        .innerJoin('fieldexam.exam', 'exam')
                        .where("fieldexam.examId = '" + req.query.examid + "'")
                        .getMany().catch(function (err) { console.log(err); })];
            case 1:
                exam_field = _a.sent();
                if (!(JSON.stringify(exam_field) !== '[]')) return [3 /*break*/, 8];
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(Cells_1.Cells, 'cells')
                        .select(['COUNT (celltype.type) AS cellNumber', 'celltype.type', 'cells.fieldId'])];
            case 2:
                cells_2 = _a.sent();
                cells_2.leftJoin('cells.cellType', 'celltype');
                first_2 = false;
                Object(exam_field).forEach(function (element) {
                    if (first_2 == false) {
                        cells_2.where("cells.fieldId = '" + element.fieldId + "'");
                        first_2 = true;
                    }
                    else
                        cells_2.orWhere("cells.fieldId = '" + element.fieldId + "'");
                });
                cells_2.groupBy('celltype.type');
                return [4 /*yield*/, cells_2.getRawMany().catch(function (err) { console.log(err); })];
            case 3:
                results = _a.sent();
                if (!(JSON.stringify(results) !== '[]')) return [3 /*break*/, 6];
                cells_3 = [{ key: 'epithelium', value: '0' },
                    { key: 'neutrophil', value: '0' },
                    { key: 'eosinophil', value: '0' },
                    { key: 'mastocyte', value: '0' },
                    { key: 'lymphocyte', value: '0' },
                    { key: 'mucipara', value: '0' },
                    { key: 'other', value: '0' }];
                Object(results).forEach(function (element) {
                    cells_3.forEach(function (e) {
                        if (e['key'] == element.celltype_type)
                            e['value'] = element.cellNumber;
                    });
                });
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(Exam_1.Exam, 'exam')
                        .select(['exam.fiscalCode'])
                        .where("exam.examId = '" + req.query.examid + "'")
                        .getMany().catch(function (err) { console.log(err); })];
            case 4:
                fiscal_code = _a.sent();
                fiscalcode = fiscal_code[0].fiscalCode;
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(Patient_1.Patient, 'patient')
                        .select(['patient.fiscalCode', 'familyanamnesis.parentsFoodAllergy', 'familyanamnesis.parentsInhalantAllergy', 'familyanamnesis.parentsPolyposis', 'familyanamnesis.siblingPolyposis', 'familyanamnesis.parentsAsthma', 'familyanamnesis.silibingAsthma', 'familyanamnesis.silibingFoodAllergy', 'familyanamnesis.silibingInhalantAllergy'])
                        .addSelect(['symptomatology.obstruction',
                        'symptomatology.rhinorrhea', 'symptomatology.nasalItching',
                        'symptomatology.sneezing', 'symptomatology.olfactoryProblems',
                        'symptomatology.auricularPadding', 'symptomatology.hypoacusis',
                        'symptomatology.tinnitus', 'symptomatology.vertiginousSyndrome',
                        'symptomatology.fever', 'symptomatology.medicineUse',
                        'symptomatology.tearing', 'symptomatology.photophobia',
                        'symptomatology.conjunctiveItching', 'symptomatology.conjunctivaBurning'])
                        .addSelect(['medicalexam.nasalPyramid', 'medicalexam.nasalValve',
                        'medicalexam.nasalSeptum', 'medicalexam.turbinates', 'medicalexam.nasalPolyposisSx',
                        'medicalexam.nasalPolyposisDx', 'medicalexam.exudate'])
                        .addSelect(['pricktest.positive', 'pricktest.perennialAllergen',
                        'pricktest.poplar', 'pricktest.hazel', 'pricktest.commonAsh', 'pricktest.willow', 'pricktest.alder',
                        'pricktest.cupressacee', 'pricktest.oakTree', 'pricktest.blackHornbeam', 'pricktest.planeTree', 'pricktest.grasses',
                        'pricktest.floweringAsh', 'pricktest.pinaceae', 'pricktest.buckwheat', 'pricktest.urticaceae', 'pricktest.plantain',
                        'pricktest.birch', 'pricktest.chestnut', 'pricktest.absinthe'])
                        .leftJoin('patient.symptomatology', 'symptomatology')
                        .leftJoin('patient.anamnesis', 'familyanamnesis')
                        .leftJoin('patient.medicalExam', 'medicalexam')
                        .leftJoin('patient.prickTest', 'pricktest')
                        .where("patient.fiscalCode = '" + fiscalcode + "'")
                        .getMany().catch(function (err) { console.log(err); })];
            case 5:
                result = _a.sent();
                responseData_1 = '';
                spawn = require('child_process').spawn;
                process_1 = spawn('python', ['./src/services/diagnosis_generation/diagnosis.py',
                    './src/services/diagnosis_generation', JSON.stringify(cells_3), JSON.stringify(result[0].anamnesis),
                    JSON.stringify(result[0].symptomatology), JSON.stringify(result[0].medicalExam),
                    JSON.stringify(result[0].prickTest)], { encoding: 'utf8' });
                process_1.stdout.on('data', function (data) {
                    responseData_1 += data.toString();
                });
                process_1.stderr.on('data', function (data) {
                    responseData_1 += data.toString();
                });
                process_1.stdout.on('end', function (data) {
                    var generated_possible_diagnosis = responseData_1.replace('[BLOAD2] File ./src/services/diagnosis_generation/fatti.clp is not a binary construct file.\n[BLOAD2] File ./src/services/diagnosis_generation/functions.clp is not a binary construct file.\n[BLOAD2] File ./src/services/diagnosis_generation/diagnosi.clp is not a binary construct file.\n', '');
                    generated_possible_diagnosis = generated_possible_diagnosis.replace(new RegExp("'", "g"), '"');
                    res.status(200).json({
                        'message ': 'diagnosis generated',
                        'generated_possible_diagnosis': JSON.parse(generated_possible_diagnosis)
                    });
                });
                return [3 /*break*/, 7];
            case 6:
                res.status(403).json({ 'message ': "cells not extracted from fields yet" });
                _a.label = 7;
            case 7: return [3 /*break*/, 9];
            case 8:
                res.status(403).json({ 'message ': "exam doesn't have fields" });
                _a.label = 9;
            case 9: return [3 /*break*/, 11];
            case 10:
                error_5 = _a.sent();
                console.log(error_5);
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 11];
            case 11: return [2 /*return*/];
        }
    });
}); };
