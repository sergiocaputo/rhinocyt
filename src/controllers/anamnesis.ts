import {Response} from 'express';
import {getConnection, getManager} from 'typeorm';
import { Patient } from '../entities/Patient';
import {FamilyAnamnesis} from '../entities/FamilyAnamnesis';
import RequestWithUser from '../interfaces/RequestWithUser.interface';


export const getFamilyAnamnesis = async (req: RequestWithUser, res: Response) => {
    try{
        const familyanamnesis =  await getManager().createQueryBuilder(FamilyAnamnesis,'familyanamnesis')
        .select(['familyanamnesis.anamnesisId','familyanamnesis.parentsFoodAllergy','familyanamnesis.parentsInhalantAllergy',
                'familyanamnesis.parentsPolyposis','familyanamnesis.siblingPolyposis',
                'familyanamnesis.parentsAsthma','familyanamnesis.silibingAsthma',
                'familyanamnesis.silibingFoodAllergy','familyanamnesis.silibingInhalantAllergy',
                'familyanamnesis.familyMedicalNotes', 'patient.name', 'patient.surname'])
        .innerJoin('familyanamnesis.patients','patient')
        .where(`patient.doctorId = '${req.user.doctorId}'`)
        .andWhere(`patient.fiscalCode = '${req.query.fiscalcode}'`)
        .getMany();
       // Object(familyanamnesis).
        if(JSON.stringify(familyanamnesis) === '[]') res.status(200).json({ 'message ' : 'empty set for family anamnesis'});
        else res.status(200).json(familyanamnesis);
    }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}

export const newFamilyAnamnesis = async (req: RequestWithUser, res: Response) => {
    try{
        const familyanamnesisRepository =  getConnection().getRepository(FamilyAnamnesis);
        const patientRepository =  getConnection().getRepository(Patient);

        // check if patient exists
        const result = await patientRepository.find(
            {select: ['doctorId', 'anamnesisId'],where:{fiscalCode: req.body.fiscalCode}}).catch((err)=> {console.log(err);});
        if(JSON.stringify(result) === '[]') return res.status(403).json( {'message ': `patient doesn't exist`});
        if(result[0].doctorId != req.user.doctorId) return res.status(403).json({ 'message ' : ` saving other patient's anamnesis is not allowed`});

        if(result[0].anamnesisId == null){
            // anamnesis is saved for the first time
            const new_anamnesis = await familyanamnesisRepository.create(req.body);
            const results = await familyanamnesisRepository.save(new_anamnesis).catch((err)=> {
                console.log(err);});

           await patientRepository.update({fiscalCode: req.body.fiscalCode}, {anamnesisId: Object(results).anamnesisId}).catch((err)=> {
            console.log(err);});
           return res.status(200).json( {results, 'message ': 'new family anamnesis added'})
        }
        else{
            // anamnesis is modified
            if(req.body.parentsFoodAllergy != null) await familyanamnesisRepository.update({anamnesisId: result[0].anamnesisId},
                {parentsFoodAllergy: req.body.parentsFoodAllergy}).catch((err)=> {
                    console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.parentsInhalantAllergy != null) await familyanamnesisRepository.update({anamnesisId: result[0].anamnesisId},
                {parentsInhalantAllergy: req.body.parentsInhalantAllergy}).catch((err)=> {
                    console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.parentsPolyposis != null) await familyanamnesisRepository.update({anamnesisId: result[0].anamnesisId},
                {parentsPolyposis: req.body.parentsPolyposis}).catch((err)=> {
                    console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.siblingPolyposis != null) await familyanamnesisRepository.update({anamnesisId: result[0].anamnesisId},
                {siblingPolyposis: req.body.siblingPolyposis}).catch((err)=> {
                    console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.parentsAsthma != null) await familyanamnesisRepository.update({anamnesisId: result[0].anamnesisId},
                {parentsAsthma: req.body.parentsAsthma}).catch((err)=> {
                    console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.silibingAsthma != null) await familyanamnesisRepository.update({anamnesisId: result[0].anamnesisId},
                {silibingAsthma: req.body.silibingAsthma}).catch((err)=> {
                    console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.silibingFoodAllergy != null) await familyanamnesisRepository.update({anamnesisId: result[0].anamnesisId},
                {silibingFoodAllergy: req.body.silibingFoodAllergy}).catch((err)=> {
                    console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.silibingInhalantAllergy != null) await familyanamnesisRepository.update({anamnesisId: result[0].anamnesisId},
                {silibingInhalantAllergy: req.body.silibingInhalantAllergy}).catch((err)=> {
                    console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.familyMedicalNotes != null) await familyanamnesisRepository.update({anamnesisId: result[0].anamnesisId},
                {familyMedicalNotes: req.body.familyMedicalNotes}).catch((err)=> {
                    console.log(err);res.status(403).json({ 'message ': 'error' });});

            return res.status(200).json( {'message ': 'family anamnesis modified'})
        }
    }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}
