import {Request, Response} from 'express';
import {getConnection} from 'typeorm';
import {Patient} from '../entities/Patient';
import RequestWithUser from '../interfaces/RequestWithUser.interface';


export const getPatientByDoctorId = async (req: RequestWithUser, res: Response) => {
    try{
        const patientRepository =  getConnection().getRepository(Patient);
        const patients = await patientRepository.find( {select: ['name', 'surname', 'fiscalCode'],
            where: {doctorId : req.user.doctorId}}).catch((err)=> {console.log(err);});

        if(JSON.stringify(patients) === '[]') res.status(200).json({  'message ' : 'empty set'});
        else res.status(200).json(patients);
    }
    catch(error) {
        res.status(403).json({  'message ': 'error' });
    }
}

export const getPatientDetails = async (req: RequestWithUser, res: Response) => {
    try{
        const patientRepository =  getConnection().getRepository(Patient);
        const patient = await patientRepository.find( {select: ['fiscalCode', 'name', 'surname','gender','birthDate'],
            where: {fiscalCode : req.query.fiscalcode, doctorId : req.user.doctorId}}).catch((err)=> {console.log(err);});

        if(JSON.stringify(patient) === '[]') res.status(200).json({  'message ' : 'patient doesn\'t exist or has another doctor'});
        else res.status(200).json(patient);
    }
    catch(error) {
        res.status(403).json({  'message ': 'error' });
    }
}

export const searchPatient = async (req: RequestWithUser, res: Response) => {
    try{
        const patientRepository =  getConnection().getRepository(Patient);
        const patient = await patientRepository.find( {select: ['fiscalCode', 'name', 'surname','gender','birthDate'],
            where: {name : req.query.name, surname: req.query.surname, doctorId : req.user.doctorId}}).catch((err)=> {console.log(err);});

        if(JSON.stringify(patient) === '[]') res.status(403).json({  'message ' : 'patient doesn\'t exist'});
        else res.status(200).json(patient);
    }
    catch(error) {
        res.status(403).json({  'message ': 'error' });
    }
}

export const newPatient = async (req: RequestWithUser, res: Response) => {
    try{
        const fiscalcodelenght = JSON.stringify(req.body.fiscalCode).length-2;
        const regularlenght = 16;
        if(fiscalcodelenght != regularlenght) return res.status(403).json( { 'message ': 'fiscal code lenght error'});

        const patientRepository =  getConnection().getRepository(Patient);
        const patient = await patientRepository.find( {select: ['fiscalCode'],
            where: {fiscalCode : req.body.fiscalCode}}).catch((err)=> {console.log(err);});

        if(JSON.stringify(patient) != '[]') return res.status(403).json({  'message ' : 'patient already registered'});

        const new_patient = await patientRepository.create(
            {doctorId: req.user.doctorId,
             fiscalCode: req.body.fiscalCode,
             name: req.body.name,
             surname: req.body.surname,
             gender: req.body.gender,
             birthDate: req.body.birthDate
        });
        await patientRepository.save(new_patient).catch((err)=> {
            console.log(err);
            throw err;
        });

        const results = await patientRepository.find( {select: ['fiscalCode'],
        where: {fiscalCode : req.body.fiscalCode, doctorId: req.user.doctorId}}).catch((err)=> {console.log(err);});
        return res.status(200).json( { 'message ': 'new patient added', results})
    }
    catch(error) {
        res.status(403).json({  'message ': 'error' });
    }
}

export const deletePatient = async (req: RequestWithUser, res: Response) => {
    try{
        const patientRepository =  getConnection().getRepository(Patient);

        const result = await patientRepository.find({select: ['doctorId'],where:{fiscalCode: req.query.fiscalcode}}).catch((err)=> {
            console.log(err);});
        if(JSON.stringify(result) === '[]') return res.status(403).json( { 'message ': 'patient doesn\'t exist'});
        if(result[0].doctorId != req.user.doctorId) return res.status(403).json({  'message ' : 'deleting other doctors\' patients is not allowed'});
        const results = await patientRepository.delete({fiscalCode: (req.query.fiscalcode).toString(), doctorId: req.user.doctorId}).catch((err)=> {
            console.log(err);});

        // check if patient was deleted
        if(Object(results).affected > 0) return res.status(200).json({ 'message ': 'patient deleted'});
        else return res.status(200).json({ 'message ': 'error'})
    }
    catch(error) {
        res.status(403).json({  'message ': 'error' });
    }
}

