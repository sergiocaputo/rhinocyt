import {Request, Response} from 'express';
import {getConnection, EntityManager, getManager, AdvancedConsoleLogger} from 'typeorm';
import {FieldExam} from '../entities/FieldExam';
import RequestWithUser from '../interfaces/RequestWithUser.interface';
import { Exam } from '../entities/Exam';
import path = require('path');
import { Cells } from '../entities/Cells';
const fs = require('fs');


export const getFieldExam = async (req: RequestWithUser, res: Response) => {
    try{
        const FieldExamRepository =  getConnection().getRepository(FieldExam);
        const examid :number= +req.query.examid;

        if(examid != null){
            // check if examId is made from doctorId's patient
            const result = await getManager().createQueryBuilder(FieldExam,'fieldexam')
            .select(['fieldexam.fieldId','exam.doctorId'])
            .innerJoin('fieldexam.exam','exam')
            .where(`fieldexam.examId = '${examid}'`)
            .getMany().catch((err)=> {console.log(err);});

            if(JSON.stringify(result) === '[]') return res.status(200).json({ 'message ' : `exam doesn't exist or doesn't have registered fields`});
            if(result[0].exam.doctorId != req.user.doctorId) return res.status(403).json({ 'message ' : `is not allowed to view other patient's field exam`});
            // return the fields of the examId given
            const fields = await FieldExamRepository.find( {select: ['fieldId','photo'],
            where: {examId: examid}}).catch((err)=> {console.log(err);});

            if(JSON.stringify(fields) === '[]') res.status(200).json({ 'message ' : `exam doesn't have fields`});
            else res.status(200).json(fields);
        }
        else{
            return res.status(403).json({ 'message ' : 'examid not provided'});
        }
    }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}

export const newFieldExam = async (req: RequestWithUser, res: Response) => {
    try{
        const fieldExamRepository =  getConnection().getRepository(FieldExam);
        const examRepository = getConnection().getRepository(Exam);
        const examid :number= +req.body.examId;

        // check if exam is made from doctorId's patient
        if(req.body.examId != null) {
            const result = await examRepository.find({select: ['doctorId'],where:{examId: examid}}).catch((err)=> {console.log(err);});
            if(JSON.stringify(result) === '[]')  return res.status(403).json( {'message ': 'exam not registered'});
            if(result[0].doctorId != req.user.doctorId)
                return res.status(403).json({ 'message ' : `is not allowed to save other patient's fields exam`});
        }
        // new field(s) saved

        const tmppath = './src/services/cell_extraction/tmp';
        const cells = './src/services/cell_extraction/cells';
        fs.mkdir(cells,{recursive: true},function(err){if(err)console.log(err)});
        const newpath = `./src/services/cell_extraction/fields/examid_${examid}`;

        fs.readdir(tmppath,function (err, files) {
            if (err) return console.log('Unable to scan directory: ' + err);
            fs.mkdir(newpath,{recursive: true},function(err){
                if(err)console.log(err)
                files.forEach(async function (file) {
                        if (err) throw err;
                        fs.rename(tmppath+'/'+file, newpath+'/'+file, function(err){
                            if(err) console.log(err);
                        })
                        // save image path in DB
                        const field = await fieldExamRepository.create({
                            photo: newpath+'/'+file,
                            examId: examid,
                        })
                        await fieldExamRepository.save(field).catch((err)=> {
                            console.log(err);
                            return res.status(403).json( {'message ': 'wrong data sent'})
                        });
                        // get the fieldid to save image with it
                        const fieldid = await fieldExamRepository.find({select: ['fieldId'],where:{photo: newpath+'/'+file}})
                            .catch((err)=> {console.log(err);});
                        fs.rename(newpath+'/'+file, newpath+'/'+'fieldid_'+fieldid[0].fieldId+'_'+file, function(err){
                            if(err) console.log(err);
                        })
                        await fieldExamRepository.update({fieldId: fieldid[0].fieldId},{photo: newpath+'/'+'fieldid_'+fieldid[0].fieldId+'_'+file});

                });
            });
        });
        return res.status(200).json( {'message ': 'new field/fields added'})
    }
    catch(error) {

        res.status(403).json({ 'message ': 'error' });
    }
}

export const deleteFieldExam = async (req: RequestWithUser, res: Response) => {
    try{
        const fieldid :number= +req.query.fieldid;

        // check if exams's fieldId is made from doctorId's patient
        const result = await getManager().createQueryBuilder(FieldExam,'fieldexam')
        .select(['fieldexam.fieldId','exam.doctorId','fieldexam.photo'])
        .innerJoin('fieldexam.exam','exam')
        .where(`fieldexam.fieldId = '${fieldid}'`)
        .getMany().catch((err)=> {console.log(err);});

        if(JSON.stringify(result) === '[]') return res.status(403).json( {'message ': `field exam provided doesn't exist`});
        if(result[0].exam.doctorId != req.user.doctorId) return res.status(403).json({ 'message ' : `deleting other patient's fields exam is not allowed`});
        const fieldExamRepository =  getConnection().getRepository(FieldExam);
        const results = await fieldExamRepository.delete({fieldId: fieldid}).catch((err)=> {console.log(err);});

        // cancel file
        const file = result[0].photo;
        fs.unlinkSync(file,(err) => {
            if (err) throw err;
          });

        // check if field was deleted
        if(Object(results).affected > 0) return res.status(200).json({'message ': 'field deleted'});
        else return res.status(403).json({'message ': 'error'})
    }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}



export const extractCells = async (req: RequestWithUser, res: Response) => {
    try{const fieldExamRepository =  getConnection().getRepository(FieldExam);
        const cellsRepository = getConnection().getRepository(Cells);
        const examid :number= +req.query.examid;

        // check if exam is made from doctorId's patient
        let result = await getManager().createQueryBuilder(FieldExam,'fieldexam')
        .select(['fieldexam.fieldId','fieldexam.examId','exam.doctorId'])
        .innerJoin('fieldexam.exam','exam')
        .where(`fieldexam.examId = '${examid}'`)
        .getMany().catch((err)=> {console.log(err);});

        if(JSON.stringify(result) === '[]') return res.status(403).json( {'message ': `exam provided doesn't exist or doesn't have fields`});
        if(result[0].exam.doctorId != req.user.doctorId)
            return res.status(403).json({ 'message ' : `saving other patient's fields exam cells is not allowed`});

        const inputpy = './src/services/cell_extraction/inputImages';

        // create a new folder for python script cell extraction
        fs.exists(inputpy,function(exist){
            if(exist){
            fs.rmdirSync(inputpy,{recursive: true}, function(err){if(err) console.log(err)});
            }
            fs.mkdir(inputpy, function(err) {if (err) { console.log(err)}});
        });

        result = await fieldExamRepository.find({select: ['photo'],where:{examId: examid}}).catch((err)=> {console.log(err);});
            if(JSON.stringify(result) === '[]')  return res.status(403).json( {'message ': 'field not registered for this exam'});
            Object(result).forEach(element => {
                fs.copyFile(element.photo, inputpy+'/'+ (element.photo).split('/')[6], (err) => {
                if (err) console.log(err);
                else console.log(`${element.photo} saved in inputImages for py script`);
            });
        });

        // execute python script to extract cells
        let responseData = ''
        const spawn = require('child_process').spawn;
        const process = spawn('python', ['./src/services/cell_extraction/cells.py','./src/services/cell_extraction',examid]);
        process.stdout.setEncoding('utf-8');
        process.stdout.on('data', function (data){
            responseData += data.toString();
        });
        process.stderr.on('data', function (data){
            responseData += data.toString();
        });
        process.stdout.on('end',function(data){
            console.log(responseData);
            res.status(200).json({'message ': 'cells extracted'});
        });
    }
    catch(error) {console.log(error)
        res.status(403).json({ 'message ': 'error' });
    }
}


export const classificateCells = async (req: RequestWithUser, res: Response) => {
    try{const fieldExamRepository =  getConnection().getRepository(FieldExam);
        const cellsRepository = getConnection().getRepository(Cells);
        const examid :number= +req.query.examid;

        // check if exam is made from doctorId's patient
        const result = await getManager().createQueryBuilder(FieldExam,'fieldexam')
        .select(['fieldexam.fieldId','fieldexam.examId','exam.doctorId'])
        .innerJoin('fieldexam.exam','exam')
        .where(`fieldexam.examId = '${examid}'`)
        .getMany().catch((err)=> {console.log(err);});

        if(JSON.stringify(result) === '[]') return res.status(403).json( {'message ': `exam provided doesn't exist or doesn't have extracted cells`});
        if(result[0].exam.doctorId != req.user.doctorId)
              return res.status(403).json({ 'message ' : `saving other patient's fields exam cells is not allowed`});

        // check if this exam has cell extracted
        fs.exists(`./src/services/cell_extraction/cells/cell_examid_${examid}`,function(exist){
            if(exist){
                const classes = './src/services/cell_extraction/Classified_Cells';
                const epithelium = './src/services/cell_extraction/Classified_Cells/epithelium';
                const neutrophil = './src/services/cell_extraction/Classified_Cells/neutrophil';
                const eosinophil = './src/services/cell_extraction/Classified_Cells/eosinophil';
                const mastocyte = './src/services/cell_extraction/Classified_Cells/mastocyte';
                const lymphocyte = './src/services/cell_extraction/Classified_Cells/lymphocyte';
                const mucipara = './src/services/cell_extraction/Classified_Cells/mucipara';
                const other = './src/services/cell_extraction/Classified_Cells/other';

                fs.exists(classes,function(exist){
                    if(exist)
                    fs.rmdirSync(classes,{recursive: true}, function(err){if(err) console.log(err)});

                    fs.mkdir(classes, function(err) {if (err) { console.log(err)}
                    fs.mkdir(epithelium, function(err) {if (err) { console.log(err)}});
                    fs.mkdir(neutrophil, function(err) {if (err) { console.log(err)}});
                    fs.mkdir(eosinophil, function(err) {if (err) { console.log(err)}});
                    fs.mkdir(mastocyte, function(err) {if (err) { console.log(err)}});
                    fs.mkdir(lymphocyte, function(err) {if (err) { console.log(err)}});
                    fs.mkdir(mucipara, function(err) {if (err) { console.log(err)}});
                    fs.mkdir(other, function(err) {if (err) { console.log(err)}});
                    });
                });
                // execute python script to classify cells
                let responseData = ''
                const spawn = require('child_process').spawn;
                const process = spawn('python', ['./src/services/cell_extraction/on_fly_classifier.py','./src/services/cell_extraction',examid]);
                process.stdout.setEncoding('utf-8');
                process.stdout.on('data', function (data){
                    responseData += data.toString();
                });
                process.stderr.on('data', function (data){
                    responseData += data.toString();
                });
                process.stdout.on('end',function(data){
                    console.log(responseData);

                    // save cells to DB

                    const celldir = `./src/services/cell_extraction/cells/cell_examid_${examid}`;
                    fs.readdir(celldir,function (err, dir) {
                        if (err) return console.log('Unable to scan directory: ' + err);
                        dir.forEach(function (d) {
                            let cellclass = d;
                            let type = celldir + '/' + d;
                            fs.readdir(type, function(err, files){
                                if (err) return console.log('Unable to scan directory: ' + err);
                                files.forEach(async function (file){
                                    let fieldid = (file).split('_')[4].split('.')[0];
                                    let cell_type = selectCellType(cellclass);

                                    // save image path in DB
                                    const cell = await cellsRepository.create({
                                        cellPhoto: celldir+'/'+cellclass+'/'+file,
                                        cellTypeId: cell_type,
                                        fieldId: fieldid,
                                        })
                                    await cellsRepository.save(cell).catch((err)=> {
                                        console.log(err);
                                        return res.status(403).json( {'message ': 'wrong data sent'})
                                    });
                                 })
                            });
                        });
                    });
                    res.status(200).json({'message ': 'cells saved'});
                });
            }
            else return res.status(403).json({ 'message ' : `exam provided doesn't have any cell extracted`});
        })
    }
    catch(error) {console.log(error)
        res.status(403).json({ 'message ': 'error' });
    }
}

export function selectCellType (type: string) {
    let cell_type: number;
    switch(type){
        case 'epithelium': cell_type = 1;
        break;
        case 'neutrophil': cell_type = 2;
        break;
        case 'eosinophil': cell_type = 3;
        break;
        case 'mastocyte': cell_type = 4;
        break;
        case 'lymphocyte': cell_type = 5;
        break;
        case 'mucipara': cell_type = 6;
        break;
        case 'other': cell_type = 7;
        break;
    }
    return cell_type;
}

