import {Response} from 'express';
import {getConnection, getManager} from 'typeorm';
import { Patient } from '../entities/Patient';
import {PrickTest} from '../entities/PrickTest';
import RequestWithUser from '../interfaces/RequestWithUser.interface';


export const getPrickTest = async (req: RequestWithUser, res: Response) => {
    try{
        const pricktest =  await getManager().createQueryBuilder(PrickTest,'pricktest')
        .select(['pricktest.prickTestId','pricktest.positive','pricktest.perennialAllergen',
                 'pricktest.poplar','pricktest.hazel','pricktest.commonAsh',
                 'pricktest.willow','pricktest.alder','pricktest.cupressacee',
                 'pricktest.oakTree','pricktest.blackHornbeam','pricktest.planeTree',
                 'pricktest.grasses','pricktest.floweringAsh','pricktest.pinaceae',
                 'pricktest.buckwheat','pricktest.urticaceae','pricktest.plantain',
                 'pricktest.birch','pricktest.chestnut','pricktest.absinthe',
                 'patient.name', 'patient.surname'])
        .innerJoin('pricktest.patients','patient')
        .where(`patient.doctorId = '${req.user.doctorId}'`)
        .andWhere(`patient.fiscalCode = "${req.query.fiscalcode}"`)
        .getMany();

        if(JSON.stringify(pricktest) === '[]') res.status(200).json({  'message ' : 'empty set for pricktest'});
        else res.status(200).json(pricktest);
    }
    catch(error) {
        res.status(403).json({  'message ': 'error' });
    }
}

export const newPrickTest = async (req: RequestWithUser, res: Response) => {
    try{
        const pricktestRepository =  getConnection().getRepository(PrickTest);
        const patientRepository =  getConnection().getRepository(Patient);

        // check if patient exists
        const result = await patientRepository.find({select: ['doctorId', 'prickTestId'],where:{fiscalCode: req.body.fiscalCode}}).catch((err)=> {
            console.log(err);});
        if(JSON.stringify(result) === '[]') return res.status(403).json( { 'message ': 'patient doesn\'t exist'});
        if(result[0].doctorId != req.user.doctorId)
            return res.status(403).json({  'message ' : ' saving other patient\'s prick test is not allowed'});

        if(result[0].prickTestId == null){
            // prick test is saved for the first time
            const new_pricktest = await pricktestRepository.create(req.body);
            const results = await pricktestRepository.save(new_pricktest).catch((err)=> {
                console.log(err);});

           await patientRepository.update({fiscalCode: req.body.fiscalCode}, {prickTestId: Object(results).prickTestId}).catch((err)=> {
            console.log(err);});
           return res.status(200).json( {results,  'message ': 'new prick test added'})
        }
        else{
            // prick test is modified
            if(req.body.positive != null) await pricktestRepository.update({prickTestId: result[0].prickTestId},
                {positive: req.body.positive}).catch((err)=> {console.log(err);res.status(403).json({  'message ': 'error' });});

            if(req.body.perennialAllergen != null) await pricktestRepository.update({prickTestId: result[0].prickTestId},
                {perennialAllergen: req.body.perennialAllergen}).catch((err)=> {console.log(err);res.status(403).json({  'message ': 'error' });});

            if(req.body.poplar != null) await pricktestRepository.update({prickTestId: result[0].prickTestId},
                {poplar: req.body.poplar}).catch((err)=> {console.log(err);res.status(403).json({  'message ': 'error' });});

            if(req.body.hazel != null) await pricktestRepository.update({prickTestId: result[0].prickTestId},
                {hazel: req.body.hazel}).catch((err)=> {console.log(err);res.status(403).json({  'message ': 'error' });});

            if(req.body.commonAsh != null) await pricktestRepository.update({prickTestId: result[0].prickTestId},
                {commonAsh: req.body.commonAsh}).catch((err)=> {console.log(err);res.status(403).json({  'message ': 'error' });});

            if(req.body.willow != null) await pricktestRepository.update({prickTestId: result[0].prickTestId},
                {willow: req.body.willow}).catch((err)=> {console.log(err);res.status(403).json({  'message ': 'error' });});

            if(req.body.alder != null) await pricktestRepository.update({prickTestId: result[0].prickTestId},
                {alder: req.body.alder}).catch((err)=> {console.log(err);res.status(403).json({  'message ': 'error' });});

            if(req.body.cupressacee != null) await pricktestRepository.update({prickTestId: result[0].prickTestId},
                {cupressacee: req.body.cupressacee}).catch((err)=> {console.log(err);res.status(403).json({  'message ': 'error' });});

            if(req.body.oakTree != null) await pricktestRepository.update({prickTestId: result[0].prickTestId},
                {oakTree: req.body.oakTree}).catch((err)=> {console.log(err);res.status(403).json({  'message ': 'error' });});

            if(req.body.blackHornbeam != null) await pricktestRepository.update({prickTestId: result[0].prickTestId},
                {blackHornbeam: req.body.blackHornbeam}).catch((err)=> {console.log(err);res.status(403).json({  'message ': 'error' });});

            if(req.body.planeTree != null) await pricktestRepository.update({prickTestId: result[0].prickTestId},
                {planeTree: req.body.planeTree}).catch((err)=> {console.log(err);res.status(403).json({  'message ': 'error' });});

            if(req.body.grasses != null) await pricktestRepository.update({prickTestId: result[0].prickTestId},
                {grasses: req.body.grasses}).catch((err)=> {console.log(err);res.status(403).json({  'message ': 'error' });});

            if(req.body.floweringAsh != null) await pricktestRepository.update({prickTestId: result[0].prickTestId},
                {floweringAsh: req.body.floweringAsh}).catch((err)=> {console.log(err);res.status(403).json({  'message ': 'error' });});

            if(req.body.pinaceae != null) await pricktestRepository.update({prickTestId: result[0].prickTestId},
                {pinaceae: req.body.pinaceae}).catch((err)=> {console.log(err);res.status(403).json({  'message ': 'error' });});

            if(req.body.buckwheat != null) await pricktestRepository.update({prickTestId: result[0].prickTestId},
                {buckwheat: req.body.buckwheat}).catch((err)=> {console.log(err);res.status(403).json({  'message ': 'error' });});

            if(req.body.urticaceae != null) await pricktestRepository.update({prickTestId: result[0].prickTestId},
                {urticaceae: req.body.urticaceae}).catch((err)=> {console.log(err);res.status(403).json({  'message ': 'error' });});

            if(req.body.plantain != null) await pricktestRepository.update({prickTestId: result[0].prickTestId},
                {plantain: req.body.plantain}).catch((err)=> {console.log(err);res.status(403).json({  'message ': 'error' });});

            if(req.body.birch != null) await pricktestRepository.update({prickTestId: result[0].prickTestId},
                {birch: req.body.birch}).catch((err)=> {console.log(err);res.status(403).json({  'message ': 'error' });});

            if(req.body.chestnut != null) await pricktestRepository.update({prickTestId: result[0].prickTestId},
                {chestnut: req.body.chestnut}).catch((err)=> {console.log(err);res.status(403).json({  'message ': 'error' });});

            if(req.body.absinthe != null) await pricktestRepository.update({prickTestId: result[0].prickTestId},
                {absinthe: req.body.absinthe}).catch((err)=> {console.log(err);res.status(403).json({  'message ': 'error' });});


            return res.status(200).json( { 'message ': 'prick test modified'})
        }
    }
    catch(error) {
        res.status(403).json({  'message ': 'error' });
    }
}
