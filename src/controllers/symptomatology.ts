import {Response} from 'express';
import {getConnection, getManager} from 'typeorm';
import { Patient } from '../entities/Patient';
import {Symptomatology} from '../entities/Symptomatology';
import RequestWithUser from '../interfaces/RequestWithUser.interface';


export const getSymptomatology = async (req: RequestWithUser, res: Response) => {
    try{
        const symptomatology =  await getManager().createQueryBuilder(Symptomatology,'symptomatology')
        .select(['symptomatology.symptomatologyId','symptomatology.obstruction','symptomatology.hypoacusis',
                'symptomatology.rhinorrhea','symptomatology.sneezing',
                'symptomatology.olfactoryProblems','symptomatology.auricularPadding',
                'symptomatology.tinnitus','symptomatology.vertiginousSyndrome',
                'symptomatology.doctorNotes','symptomatology.tearing',
                'symptomatology.photophobia','symptomatology.conjunctiveItching',
                'symptomatology.conjunctivaBurning','symptomatology.nasalItching',
                'symptomatology.medicineUse','symptomatology.fever',
                'patient.name', 'patient.surname'])
        .innerJoin('symptomatology.patients','patient')
        .where(`patient.doctorId = '${req.user.doctorId}'`)
        .andWhere(`patient.fiscalCode = '${req.query.fiscalcode}'`)
        .getMany();

        if(JSON.stringify(symptomatology) === '[]') res.status(200).json({ 'message ' : 'empty set for symptomatology'});
        else res.status(200).json(symptomatology);
    }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}

export const newSymptomatology = async (req: RequestWithUser, res: Response) => {
    try{
        const symptomatologyRepository =  getConnection().getRepository(Symptomatology);
        const patientRepository =  getConnection().getRepository(Patient);

        // check if patient exists
        const result = await patientRepository.find({select: ['doctorId', 'symptomatologyId'],
        where:{fiscalCode: req.body.fiscalCode}}).catch((err)=> {
            console.log(err);});
        if(JSON.stringify(result) === '[]') return res.status(403).json( {'message ': 'patient doesn\'t exist'});
        if(result[0].doctorId != req.user.doctorId)
        return res.status(403).json({ 'message ' : ' saving other patient\'s symptomatology is not allowed'});

        if(result[0].symptomatologyId == null){
            // symptomatology is saved for the first time
            const new_symptomatology = await symptomatologyRepository.create(req.body);
            const results = await symptomatologyRepository.save(new_symptomatology).catch((err)=> {
                console.log(err);});

           await patientRepository.update({fiscalCode: req.body.fiscalCode}, {symptomatologyId: Object(results).symptomatologyId}).catch((err)=> {
            console.log(err);});
           return res.status(200).json( {results, 'message ': 'new symptomatology added'})
        }
        else{
            // symptomatology is modified
            if(req.body.obstruction != null) await symptomatologyRepository.update({symptomatologyId: result[0].symptomatologyId},
                {obstruction: req.body.obstruction}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.hypoacusis != null) await symptomatologyRepository.update({symptomatologyId: result[0].symptomatologyId},
                {hypoacusis: req.body.hypoacusis}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.rhinorrhea != null) await symptomatologyRepository.update({symptomatologyId: result[0].symptomatologyId},
                {rhinorrhea: req.body.rhinorrhea}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.sneezing != null) await symptomatologyRepository.update({symptomatologyId: result[0].symptomatologyId},
                {sneezing: req.body.sneezing}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.olfactoryProblems != null) await symptomatologyRepository.update({symptomatologyId: result[0].symptomatologyId},
                {olfactoryProblems: req.body.olfactoryProblems}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.auricularPadding != null) await symptomatologyRepository.update({symptomatologyId: result[0].symptomatologyId},
                {auricularPadding: req.body.auricularPadding}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.tinnitus != null) await symptomatologyRepository.update({symptomatologyId: result[0].symptomatologyId},
                {tinnitus: req.body.tinnitus}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.vertiginousSyndrome != null) await symptomatologyRepository.update({symptomatologyId: result[0].symptomatologyId},
                {vertiginousSyndrome: req.body.vertiginousSyndrome}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.doctorNotes != null) await symptomatologyRepository.update({symptomatologyId: result[0].symptomatologyId},
                {doctorNotes: req.body.doctorNotes}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.tearing != null) await symptomatologyRepository.update({symptomatologyId: result[0].symptomatologyId},
                {tearing: req.body.tearing}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.photophobia != null) await symptomatologyRepository.update({symptomatologyId: result[0].symptomatologyId},
                {photophobia: req.body.photophobia}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.conjunctiveItching != null) await symptomatologyRepository.update({symptomatologyId: result[0].symptomatologyId},
                {conjunctiveItching: req.body.conjunctiveItching}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.conjunctivaBurning != null) await symptomatologyRepository.update({symptomatologyId: result[0].symptomatologyId},
                {conjunctivaBurning: req.body.conjunctivaBurning}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.nasalItching != null) await symptomatologyRepository.update({symptomatologyId: result[0].symptomatologyId},
                {nasalItching: req.body.nasalItching}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.medicineUse != null) await symptomatologyRepository.update({symptomatologyId: result[0].symptomatologyId},
                {medicineUse: req.body.medicineUse}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.fever != null) await symptomatologyRepository.update({symptomatologyId: result[0].symptomatologyId},
                {fever: req.body.fever}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            return res.status(200).json( {'message ': 'symptomatology modified'})
        }
    }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}
