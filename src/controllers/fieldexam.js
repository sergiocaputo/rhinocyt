"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.selectCellType = exports.classificateCells = exports.extractCells = exports.deleteFieldExam = exports.newFieldExam = exports.getFieldExam = void 0;
var typeorm_1 = require("typeorm");
var FieldExam_1 = require("../entities/FieldExam");
var Exam_1 = require("../entities/Exam");
var Cells_1 = require("../entities/Cells");
var fs = require('fs');
exports.getFieldExam = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var FieldExamRepository, examid, result, fields, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 5, , 6]);
                FieldExamRepository = typeorm_1.getConnection().getRepository(FieldExam_1.FieldExam);
                examid = +req.query.examid;
                if (!(examid != null)) return [3 /*break*/, 3];
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(FieldExam_1.FieldExam, 'fieldexam')
                        .select(['fieldexam.fieldId', 'exam.doctorId'])
                        .innerJoin('fieldexam.exam', 'exam')
                        .where("fieldexam.examId = '" + examid + "'")
                        .getMany().catch(function (err) { console.log(err); })];
            case 1:
                result = _a.sent();
                if (JSON.stringify(result) === '[]')
                    return [2 /*return*/, res.status(200).json({ 'message ': "exam doesn't exist or doesn't have registered fields" })];
                if (result[0].exam.doctorId != req.user.doctorId)
                    return [2 /*return*/, res.status(403).json({ 'message ': "is not allowed to view other patient's field exam" })];
                return [4 /*yield*/, FieldExamRepository.find({ select: ['fieldId', 'photo'],
                        where: { examId: examid } }).catch(function (err) { console.log(err); })];
            case 2:
                fields = _a.sent();
                if (JSON.stringify(fields) === '[]')
                    res.status(200).json({ 'message ': "exam doesn't have fields" });
                else
                    res.status(200).json(fields);
                return [3 /*break*/, 4];
            case 3: return [2 /*return*/, res.status(403).json({ 'message ': 'examid not provided' })];
            case 4: return [3 /*break*/, 6];
            case 5:
                error_1 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}); };
exports.newFieldExam = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var fieldExamRepository_1, examRepository, examid_1, result, tmppath_1, cells, newpath_1, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 4]);
                fieldExamRepository_1 = typeorm_1.getConnection().getRepository(FieldExam_1.FieldExam);
                examRepository = typeorm_1.getConnection().getRepository(Exam_1.Exam);
                examid_1 = +req.body.examId;
                if (!(req.body.examId != null)) return [3 /*break*/, 2];
                return [4 /*yield*/, examRepository.find({ select: ['doctorId'], where: { examId: examid_1 } }).catch(function (err) { console.log(err); })];
            case 1:
                result = _a.sent();
                if (JSON.stringify(result) === '[]')
                    return [2 /*return*/, res.status(403).json({ 'message ': 'exam not registered' })];
                if (result[0].doctorId != req.user.doctorId)
                    return [2 /*return*/, res.status(403).json({ 'message ': "is not allowed to save other patient's fields exam" })];
                _a.label = 2;
            case 2:
                tmppath_1 = './src/services/cell_extraction/tmp';
                cells = './src/services/cell_extraction/cells';
                fs.mkdir(cells, { recursive: true }, function (err) { if (err)
                    console.log(err); });
                newpath_1 = "./src/services/cell_extraction/fields/examid_" + examid_1;
                fs.readdir(tmppath_1, function (err, files) {
                    if (err)
                        return console.log('Unable to scan directory: ' + err);
                    fs.mkdir(newpath_1, { recursive: true }, function (err) {
                        if (err)
                            console.log(err);
                        files.forEach(function (file) {
                            return __awaiter(this, void 0, void 0, function () {
                                var field, fieldid;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            if (err)
                                                throw err;
                                            fs.rename(tmppath_1 + '/' + file, newpath_1 + '/' + file, function (err) {
                                                if (err)
                                                    console.log(err);
                                            });
                                            return [4 /*yield*/, fieldExamRepository_1.create({
                                                    photo: newpath_1 + '/' + file,
                                                    examId: examid_1,
                                                })];
                                        case 1:
                                            field = _a.sent();
                                            return [4 /*yield*/, fieldExamRepository_1.save(field).catch(function (err) {
                                                    console.log(err);
                                                    return res.status(403).json({ 'message ': 'wrong data sent' });
                                                })];
                                        case 2:
                                            _a.sent();
                                            return [4 /*yield*/, fieldExamRepository_1.find({ select: ['fieldId'], where: { photo: newpath_1 + '/' + file } })
                                                    .catch(function (err) { console.log(err); })];
                                        case 3:
                                            fieldid = _a.sent();
                                            fs.rename(newpath_1 + '/' + file, newpath_1 + '/' + 'fieldid_' + fieldid[0].fieldId + '_' + file, function (err) {
                                                if (err)
                                                    console.log(err);
                                            });
                                            return [4 /*yield*/, fieldExamRepository_1.update({ fieldId: fieldid[0].fieldId }, { photo: newpath_1 + '/' + 'fieldid_' + fieldid[0].fieldId + '_' + file })];
                                        case 4:
                                            _a.sent();
                                            return [2 /*return*/];
                                    }
                                });
                            });
                        });
                    });
                });
                return [2 /*return*/, res.status(200).json({ 'message ': 'new field/fields added' })];
            case 3:
                error_2 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.deleteFieldExam = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var fieldid, result, fieldExamRepository, results, file, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 4]);
                fieldid = +req.query.fieldid;
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(FieldExam_1.FieldExam, 'fieldexam')
                        .select(['fieldexam.fieldId', 'exam.doctorId', 'fieldexam.photo'])
                        .innerJoin('fieldexam.exam', 'exam')
                        .where("fieldexam.fieldId = '" + fieldid + "'")
                        .getMany().catch(function (err) { console.log(err); })];
            case 1:
                result = _a.sent();
                if (JSON.stringify(result) === '[]')
                    return [2 /*return*/, res.status(403).json({ 'message ': "field exam provided doesn't exist" })];
                if (result[0].exam.doctorId != req.user.doctorId)
                    return [2 /*return*/, res.status(403).json({ 'message ': "deleting other patient's fields exam is not allowed" })];
                fieldExamRepository = typeorm_1.getConnection().getRepository(FieldExam_1.FieldExam);
                return [4 /*yield*/, fieldExamRepository.delete({ fieldId: fieldid }).catch(function (err) { console.log(err); })];
            case 2:
                results = _a.sent();
                file = result[0].photo;
                fs.unlinkSync(file, function (err) {
                    if (err)
                        throw err;
                });
                // check if field was deleted
                if (Object(results).affected > 0)
                    return [2 /*return*/, res.status(200).json({ 'message ': 'field deleted' })];
                else
                    return [2 /*return*/, res.status(403).json({ 'message ': 'error' })];
                return [3 /*break*/, 4];
            case 3:
                error_3 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.extractCells = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var fieldExamRepository, cellsRepository, examid, result, inputpy_1, responseData_1, spawn, process_1, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 4]);
                fieldExamRepository = typeorm_1.getConnection().getRepository(FieldExam_1.FieldExam);
                cellsRepository = typeorm_1.getConnection().getRepository(Cells_1.Cells);
                examid = +req.query.examid;
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(FieldExam_1.FieldExam, 'fieldexam')
                        .select(['fieldexam.fieldId', 'fieldexam.examId', 'exam.doctorId'])
                        .innerJoin('fieldexam.exam', 'exam')
                        .where("fieldexam.examId = '" + examid + "'")
                        .getMany().catch(function (err) { console.log(err); })];
            case 1:
                result = _a.sent();
                if (JSON.stringify(result) === '[]')
                    return [2 /*return*/, res.status(403).json({ 'message ': "exam provided doesn't exist or doesn't have fields" })];
                if (result[0].exam.doctorId != req.user.doctorId)
                    return [2 /*return*/, res.status(403).json({ 'message ': "saving other patient's fields exam cells is not allowed" })];
                inputpy_1 = './src/services/cell_extraction/inputImages';
                // create a new folder for python script cell extraction
                fs.exists(inputpy_1, function (exist) {
                    if (exist) {
                        fs.rmdirSync(inputpy_1, { recursive: true }, function (err) { if (err)
                            console.log(err); });
                    }
                    fs.mkdir(inputpy_1, function (err) { if (err) {
                        console.log(err);
                    } });
                });
                return [4 /*yield*/, fieldExamRepository.find({ select: ['photo'], where: { examId: examid } }).catch(function (err) { console.log(err); })];
            case 2:
                result = _a.sent();
                if (JSON.stringify(result) === '[]')
                    return [2 /*return*/, res.status(403).json({ 'message ': 'field not registered for this exam' })];
                Object(result).forEach(function (element) {
                    fs.copyFile(element.photo, inputpy_1 + '/' + (element.photo).split('/')[6], function (err) {
                        if (err)
                            console.log(err);
                        else
                            console.log(element.photo + " saved in inputImages for py script");
                    });
                });
                responseData_1 = '';
                spawn = require('child_process').spawn;
                process_1 = spawn('python', ['./src/services/cell_extraction/cells.py', './src/services/cell_extraction', examid]);
                process_1.stdout.setEncoding('utf-8');
                process_1.stdout.on('data', function (data) {
                    responseData_1 += data.toString();
                });
                process_1.stderr.on('data', function (data) {
                    responseData_1 += data.toString();
                });
                process_1.stdout.on('end', function (data) {
                    console.log(responseData_1);
                    res.status(200).json({ 'message ': 'cells extracted' });
                });
                return [3 /*break*/, 4];
            case 3:
                error_4 = _a.sent();
                console.log(error_4);
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.classificateCells = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var fieldExamRepository, cellsRepository_1, examid_2, result, error_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                fieldExamRepository = typeorm_1.getConnection().getRepository(FieldExam_1.FieldExam);
                cellsRepository_1 = typeorm_1.getConnection().getRepository(Cells_1.Cells);
                examid_2 = +req.query.examid;
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(FieldExam_1.FieldExam, 'fieldexam')
                        .select(['fieldexam.fieldId', 'fieldexam.examId', 'exam.doctorId'])
                        .innerJoin('fieldexam.exam', 'exam')
                        .where("fieldexam.examId = '" + examid_2 + "'")
                        .getMany().catch(function (err) { console.log(err); })];
            case 1:
                result = _a.sent();
                if (JSON.stringify(result) === '[]')
                    return [2 /*return*/, res.status(403).json({ 'message ': "exam provided doesn't exist or doesn't have extracted cells" })];
                if (result[0].exam.doctorId != req.user.doctorId)
                    return [2 /*return*/, res.status(403).json({ 'message ': "saving other patient's fields exam cells is not allowed" })];
                // check if this exam has cell extracted
                fs.exists("./src/services/cell_extraction/cells/cell_examid_" + examid_2, function (exist) {
                    if (exist) {
                        var classes_1 = './src/services/cell_extraction/Classified_Cells';
                        var epithelium_1 = './src/services/cell_extraction/Classified_Cells/epithelium';
                        var neutrophil_1 = './src/services/cell_extraction/Classified_Cells/neutrophil';
                        var eosinophil_1 = './src/services/cell_extraction/Classified_Cells/eosinophil';
                        var mastocyte_1 = './src/services/cell_extraction/Classified_Cells/mastocyte';
                        var lymphocyte_1 = './src/services/cell_extraction/Classified_Cells/lymphocyte';
                        var mucipara_1 = './src/services/cell_extraction/Classified_Cells/mucipara';
                        var other_1 = './src/services/cell_extraction/Classified_Cells/other';
                        fs.exists(classes_1, function (exist) {
                            if (exist)
                                fs.rmdirSync(classes_1, { recursive: true }, function (err) { if (err)
                                    console.log(err); });
                            fs.mkdir(classes_1, function (err) {
                                if (err) {
                                    console.log(err);
                                }
                                fs.mkdir(epithelium_1, function (err) { if (err) {
                                    console.log(err);
                                } });
                                fs.mkdir(neutrophil_1, function (err) { if (err) {
                                    console.log(err);
                                } });
                                fs.mkdir(eosinophil_1, function (err) { if (err) {
                                    console.log(err);
                                } });
                                fs.mkdir(mastocyte_1, function (err) { if (err) {
                                    console.log(err);
                                } });
                                fs.mkdir(lymphocyte_1, function (err) { if (err) {
                                    console.log(err);
                                } });
                                fs.mkdir(mucipara_1, function (err) { if (err) {
                                    console.log(err);
                                } });
                                fs.mkdir(other_1, function (err) { if (err) {
                                    console.log(err);
                                } });
                            });
                        });
                        // execute python script to classify cells
                        var responseData_2 = '';
                        var spawn = require('child_process').spawn;
                        var process_2 = spawn('python', ['./src/services/cell_extraction/on_fly_classifier.py', './src/services/cell_extraction', examid_2]);
                        process_2.stdout.setEncoding('utf-8');
                        process_2.stdout.on('data', function (data) {
                            responseData_2 += data.toString();
                        });
                        process_2.stderr.on('data', function (data) {
                            responseData_2 += data.toString();
                        });
                        process_2.stdout.on('end', function (data) {
                            console.log(responseData_2);
                            // save cells to DB
                            var celldir = "./src/services/cell_extraction/cells/cell_examid_" + examid_2;
                            fs.readdir(celldir, function (err, dir) {
                                if (err)
                                    return console.log('Unable to scan directory: ' + err);
                                dir.forEach(function (d) {
                                    var cellclass = d;
                                    var type = celldir + '/' + d;
                                    fs.readdir(type, function (err, files) {
                                        if (err)
                                            return console.log('Unable to scan directory: ' + err);
                                        files.forEach(function (file) {
                                            return __awaiter(this, void 0, void 0, function () {
                                                var fieldid, cell_type, cell;
                                                return __generator(this, function (_a) {
                                                    switch (_a.label) {
                                                        case 0:
                                                            fieldid = (file).split('_')[4].split('.')[0];
                                                            cell_type = selectCellType(cellclass);
                                                            return [4 /*yield*/, cellsRepository_1.create({
                                                                    cellPhoto: celldir + '/' + cellclass + '/' + file,
                                                                    cellTypeId: cell_type,
                                                                    fieldId: fieldid,
                                                                })];
                                                        case 1:
                                                            cell = _a.sent();
                                                            return [4 /*yield*/, cellsRepository_1.save(cell).catch(function (err) {
                                                                    console.log(err);
                                                                    return res.status(403).json({ 'message ': 'wrong data sent' });
                                                                })];
                                                        case 2:
                                                            _a.sent();
                                                            return [2 /*return*/];
                                                    }
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                            res.status(200).json({ 'message ': 'cells saved' });
                        });
                    }
                    else
                        return res.status(403).json({ 'message ': "exam provided doesn't have any cell extracted" });
                });
                return [3 /*break*/, 3];
            case 2:
                error_5 = _a.sent();
                console.log(error_5);
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
function selectCellType(type) {
    var cell_type;
    switch (type) {
        case 'epithelium':
            cell_type = 1;
            break;
        case 'neutrophil':
            cell_type = 2;
            break;
        case 'eosinophil':
            cell_type = 3;
            break;
        case 'mastocyte':
            cell_type = 4;
            break;
        case 'lymphocyte':
            cell_type = 5;
            break;
        case 'mucipara':
            cell_type = 6;
            break;
        case 'other':
            cell_type = 7;
            break;
    }
    return cell_type;
}
exports.selectCellType = selectCellType;
