import {Request, Response} from 'express';
import {getConnection, AdvancedConsoleLogger} from 'typeorm';
import {Doctor} from '../entities/Doctor';
import * as bcrypt from 'bcrypt';
import RequestWithUser from '../interfaces/RequestWithUser.interface';
import { createToken, createCookie } from '../services/authentication.service';


export const getDoctorById = async (req: Request, res: Response) => {
    try{
        const doctorRepository =  getConnection().getRepository(Doctor);
        const results = await doctorRepository.find( {select: ['name', 'surname', 'email'],
            where: {doctorId : req.params.iddoctor}}).catch((err)=> {console.log(err);});

        if(JSON.stringify(results) === '[]') res.status(200).json({ 'message ' : 'empty set'});
        else res.status(200).json(results);
    }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}

export const updateDoctor = async (req: RequestWithUser, res: Response) => {
    try{
        const doctorRepository =  getConnection().getRepository(Doctor);

        if(req.body.password != null){
            const hashed_password = await bcrypt.hash(req.body.password,10);

            await doctorRepository.update({doctorId: req.user.doctorId}, {password: hashed_password});
            const results = await doctorRepository.findOne(req.user.doctorId);
            if(JSON.stringify(results) === '[]') res.status(200).json({ 'message ' : 'empty set'});

            return res.status(200).json({results, 'message ': 'password updated'});
        }
        else if(req.body.email != null){

            await doctorRepository.update({doctorId: req.user.doctorId}, {email: req.body.email});
            const results = await doctorRepository.findOne(req.user.doctorId);
            if(JSON.stringify(results) === '[]') res.status(200).json({ 'message ' : 'empty set'});

            return res.status(200).json({results, 'message ': 'email updated'});
        }
        else return res.status(403).json({'message ': 'wrong parameters given'});
    }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}

export const login = async (req: Request, res: Response) => {
    try{
        const doctorRepository =  getConnection().getRepository(Doctor);
        const results = await doctorRepository.find( {select: ['doctorId','email', 'password'],
            where: {email : req.body.email}}).catch((err)=> {console.log(err);});

        if(JSON.stringify(results) === '[]') res.status(403).json({ 'message ' : 'user not registered'});
            else {
              const user = results[0].doctorId;
              const password = results[0].password;

              if(! (await bcrypt.compare(req.body.password, password))) res.status(403).json({ 'message ' : 'incorrect password'});
                else {
                      const tokenData = createToken(user);
                      res.setHeader('Set-Cookie', [createCookie(tokenData)]);
                      console.log('New session started by doctor with id '+ user);
                      return res.status(200).json({'message ': 'doctor logged in'});
                     }
            }
        }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}

export const newDoctor = async (req: Request, res: Response) => {
    try{
        const doctorRepository =  getConnection().getRepository(Doctor);
        const doctor = await doctorRepository.find( {select: ['email'],
            where: {email : req.body.email}}).catch((err)=> {console.log(err);});

        if(JSON.stringify(doctor) != '[]') return res.status(403).json({ 'message ' : 'doctor already registered'});
        const hashed_password = await bcrypt.hash(req.body.password,10);
        req.body.password = hashed_password;
        const new_doctor = await doctorRepository.create(req.body);
        const results = await doctorRepository.save(new_doctor).catch((err)=> {
            console.log(err);});

        // get doctorId assigned for the new registration to create token session
        const doctorId = await doctorRepository.find( {select: ['doctorId'],
            where: {email : req.body.email, password: hashed_password}}).catch((err)=> {console.log(err);});

        const user = doctorId[0].doctorId;
        const tokenData = createToken(user);
        res.setHeader('Set-Cookie', [createCookie(tokenData)]);
        console.log('New session started by doctor with id '+ user)
        return res.status(200).send(results);
    }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}


export const logout = (req: RequestWithUser, res: Response) => {
    console.log('Session closed by doctor with id '+ req.user.doctorId)
    res.setHeader('Set-Cookie', ['Authorization=;Max-age=0']);
    res.status(200).json({ 'message ': 'logged out' })
  }