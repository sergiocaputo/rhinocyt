"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.newMedicalExam = exports.getMedicalExam = void 0;
var typeorm_1 = require("typeorm");
var Patient_1 = require("../entities/Patient");
var MedicalExam_1 = require("../entities/MedicalExam");
exports.getMedicalExam = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var medicalExam, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(MedicalExam_1.MedicalExam, 'medicalExam')
                        .select(['medicalExam.medicalExamId', 'medicalExam.nasalPyramid', 'medicalExam.nasalValve',
                        'medicalExam.nasalSeptum', 'medicalExam.turbinates', 'medicalExam.nasalPolyposisSx',
                        'medicalExam.nasalPolyposisDx', 'medicalExam.exudate', 'medicalExam.adenoidHypertrophy',
                        'medicalExam.alterationNotes', 'medicalExam.earExam', 'medicalExam.rinoBaseSx',
                        'medicalExam.rinoBaseDx', 'medicalExam.rinoBaseSxDx', 'medicalExam.decongBaseSx',
                        'medicalExam.decongBaseDx', 'medicalExam.decongBaseSxDx', 'medicalExam.conclusions',
                        'patient.name', 'patient.surname'])
                        .innerJoin('medicalExam.patients', 'patient')
                        .where("patient.doctorId = '" + req.user.doctorId + "'")
                        .andWhere("patient.fiscalCode = '" + req.query.fiscalcode + "'")
                        .getMany()];
            case 1:
                medicalExam = _a.sent();
                if (JSON.stringify(medicalExam) === '[]')
                    res.status(200).json({ 'message ': 'empty set for medicalExam' });
                else
                    res.status(200).json(medicalExam);
                return [3 /*break*/, 3];
            case 2:
                error_1 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.newMedicalExam = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var medicalExamRepository, patientRepository, result, new_medicalExam, results, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 41, , 42]);
                medicalExamRepository = typeorm_1.getConnection().getRepository(MedicalExam_1.MedicalExam);
                patientRepository = typeorm_1.getConnection().getRepository(Patient_1.Patient);
                return [4 /*yield*/, patientRepository.find({ select: ['doctorId', 'medicalExamId'], where: { fiscalCode: req.body.fiscalCode } }).catch(function (err) {
                        console.log(err);
                    })];
            case 1:
                result = _a.sent();
                if (JSON.stringify(result) === '[]')
                    return [2 /*return*/, res.status(403).json({ 'message ': 'patient doesn\'t exist' })];
                if (result[0].doctorId != req.user.doctorId)
                    return [2 /*return*/, res.status(403).json({ 'message ': ' saving other patient\'s symptomatology is not allowed' })];
                if (!(result[0].medicalExamId == null)) return [3 /*break*/, 5];
                return [4 /*yield*/, medicalExamRepository.create(req.body)];
            case 2:
                new_medicalExam = _a.sent();
                return [4 /*yield*/, medicalExamRepository.save(new_medicalExam).catch(function (err) {
                        console.log(err);
                    })];
            case 3:
                results = _a.sent();
                return [4 /*yield*/, patientRepository.update({ fiscalCode: req.body.fiscalCode }, { medicalExamId: Object(results).medicalExamId }).catch(function (err) {
                        console.log(err);
                    })];
            case 4:
                _a.sent();
                return [2 /*return*/, res.status(200).json({ results: results, 'message ': 'new medical exam added' })];
            case 5:
                if (!(req.body.nasalPyramid != null)) return [3 /*break*/, 7];
                return [4 /*yield*/, medicalExamRepository.update({ medicalExamId: result[0].medicalExamId }, { nasalPyramid: req.body.nasalPyramid }).catch(function (err) { console.log(err); res.status(403).json({ 'message ': 'error' }); })];
            case 6:
                _a.sent();
                _a.label = 7;
            case 7:
                if (!(req.body.nasalValve != null)) return [3 /*break*/, 9];
                return [4 /*yield*/, medicalExamRepository.update({ medicalExamId: result[0].medicalExamId }, { nasalValve: req.body.nasalValve }).catch(function (err) { console.log(err); res.status(403).json({ 'message ': 'error' }); })];
            case 8:
                _a.sent();
                _a.label = 9;
            case 9:
                if (!(req.body.nasalSeptum != null)) return [3 /*break*/, 11];
                return [4 /*yield*/, medicalExamRepository.update({ medicalExamId: result[0].medicalExamId }, { nasalSeptum: req.body.nasalSeptum }).catch(function (err) { console.log(err); res.status(403).json({ 'message ': 'error' }); })];
            case 10:
                _a.sent();
                _a.label = 11;
            case 11:
                if (!(req.body.turbinates != null)) return [3 /*break*/, 13];
                return [4 /*yield*/, medicalExamRepository.update({ medicalExamId: result[0].medicalExamId }, { turbinates: req.body.turbinates }).catch(function (err) { console.log(err); res.status(403).json({ 'message ': 'error' }); })];
            case 12:
                _a.sent();
                _a.label = 13;
            case 13:
                if (!(req.body.nasalPolyposisSx != null)) return [3 /*break*/, 15];
                return [4 /*yield*/, medicalExamRepository.update({ medicalExamId: result[0].medicalExamId }, { nasalPolyposisSx: req.body.nasalPolyposisSx }).catch(function (err) { console.log(err); res.status(403).json({ 'message ': 'error' }); })];
            case 14:
                _a.sent();
                _a.label = 15;
            case 15:
                if (!(req.body.nasalPolyposisDx != null)) return [3 /*break*/, 17];
                return [4 /*yield*/, medicalExamRepository.update({ medicalExamId: result[0].medicalExamId }, { nasalPolyposisDx: req.body.nasalPolyposisDx }).catch(function (err) { console.log(err); res.status(403).json({ 'message ': 'error' }); })];
            case 16:
                _a.sent();
                _a.label = 17;
            case 17:
                if (!(req.body.exudate != null)) return [3 /*break*/, 19];
                return [4 /*yield*/, medicalExamRepository.update({ medicalExamId: result[0].medicalExamId }, { exudate: req.body.exudate }).catch(function (err) { console.log(err); res.status(403).json({ 'message ': 'error' }); })];
            case 18:
                _a.sent();
                _a.label = 19;
            case 19:
                if (!(req.body.adenoidHypertrophy != null)) return [3 /*break*/, 21];
                return [4 /*yield*/, medicalExamRepository.update({ medicalExamId: result[0].medicalExamId }, { adenoidHypertrophy: req.body.adenoidHypertrophy }).catch(function (err) { console.log(err); res.status(403).json({ 'message ': 'error' }); })];
            case 20:
                _a.sent();
                _a.label = 21;
            case 21:
                if (!(req.body.alterationNotes != null)) return [3 /*break*/, 23];
                return [4 /*yield*/, medicalExamRepository.update({ medicalExamId: result[0].medicalExamId }, { alterationNotes: req.body.alterationNotes }).catch(function (err) { console.log(err); res.status(403).json({ 'message ': 'error' }); })];
            case 22:
                _a.sent();
                _a.label = 23;
            case 23:
                if (!(req.body.earExam != null)) return [3 /*break*/, 25];
                return [4 /*yield*/, medicalExamRepository.update({ medicalExamId: result[0].medicalExamId }, { earExam: req.body.earExam }).catch(function (err) { console.log(err); res.status(403).json({ 'message ': 'error' }); })];
            case 24:
                _a.sent();
                _a.label = 25;
            case 25:
                if (!(req.body.rinoBaseSx != null)) return [3 /*break*/, 27];
                return [4 /*yield*/, medicalExamRepository.update({ medicalExamId: result[0].medicalExamId }, { rinoBaseSx: req.body.rinoBaseSx }).catch(function (err) { console.log(err); res.status(403).json({ 'message ': 'error' }); })];
            case 26:
                _a.sent();
                _a.label = 27;
            case 27:
                if (!(req.body.rinoBaseDx != null)) return [3 /*break*/, 29];
                return [4 /*yield*/, medicalExamRepository.update({ medicalExamId: result[0].medicalExamId }, { rinoBaseDx: req.body.rinoBaseDx }).catch(function (err) { console.log(err); res.status(403).json({ 'message ': 'error' }); })];
            case 28:
                _a.sent();
                _a.label = 29;
            case 29:
                if (!(req.body.rinoBaseSxDx != null)) return [3 /*break*/, 31];
                return [4 /*yield*/, medicalExamRepository.update({ medicalExamId: result[0].medicalExamId }, { rinoBaseSxDx: req.body.rinoBaseSxDx }).catch(function (err) { console.log(err); res.status(403).json({ 'message ': 'error' }); })];
            case 30:
                _a.sent();
                _a.label = 31;
            case 31:
                if (!(req.body.decongBaseSx != null)) return [3 /*break*/, 33];
                return [4 /*yield*/, medicalExamRepository.update({ medicalExamId: result[0].medicalExamId }, { decongBaseSx: req.body.decongBaseSx }).catch(function (err) { console.log(err); res.status(403).json({ 'message ': 'error' }); })];
            case 32:
                _a.sent();
                _a.label = 33;
            case 33:
                if (!(req.body.decongBaseDx != null)) return [3 /*break*/, 35];
                return [4 /*yield*/, medicalExamRepository.update({ medicalExamId: result[0].medicalExamId }, { decongBaseDx: req.body.decongBaseDx }).catch(function (err) { console.log(err); res.status(403).json({ 'message ': 'error' }); })];
            case 34:
                _a.sent();
                _a.label = 35;
            case 35:
                if (!(req.body.decongBaseSxDx != null)) return [3 /*break*/, 37];
                return [4 /*yield*/, medicalExamRepository.update({ medicalExamId: result[0].medicalExamId }, { decongBaseSxDx: req.body.decongBaseSxDx }).catch(function (err) { console.log(err); res.status(403).json({ 'message ': 'error' }); })];
            case 36:
                _a.sent();
                _a.label = 37;
            case 37:
                if (!(req.body.conclusions != null)) return [3 /*break*/, 39];
                return [4 /*yield*/, medicalExamRepository.update({ medicalExamId: result[0].medicalExamId }, { conclusions: req.body.conclusions }).catch(function (err) { console.log(err); res.status(403).json({ 'message ': 'error' }); })];
            case 38:
                _a.sent();
                _a.label = 39;
            case 39: return [2 /*return*/, res.status(200).json({ 'message ': 'medical exam modified' })];
            case 40: return [3 /*break*/, 42];
            case 41:
                error_2 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 42];
            case 42: return [2 /*return*/];
        }
    });
}); };
