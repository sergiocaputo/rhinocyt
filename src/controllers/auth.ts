import RequestWithUser from '../interfaces/RequestWithUser.interface';
import { getTwoFactorAuthenticationCode, respondWithQRCode, createToken, verifyTwoFactorAuthenticationCode, createCookie } from '../services/authentication.service';
import {Request, Response} from 'express';
import { getConnection } from 'typeorm';
import { Doctor } from '../entities/Doctor';


export const generateTwoFactorAuthenticationCode = async (request: RequestWithUser, response: Response) => {
    const user = request.user;
    const {otpauthUrl, base32} = getTwoFactorAuthenticationCode();
    const doctorRepository =  getConnection().getRepository(Doctor);
    await doctorRepository.update({doctorId: request.user.doctorId}, {twoFactorAuthenticationCode: base32});
    respondWithQRCode(otpauthUrl, response);
}

export const secondFactorAuthentication = async (request: RequestWithUser, response: Response, next) => {
    const { twoFactorAuthenticationCode } = request.body;
    const user = request.user;
    const isCodeValid = verifyTwoFactorAuthenticationCode(twoFactorAuthenticationCode, user);

    if (isCodeValid) {
      const tokenData = createToken(user.doctorId, true);
      response.setHeader('Set-Cookie', [createCookie(tokenData)]);
      response.send({
        user,
        password: undefined,
        twoFactorAuthenticationCode: undefined
      });
      console.log('Doctor with id '+user.doctorId+ ' completed two factor authentication');
    } else {
        console.log('Invalid authentication code');
        response.status(403).json({ 'message ': 'Invalid authentication code ' });
    }
  }

