"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.newFamilyAnamnesis = exports.getFamilyAnamnesis = void 0;
var typeorm_1 = require("typeorm");
var Patient_1 = require("../entities/Patient");
var FamilyAnamnesis_1 = require("../entities/FamilyAnamnesis");
exports.getFamilyAnamnesis = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var familyanamnesis, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(FamilyAnamnesis_1.FamilyAnamnesis, 'familyanamnesis')
                        .select(['familyanamnesis.anamnesisId', 'familyanamnesis.parentsFoodAllergy', 'familyanamnesis.parentsInhalantAllergy',
                        'familyanamnesis.parentsPolyposis', 'familyanamnesis.siblingPolyposis',
                        'familyanamnesis.parentsAsthma', 'familyanamnesis.silibingAsthma',
                        'familyanamnesis.silibingFoodAllergy', 'familyanamnesis.silibingInhalantAllergy',
                        'familyanamnesis.familyMedicalNotes', 'patient.name', 'patient.surname'])
                        .innerJoin('familyanamnesis.patients', 'patient')
                        .where("patient.doctorId = '" + req.user.doctorId + "'")
                        .andWhere("patient.fiscalCode = '" + req.query.fiscalcode + "'")
                        .getMany()];
            case 1:
                familyanamnesis = _a.sent();
                // Object(familyanamnesis).
                if (JSON.stringify(familyanamnesis) === '[]')
                    res.status(200).json({ 'message ': 'empty set for family anamnesis' });
                else
                    res.status(200).json(familyanamnesis);
                return [3 /*break*/, 3];
            case 2:
                error_1 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.newFamilyAnamnesis = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var familyanamnesisRepository, patientRepository, result, new_anamnesis, results, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 25, , 26]);
                familyanamnesisRepository = typeorm_1.getConnection().getRepository(FamilyAnamnesis_1.FamilyAnamnesis);
                patientRepository = typeorm_1.getConnection().getRepository(Patient_1.Patient);
                return [4 /*yield*/, patientRepository.find({ select: ['doctorId', 'anamnesisId'], where: { fiscalCode: req.body.fiscalCode } }).catch(function (err) { console.log(err); })];
            case 1:
                result = _a.sent();
                if (JSON.stringify(result) === '[]')
                    return [2 /*return*/, res.status(403).json({ 'message ': "patient doesn't exist" })];
                if (result[0].doctorId != req.user.doctorId)
                    return [2 /*return*/, res.status(403).json({ 'message ': " saving other patient's anamnesis is not allowed" })];
                if (!(result[0].anamnesisId == null)) return [3 /*break*/, 5];
                return [4 /*yield*/, familyanamnesisRepository.create(req.body)];
            case 2:
                new_anamnesis = _a.sent();
                return [4 /*yield*/, familyanamnesisRepository.save(new_anamnesis).catch(function (err) {
                        console.log(err);
                    })];
            case 3:
                results = _a.sent();
                return [4 /*yield*/, patientRepository.update({ fiscalCode: req.body.fiscalCode }, { anamnesisId: Object(results).anamnesisId }).catch(function (err) {
                        console.log(err);
                    })];
            case 4:
                _a.sent();
                return [2 /*return*/, res.status(200).json({ results: results, 'message ': 'new family anamnesis added' })];
            case 5:
                if (!(req.body.parentsFoodAllergy != null)) return [3 /*break*/, 7];
                return [4 /*yield*/, familyanamnesisRepository.update({ anamnesisId: result[0].anamnesisId }, { parentsFoodAllergy: req.body.parentsFoodAllergy }).catch(function (err) {
                        console.log(err);
                        res.status(403).json({ 'message ': 'error' });
                    })];
            case 6:
                _a.sent();
                _a.label = 7;
            case 7:
                if (!(req.body.parentsInhalantAllergy != null)) return [3 /*break*/, 9];
                return [4 /*yield*/, familyanamnesisRepository.update({ anamnesisId: result[0].anamnesisId }, { parentsInhalantAllergy: req.body.parentsInhalantAllergy }).catch(function (err) {
                        console.log(err);
                        res.status(403).json({ 'message ': 'error' });
                    })];
            case 8:
                _a.sent();
                _a.label = 9;
            case 9:
                if (!(req.body.parentsPolyposis != null)) return [3 /*break*/, 11];
                return [4 /*yield*/, familyanamnesisRepository.update({ anamnesisId: result[0].anamnesisId }, { parentsPolyposis: req.body.parentsPolyposis }).catch(function (err) {
                        console.log(err);
                        res.status(403).json({ 'message ': 'error' });
                    })];
            case 10:
                _a.sent();
                _a.label = 11;
            case 11:
                if (!(req.body.siblingPolyposis != null)) return [3 /*break*/, 13];
                return [4 /*yield*/, familyanamnesisRepository.update({ anamnesisId: result[0].anamnesisId }, { siblingPolyposis: req.body.siblingPolyposis }).catch(function (err) {
                        console.log(err);
                        res.status(403).json({ 'message ': 'error' });
                    })];
            case 12:
                _a.sent();
                _a.label = 13;
            case 13:
                if (!(req.body.parentsAsthma != null)) return [3 /*break*/, 15];
                return [4 /*yield*/, familyanamnesisRepository.update({ anamnesisId: result[0].anamnesisId }, { parentsAsthma: req.body.parentsAsthma }).catch(function (err) {
                        console.log(err);
                        res.status(403).json({ 'message ': 'error' });
                    })];
            case 14:
                _a.sent();
                _a.label = 15;
            case 15:
                if (!(req.body.silibingAsthma != null)) return [3 /*break*/, 17];
                return [4 /*yield*/, familyanamnesisRepository.update({ anamnesisId: result[0].anamnesisId }, { silibingAsthma: req.body.silibingAsthma }).catch(function (err) {
                        console.log(err);
                        res.status(403).json({ 'message ': 'error' });
                    })];
            case 16:
                _a.sent();
                _a.label = 17;
            case 17:
                if (!(req.body.silibingFoodAllergy != null)) return [3 /*break*/, 19];
                return [4 /*yield*/, familyanamnesisRepository.update({ anamnesisId: result[0].anamnesisId }, { silibingFoodAllergy: req.body.silibingFoodAllergy }).catch(function (err) {
                        console.log(err);
                        res.status(403).json({ 'message ': 'error' });
                    })];
            case 18:
                _a.sent();
                _a.label = 19;
            case 19:
                if (!(req.body.silibingInhalantAllergy != null)) return [3 /*break*/, 21];
                return [4 /*yield*/, familyanamnesisRepository.update({ anamnesisId: result[0].anamnesisId }, { silibingInhalantAllergy: req.body.silibingInhalantAllergy }).catch(function (err) {
                        console.log(err);
                        res.status(403).json({ 'message ': 'error' });
                    })];
            case 20:
                _a.sent();
                _a.label = 21;
            case 21:
                if (!(req.body.familyMedicalNotes != null)) return [3 /*break*/, 23];
                return [4 /*yield*/, familyanamnesisRepository.update({ anamnesisId: result[0].anamnesisId }, { familyMedicalNotes: req.body.familyMedicalNotes }).catch(function (err) {
                        console.log(err);
                        res.status(403).json({ 'message ': 'error' });
                    })];
            case 22:
                _a.sent();
                _a.label = 23;
            case 23: return [2 /*return*/, res.status(200).json({ 'message ': 'family anamnesis modified' })];
            case 24: return [3 /*break*/, 26];
            case 25:
                error_2 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 26];
            case 26: return [2 /*return*/];
        }
    });
}); };
