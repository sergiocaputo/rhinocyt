"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.changeCellClass = exports.deleteCell = exports.getTable = exports.getCells = void 0;
var typeorm_1 = require("typeorm");
var FieldExam_1 = require("../entities/FieldExam");
var Cells_1 = require("../entities/Cells");
var fieldexam_1 = require("../controllers/fieldexam");
var CellType_1 = require("../entities/CellType");
var fs = require('fs');
exports.getCells = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var fieldid, result, cells, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 5, , 6]);
                fieldid = +req.query.fieldid;
                if (!(fieldid.toString() != NaN.toString())) return [3 /*break*/, 3];
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(FieldExam_1.FieldExam, 'fieldexam')
                        .select(['fieldexam.fieldId', 'exam.doctorId'])
                        .innerJoin('fieldexam.exam', 'exam')
                        .where("fieldexam.fieldId = '" + fieldid + "'")
                        .getMany().catch(function (err) { console.log(err); })];
            case 1:
                result = _a.sent();
                if (JSON.stringify(result) === '[]')
                    return [2 /*return*/, res.status(403).json({ 'message ': 'field not registered' })];
                if (result[0].exam.doctorId != req.user.doctorId)
                    return [2 /*return*/, res.status(403).json({ 'message ': "is not allowed to view other patient's field cells" })];
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(Cells_1.Cells, 'cells')
                        .select(['cells.cellId', 'cells.cellPhoto', 'celltype.type'])
                        .innerJoin('cells.cellType', 'celltype')
                        .where("cells.fieldId = '" + fieldid + "'")
                        .getMany().catch(function (err) { console.log(err); })];
            case 2:
                cells = _a.sent();
                if (JSON.stringify(cells) === '[]')
                    res.status(200).json({ 'message ': "field doesn't have cells" });
                else
                    res.status(200).json(cells);
                return [3 /*break*/, 4];
            case 3: return [2 /*return*/, res.status(403).json({ 'message ': 'fieldid not provided' })];
            case 4: return [3 /*break*/, 6];
            case 5:
                error_1 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}); };
exports.getTable = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var examid, result, cells_1, first_1, results, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 5, , 6]);
                examid = +req.query.examid;
                if (!(examid.toString() != NaN.toString())) return [3 /*break*/, 3];
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(FieldExam_1.FieldExam, 'fieldexam')
                        .select(['fieldexam.fieldId', 'exam.doctorId'])
                        .innerJoin('fieldexam.exam', 'exam')
                        .where("fieldexam.examId = '" + examid + "'")
                        .getMany().catch(function (err) { console.log(err); })];
            case 1:
                result = _a.sent();
                if (JSON.stringify(result) === '[]')
                    return [2 /*return*/, res.status(403).json({ 'message ': 'exam not registered' })];
                if (result[0].exam.doctorId != req.user.doctorId)
                    return [2 /*return*/, res.status(403).json({ 'message ': "is not allowed to view other patient's field cells" })];
                cells_1 = typeorm_1.getManager().createQueryBuilder(Cells_1.Cells, 'cells')
                    .select(['COUNT (celltype.type) AS cellNumber', 'celltype.type', 'cells.fieldId']);
                cells_1.innerJoin('cells.cellType', 'celltype');
                first_1 = false;
                Object(result).forEach(function (element) {
                    if (first_1 == false) {
                        cells_1.where("cells.fieldId = '" + element.fieldId + "'");
                        first_1 = true;
                    }
                    else
                        cells_1.orWhere("cells.fieldId = '" + element.fieldId + "'");
                });
                cells_1.groupBy('celltype.type');
                return [4 /*yield*/, cells_1.getRawMany().catch(function (err) { console.log(err); })];
            case 2:
                results = _a.sent();
                if (JSON.stringify(results) === '[]')
                    res.status(200).json({ 'message ': "field doesn't have cells" });
                else
                    return [2 /*return*/, res.status(200).json({ results: results })];
                return [3 /*break*/, 4];
            case 3: return [2 /*return*/, res.status(403).json({ 'message ': 'fieldid not provided' })];
            case 4: return [3 /*break*/, 6];
            case 5:
                error_2 = _a.sent();
                console.log(error_2);
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}); };
/*
export const newCells = async (req: RequestWithUser, res: Response) => {
    try{
        const cellsRepository =  getConnection().getRepository(Cells);
        const fieldid :number= +req.body.fieldId;

        //check if exam's fieldId is from doctorId's patient
        if(fieldid.toString() != NaN.toString()) {
            const result = await getManager().createQueryBuilder(FieldExam,'fieldexam')
            .select(['fieldexam.fieldId','exam.doctorId'])
            .innerJoin('fieldexam.exam','exam')
            .where(`fieldexam.fieldId = '${fieldid}'`)
            .getMany().catch((err)=> {console.log(err);});

            if(JSON.stringify(result) === '[]') return res.status(403).json({ 'message' : 'field not registered'});
            if(result[0].exam.doctorId != req.user.doctorId)
                return res.status(403).json({ 'message' : `is not allowed to save other patient's field cells`});

            //new cells saved

            //photos sent with array
            //cells type sent with array
            if(req.body.photo.length != req.body.cellType.length)
                return res.status(403).json({ 'error' : 'cell photo number is different from cell type number'});

            //check if cell types given in input are saved as cell types
            const cellTypesRepository =  getConnection().getRepository(CellType);
            for(let j = 0; j < req.body.cellType.length; j++){
                const result = await cellTypesRepository.find( {select: ['cellTypeId','type'],
                where: {type : req.body.cellType[j]}}).catch((err)=> {console.log(err);});
                if(JSON.stringify(result) === '[]')
                    return res.status(403).json(
                         {'error': `cell type ${req.body.cellType[j]} is not saved as cell type, save it before save cells with this type `})
            }

            let celltype  = await selectCellType(req.body.cellType);

            const photos = req.body.photo;  //['imm\\cell1.jpg','imm\\cell2.jpg']

            for(let i = 0; i < photos.length; i++){
                const cell =  cellsRepository.create({
                    cellPhoto : req.body.photo[i],
                    cellTypeId: celltype[i],
                    fieldId: fieldid,
                });
                await cellsRepository.save(cell).catch((err)=> {
                    console.log(err);
                    return res.status(200).json( {'message': 'wrong data sent'})
                });
            };
            return res.status(200).json( {'message': 'new cells saved'})
        }
        else{
            return res.status(200).json( {'message': 'fieldid not provided'})
        }
    }
    catch(error) {
        res.status(403).json({ 'message': 'error' });
    }
}
*/
exports.deleteCell = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var cellid, fieldquery, fieldid, result, cellsRepository, results, file, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 4, , 5]);
                cellid = +req.query.cellid;
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(Cells_1.Cells, 'cells')
                        .select(['cells.cellId', 'fieldexam.fieldId', 'cells.cellPhoto'])
                        .innerJoin('cells.field', 'fieldexam')
                        .where("cells.cellId = '" + cellid + "'")
                        .getMany().catch(function (err) { console.log(err); })];
            case 1:
                fieldquery = _a.sent();
                if (JSON.stringify(fieldquery) === '[]')
                    return [2 /*return*/, res.status(403).json({ 'message ': "cell provided doesn't exist" })];
                fieldid = fieldquery[0].field.fieldId;
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(FieldExam_1.FieldExam, 'fieldexam')
                        .select(['fieldexam.fieldId', 'exam.doctorId'])
                        .innerJoin('fieldexam.exam', 'exam')
                        .where("fieldexam.fieldId = '" + fieldid + "'")
                        .getMany().catch(function (err) { console.log(err); })];
            case 2:
                result = _a.sent();
                if (result[0].exam.doctorId != req.user.doctorId)
                    return [2 /*return*/, res.status(403).json({ 'message ': "deleting other patient's cells is not allowed" })];
                cellsRepository = typeorm_1.getConnection().getRepository(Cells_1.Cells);
                return [4 /*yield*/, cellsRepository.delete({ cellId: cellid }).catch(function (err) { console.log(err); })];
            case 3:
                results = _a.sent();
                file = fieldquery[0].cellPhoto;
                fs.unlinkSync(file, function (err) {
                    if (err)
                        throw err;
                });
                // check if field was deleted
                if (Object(results).affected > 0)
                    return [2 /*return*/, res.status(200).json({ 'message ': 'cell deleted' })];
                else
                    return [2 /*return*/, res.status(403).json({ 'message ': 'error' })];
                return [3 /*break*/, 5];
            case 4:
                error_3 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); };
exports.changeCellClass = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var cellid, fieldquery, fieldid, result, celltype, cellTypeRepository, results, cellsRepository, cell_type, cell, file, newpath, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 6, , 7]);
                cellid = +req.body.cellId;
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(Cells_1.Cells, 'cells')
                        .select(['cells.cellId', 'fieldexam.fieldId'])
                        .innerJoin('cells.field', 'fieldexam')
                        .where("cells.cellId = '" + cellid + "'")
                        .getMany().catch(function (err) { console.log(err); })];
            case 1:
                fieldquery = _a.sent();
                if (JSON.stringify(fieldquery) === '[]')
                    return [2 /*return*/, res.status(403).json({ 'message ': "cell provided doesn't exist" })];
                fieldid = fieldquery[0].field.fieldId;
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(FieldExam_1.FieldExam, 'fieldexam')
                        .select(['fieldexam.fieldId', 'exam.doctorId', 'fieldexam.examId'])
                        .innerJoin('fieldexam.exam', 'exam')
                        .where("fieldexam.fieldId = '" + fieldid + "'")
                        .getMany().catch(function (err) { console.log(err); })];
            case 2:
                result = _a.sent();
                if (result[0].exam.doctorId != req.user.doctorId)
                    return [2 /*return*/, res.status(403).json({ 'message ': "changing other patient's cells type is not allowed" })];
                celltype = req.body.cellType;
                cellTypeRepository = typeorm_1.getConnection().getRepository(CellType_1.CellType);
                return [4 /*yield*/, cellTypeRepository.find({ select: ['cellTypeId'], where: { type: celltype } }).catch(function (err) { console.log(err); })];
            case 3:
                results = _a.sent();
                if (JSON.stringify(results) === '[]')
                    return [2 /*return*/, res.status(403).json({ 'message ': "celltype given doesn't exist" })];
                cellsRepository = typeorm_1.getConnection().getRepository(Cells_1.Cells);
                cell_type = fieldexam_1.selectCellType(celltype);
                return [4 /*yield*/, cellsRepository.find({ select: ['cellPhoto'], where: { cellId: cellid } }).catch(function (err) { console.log(err); })];
            case 4:
                cell = _a.sent();
                file = cell[0].cellPhoto.split('/')[7];
                newpath = "./src/services/cell_extraction/cells/cell_examid_" + result[0].examId + "/" + celltype + "/" + file;
                return [4 /*yield*/, cellsRepository.update({ cellId: cellid }, { cellTypeId: cell_type, cellPhoto: newpath })];
            case 5:
                _a.sent();
                fs.rename(cell[0].cellPhoto, newpath, function (err) {
                    if (err)
                        console.log(err);
                });
                return [2 /*return*/, res.status(200).json({ 'message ': 'cell type updated' })];
            case 6:
                error_4 = _a.sent();
                res.status(403).json({ 'message ': 'error' });
                return [3 /*break*/, 7];
            case 7: return [2 /*return*/];
        }
    });
}); };
