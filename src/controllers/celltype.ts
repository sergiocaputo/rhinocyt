import {Request, Response} from 'express';
import {getConnection, EntityManager, getManager} from 'typeorm';
import RequestWithUser from '../interfaces/RequestWithUser.interface';
import { CellType } from '../entities/CellType';


export const getCellTypes = async (req: RequestWithUser, res: Response) => {
    try{
        const celltype :number= +req.query.celltypeid;
        const cellTypesRepository =  getConnection().getRepository(CellType);

        if(celltype.toString() == NaN.toString()){
            // return all the cell types
            const types = await cellTypesRepository.find( {select: ['cellTypeId','type']})
            .catch((err)=> {console.log(err);});

            if(JSON.stringify(types) === '[]') res.status(403).json({ 'message ' : 'cell types not registered yet'});
            else res.status(200).json(types);
        }
        else{
            // return the cell type with cellTypeId given
            const types = await cellTypesRepository.find( {select: ['cellTypeId','type'],
            where: {cellTypeId : celltype}}).catch((err)=> {console.log(err);});

            if(JSON.stringify(types) === '[]') res.status(403).json({ 'message ' : 'cellTypeId not registered'});
            else res.status(200).json(types);
        }
    }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}

export const newCellType = async (req: RequestWithUser, res: Response) => {
    try{
        const celltype :string= req.body.type;
        const cellTypesRepository =  getConnection().getRepository(CellType);

        if(celltype != null) {
            // check if celltype is already saved
            const result = await cellTypesRepository.find( {select: ['cellTypeId','type'],
            where: {type : celltype}}).catch((err)=> {console.log(err);});

            if(JSON.stringify(result) === '[]'){
                const type = await cellTypesRepository.create({
                    type : celltype,
                });
                await cellTypesRepository.save(type).catch((err)=> {
                    console.log(err);
                    return res.status(403).json( {'message ': 'wrong data sent'})
                });
                return res.status(200).json( {'message ': 'new cell type saved'})
            }
           else return res.status(403).json( {'message ': 'cell type already saved'})
        }
        return res.status(403).json( {'message ': 'cell type not provided'})
    }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}

export const deleteCellType = async (req: RequestWithUser, res: Response) => {
    try{
        const celltypeid :number= +req.query.celltypeid;
        const cellTypesRepository =  getConnection().getRepository(CellType);

        if(celltypeid.toString() == NaN.toString()) return res.status(403).json( {'message ': 'cell type not provided'});

        const result = await cellTypesRepository.find({select: ['cellTypeId','type'],
        where: {cellTypeId : celltypeid}}).catch((err)=> {console.log(err);});
        if(JSON.stringify(result) === '[]') return res.status(403).json( {'message ': `celltype doesn't exist`});
        const results = await cellTypesRepository.delete({cellTypeId: celltypeid}).catch((err)=> {console.log(err);});

        // check if celltype was deleted
        if(Object(results).affected > 0) return res.status(200).json({'message ': 'celltype deleted'});
        else return res.status(403).json({'message ': 'error'});
    }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}
