import {Response} from 'express';
import {getConnection, getManager} from 'typeorm';
import { Patient } from '../entities/Patient';
import {MedicalExam} from '../entities/MedicalExam';
import RequestWithUser from '../interfaces/RequestWithUser.interface';


export const getMedicalExam = async (req: RequestWithUser, res: Response) => {
    try{
        const medicalExam =  await getManager().createQueryBuilder(MedicalExam,'medicalExam')
        .select(['medicalExam.medicalExamId','medicalExam.nasalPyramid','medicalExam.nasalValve',
                 'medicalExam.nasalSeptum','medicalExam.turbinates','medicalExam.nasalPolyposisSx',
                 'medicalExam.nasalPolyposisDx','medicalExam.exudate','medicalExam.adenoidHypertrophy',
                 'medicalExam.alterationNotes','medicalExam.earExam','medicalExam.rinoBaseSx',
                 'medicalExam.rinoBaseDx','medicalExam.rinoBaseSxDx','medicalExam.decongBaseSx',
                 'medicalExam.decongBaseDx','medicalExam.decongBaseSxDx','medicalExam.conclusions',
                 'patient.name', 'patient.surname'])
        .innerJoin('medicalExam.patients','patient')
        .where(`patient.doctorId = '${req.user.doctorId}'`)
        .andWhere(`patient.fiscalCode = '${req.query.fiscalcode}'`)
        .getMany();

        if(JSON.stringify(medicalExam) === '[]') res.status(200).json({ 'message ' : 'empty set for medicalExam'});
        else res.status(200).json(medicalExam);
    }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}

export const newMedicalExam = async (req: RequestWithUser, res: Response) => {
    try{
        const medicalExamRepository =  getConnection().getRepository(MedicalExam);
        const patientRepository =  getConnection().getRepository(Patient);

        // check if patient exists
        const result = await patientRepository.find({select: ['doctorId', 'medicalExamId'],where:{fiscalCode: req.body.fiscalCode}}).catch((err)=> {
            console.log(err);});
        if(JSON.stringify(result) === '[]') return res.status(403).json( {'message ': 'patient doesn\'t exist'});
        if(result[0].doctorId != req.user.doctorId) return res.status(403).json(
            { 'message ' : ' saving other patient\'s symptomatology is not allowed'});

        if(result[0].medicalExamId == null){
            // medical exam is saved for the first time
            const new_medicalExam = await medicalExamRepository.create(req.body);
            const results = await medicalExamRepository.save(new_medicalExam).catch((err)=> {
                console.log(err);});

           await patientRepository.update({fiscalCode: req.body.fiscalCode}, {medicalExamId: Object(results).medicalExamId}).catch((err)=> {
            console.log(err);});
           return res.status(200).json( {results, 'message ': 'new medical exam added'})
        }
        else{
            // medical exam is modified
            if(req.body.nasalPyramid != null) await medicalExamRepository.update({medicalExamId: result[0].medicalExamId},
                {nasalPyramid: req.body.nasalPyramid}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.nasalValve != null) await medicalExamRepository.update({medicalExamId: result[0].medicalExamId},
                {nasalValve: req.body.nasalValve}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.nasalSeptum != null) await medicalExamRepository.update({medicalExamId: result[0].medicalExamId},
                {nasalSeptum: req.body.nasalSeptum}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.turbinates != null) await medicalExamRepository.update({medicalExamId: result[0].medicalExamId},
                {turbinates: req.body.turbinates}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.nasalPolyposisSx != null) await medicalExamRepository.update({medicalExamId: result[0].medicalExamId},
                {nasalPolyposisSx: req.body.nasalPolyposisSx}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.nasalPolyposisDx != null) await medicalExamRepository.update({medicalExamId: result[0].medicalExamId},
                {nasalPolyposisDx: req.body.nasalPolyposisDx}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.exudate != null) await medicalExamRepository.update({medicalExamId: result[0].medicalExamId},
                {exudate: req.body.exudate}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.adenoidHypertrophy != null) await medicalExamRepository.update({medicalExamId: result[0].medicalExamId},
                {adenoidHypertrophy: req.body.adenoidHypertrophy}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.alterationNotes != null) await medicalExamRepository.update({medicalExamId: result[0].medicalExamId},
                {alterationNotes: req.body.alterationNotes}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.earExam != null) await medicalExamRepository.update({medicalExamId: result[0].medicalExamId},
                {earExam: req.body.earExam}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.rinoBaseSx != null) await medicalExamRepository.update({medicalExamId: result[0].medicalExamId},
                {rinoBaseSx: req.body.rinoBaseSx}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.rinoBaseDx != null) await medicalExamRepository.update({medicalExamId: result[0].medicalExamId},
                {rinoBaseDx: req.body.rinoBaseDx}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.rinoBaseSxDx != null) await medicalExamRepository.update({medicalExamId: result[0].medicalExamId},
                {rinoBaseSxDx: req.body.rinoBaseSxDx}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.decongBaseSx != null) await medicalExamRepository.update({medicalExamId: result[0].medicalExamId},
                {decongBaseSx: req.body.decongBaseSx}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.decongBaseDx != null) await medicalExamRepository.update({medicalExamId: result[0].medicalExamId},
                {decongBaseDx: req.body.decongBaseDx}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.decongBaseSxDx != null) await medicalExamRepository.update({medicalExamId: result[0].medicalExamId},
                {decongBaseSxDx: req.body.decongBaseSxDx}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            if(req.body.conclusions != null) await medicalExamRepository.update({medicalExamId: result[0].medicalExamId},
                {conclusions: req.body.conclusions}).catch((err)=> {console.log(err);res.status(403).json({ 'message ': 'error' });});

            return res.status(200).json( {'message ': 'medical exam modified'})
        }
    }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}
