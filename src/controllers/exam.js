"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteExam = exports.newExam = exports.searchExams = exports.getPatientExams = void 0;
var typeorm_1 = require("typeorm");
var Exam_1 = require("../entities/Exam");
var Patient_1 = require("../entities/Patient");
exports.getPatientExams = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var examRepository, exams, exams, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 5, , 6]);
                examRepository = typeorm_1.getConnection().getRepository(Exam_1.Exam);
                if (!(req.query.fiscalcode == null)) return [3 /*break*/, 2];
                return [4 /*yield*/, examRepository.find({ select: ['examId', 'fiscalCode', 'examDate', 'diagnosis'],
                        where: { doctorId: req.user.doctorId } }).catch(function (err) { console.log(err); })];
            case 1:
                exams = _a.sent();
                if (JSON.stringify(exams) === '[]')
                    res.status(200).json({ ' message ': 'empty set' });
                else
                    res.status(200).json(exams);
                return [3 /*break*/, 4];
            case 2: return [4 /*yield*/, examRepository.find({ select: ['examId', 'examDate', 'diagnosis'],
                    where: { doctorId: req.user.doctorId, fiscalCode: req.query.fiscalcode } }).catch(function (err) { console.log(err); })];
            case 3:
                exams = _a.sent();
                if (JSON.stringify(exams) === '[]')
                    res.status(200).json({ ' message ': 'empty set for exams' });
                else
                    res.status(200).json(exams);
                _a.label = 4;
            case 4: return [3 /*break*/, 6];
            case 5:
                error_1 = _a.sent();
                res.status(403).json({ ' message ': 'error' });
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}); };
exports.searchExams = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var exams, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, typeorm_1.getManager().createQueryBuilder(Exam_1.Exam, 'exam')
                        .select(['exam.fiscalCode', 'exam.examId', 'exam.examDate', 'exam.diagnosis'])
                        .innerJoin('exam.fiscalCode2', 'patient')
                        .where("exam.doctorId = " + req.user.doctorId)
                        .andWhere("patient.name = \"" + req.query.name + "\"")
                        .andWhere("patient.surname = \"" + req.query.surname + "\"")
                        .getMany()];
            case 1:
                exams = _a.sent();
                if (JSON.stringify(exams) === '[]')
                    res.status(200).json({ ' message ': 'no exams found' });
                else
                    res.status(200).json(exams);
                return [3 /*break*/, 3];
            case 2:
                error_2 = _a.sent();
                console.log(error_2);
                res.status(403).json({ ' message ': 'error' });
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.newExam = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var examRepository, patientRepository, result, exam, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 5, , 6]);
                examRepository = typeorm_1.getConnection().getRepository(Exam_1.Exam);
                patientRepository = typeorm_1.getConnection().getRepository(Patient_1.Patient);
                if (!(req.body.fiscalCode != null)) return [3 /*break*/, 2];
                return [4 /*yield*/, patientRepository.find({ select: ['doctorId'], where: { fiscalCode: req.body.fiscalCode } }).catch(function (err) {
                        console.log(err);
                    })];
            case 1:
                result = _a.sent();
                if (JSON.stringify(result) === '[]')
                    return [2 /*return*/, res.status(403).json({ ' message ': 'fiscal code not registered' })];
                if (result[0].doctorId != req.user.doctorId)
                    return [2 /*return*/, res.status(403).json({ ' message ': 'is not allowed to save other patient\'s exams' })];
                _a.label = 2;
            case 2: return [4 /*yield*/, examRepository.create({ examDate: req.body.examDate,
                    diagnosis: req.body.diagnosis,
                    doctorId: req.user.doctorId,
                    fiscalCode: req.body.fiscalCode
                })];
            case 3:
                exam = _a.sent();
                return [4 /*yield*/, examRepository.save(exam).catch(function (err) {
                        console.log(err);
                        return res.status(403).json({ ' message ': 'wrong data sent' });
                    })];
            case 4:
                _a.sent();
                return [2 /*return*/, res.status(200).json({ ' message ': 'new exam added' })];
            case 5:
                error_3 = _a.sent();
                res.status(403).json({ ' message ': 'error' });
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}); };
exports.deleteExam = function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var examRepository, examid, result, results, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 4]);
                examRepository = typeorm_1.getConnection().getRepository(Exam_1.Exam);
                examid = +req.query.examid;
                return [4 /*yield*/, examRepository.find({ select: ['doctorId'], where: { examId: examid } }).catch(function (err) { console.log(err); })];
            case 1:
                result = _a.sent();
                if (JSON.stringify(result) === '[]')
                    return [2 /*return*/, res.status(403).json({ ' message ': 'exam doesn\'t exist' })];
                if (result[0].doctorId != req.user.doctorId)
                    return [2 /*return*/, res.status(403).json({ ' message ': 'deleting other patient\'s exams is not allowed' })];
                return [4 /*yield*/, examRepository.delete({ examId: examid, doctorId: req.user.doctorId }).catch(function (err) { console.log(err); })];
            case 2:
                results = _a.sent();
                // check if exam was deleted
                if (Object(results).affected > 0)
                    return [2 /*return*/, res.status(200).json({ ' message ': 'exam deleted' })];
                else
                    return [2 /*return*/, res.status(403).json({ ' message ': 'error' })];
                return [3 /*break*/, 4];
            case 3:
                error_4 = _a.sent();
                res.status(403).json({ ' message ': 'error' });
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
