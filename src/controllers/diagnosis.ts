import { spawnSync } from 'child_process';
import {json, Response} from 'express';
import {getConnection, getManager} from 'typeorm';
import { Cells } from '../entities/Cells';
import {Exam} from '../entities/Exam';
import { FamilyAnamnesis } from '../entities/FamilyAnamnesis';
import { FieldExam } from '../entities/FieldExam';
import { Patient } from '../entities/Patient';
import { Symptomatology } from '../entities/Symptomatology';
import RequestWithUser from '../interfaces/RequestWithUser.interface';
const fs = require('fs');


export const getDiagnosis = async (req: RequestWithUser, res: Response) => {
    try{
        const examRepository =  getConnection().getRepository(Exam);
        const exams = await examRepository.find( {select: ['examId','fiscalCode','examDate','diagnosis'],
            where: {doctorId : req.user.doctorId, fiscalCode: req.query.fiscalcode, examDate: req.query.examdate}}).catch((err)=> {
                console.log(err);});

        if(JSON.stringify(exams) === '[]') res.status(200).json({ 'message ' : 'empty set for diagnosis'});
        else res.status(200).json(exams);
    }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}

export const newDiagnosis = async (req: RequestWithUser, res: Response) => {
    try{
        const examRepository =  getConnection().getRepository(Exam);

        // check if exam exists
        const result = await examRepository.find({select: ['doctorId'],where:{examId: req.body.examId}}).catch((err)=> {console.log(err);});
        if(JSON.stringify(result) === '[]') return res.status(403).json( {'message ': `exam doesn't exist`});
        if(result[0].doctorId != req.user.doctorId) return res.status(403).json({ 'message ' : ` saving other patient's diagnosis is not allowed`});
        await examRepository.update({doctorId: req.user.doctorId, examId: req.body.examId}, {diagnosis: req.body.diagnosis});

        return res.status(200).json( {'message ': 'new diagnosis added'})
    }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}

export const deleteDiagnosis = async (req: RequestWithUser, res: Response) => {
    try{
        const examRepository =  getConnection().getRepository(Exam);
        const examid :number= +req.query.examid;

        const result = await examRepository.find({select: ['doctorId'],where:{examId: examid}}).catch((err)=> {console.log(err);});
        if(JSON.stringify(result) === '[]') return res.status(403).json( {'message ': `exam doesn't exist`});
        if(result[0].doctorId != req.user.doctorId) return res.status(403).json({ 'message ' : `deleting other patient's diagnosis is not allowed`});
        await examRepository.update({doctorId: req.user.doctorId, examId: examid}, {diagnosis: null}).catch((err)=> {console.log(err);}); ;

        return res.status(200).json( {'message ': 'diagnosis deleted'})
    }
    catch(error) {
        res.status(403).json({ 'message ': 'error' });
    }
}

export const downloadReport = async (req: RequestWithUser, res: Response) => {
    try{
        const examRepository =  getConnection().getRepository(Exam);

        const exams = await getManager().createQueryBuilder(Exam,'exam')
        .select(['exam.examId','exam.examDate','exam.diagnosis','exam.fiscalCode','doctor.name','doctor.surname','doctor.email','patient.name','patient.surname','patient.birthDate'])
        .innerJoin('exam.doctor','doctor')
        .innerJoin('exam.fiscalCode2','patient')
        .where(`exam.fiscalCode = '${req.query.fiscalcode}'`)
        .andWhere(`exam.examDate = '${req.query.examdate}'`)
        .andWhere(`exam.doctorId = '${req.user.doctorId}'`)
        .getMany().catch((err)=> {console.log(err);});


        if(JSON.stringify(exams) === '[]') res.status(200).json({ 'message ' : 'empty set for diagnosis'});
        else {
                let e = 0;
                Object(exams).forEach(element => { e++});

                const PDFDocument = require('../services/pdf_tables');
                const doc = new PDFDocument();

                doc.font('Helvetica-Bold').text('Exams Report',{ width: 510,align: 'center'});
                doc.moveDown(3);

                doc.font('Helvetica-Bold').text(`Patient`);
                doc.moveDown(0.5)
                doc.font('Helvetica').text(`Fiscal Code: ${exams[0].fiscalCode}`);
                doc.text(`Name: ${exams[0].fiscalCode2.name}`);
                doc.text(`Surname: ${exams[0].fiscalCode2.surname}`);
                doc.text(`Birth Date: ${exams[0].fiscalCode2.birthDate}`);
                doc.moveDown()
                doc.font('Helvetica-Bold').text('Doctor');
                doc.moveDown(0.5)
                doc.font('Helvetica').text(`Name: ${exams[0].doctor.name}`);
                doc.text(`Surname: ${exams[0].doctor.surname}`);
                doc.text(`Email: ${exams[0].doctor.email}`);
                doc.moveDown()

                const anamnesis = await getManager().createQueryBuilder(Patient,'patient')
                                .select(['patient.fiscalCode', 'anamnesis.parentsFoodAllergy', 'anamnesis.parentsInhalantAllergy', 'anamnesis.parentsPolyposis', 'anamnesis.siblingPolyposis',  'anamnesis.parentsAsthma', 'anamnesis.silibingAsthma', 'anamnesis.silibingFoodAllergy', 'anamnesis.silibingInhalantAllergy', 'anamnesis.familyMedicalNotes'])
                                .innerJoin('patient.anamnesis','anamnesis')
                                .where(`patient.fiscalCode = '${req.query.fiscalcode}'`)
                                .getMany().catch((err)=> {console.log(err);});

                if(JSON.stringify(anamnesis) !== '[]'){
                    doc.font('Helvetica-Bold').text('Anamnesis',{ width: 510,align: 'center'});
                    doc.moveDown(0.5)
                    if(anamnesis[0].anamnesis.parentsFoodAllergy != 1) doc.font('Helvetica').text(`Parents Food Allergy: no`);
                    else doc.font('Helvetica').text(`Parents Food Allergy: yes`);
                    if(anamnesis[0].anamnesis.parentsInhalantAllergy != 1) doc.text(`Parents Inhalant Allergy: no`);
                    else doc.text(`Parents Inhalant Allergy: yes`);
                    if(anamnesis[0].anamnesis.parentsPolyposis != 1) doc.text(`Parents Polyposis: no`);
                    else doc.text(`Parents Polyposis: yes`);
                    if(anamnesis[0].anamnesis.siblingPolyposis != 1) doc.text(`Sibling Polyposis: no`);
                    else doc.text(`Sibling Polyposis: yes`);
                    if(anamnesis[0].anamnesis.parentsAsthma != 1) doc.text(`Parents Asthma: no`);
                    else doc.text(`Parents Asthma: yes`);
                    if(anamnesis[0].anamnesis.silibingAsthma != 1) doc.text(`Silibing Asthma: no`);
                    else doc.text(`Sibling Asthma: yes`);
                    if(anamnesis[0].anamnesis.silibingFoodAllergy != 1) doc.text(`Silibing Food Allergy: no`);
                    else doc.text(`Silibing Food Allergy: yes`);
                    if(anamnesis[0].anamnesis.silibingInhalantAllergy != 1) doc.text(`Silibing Inhalant Allergy: no`);
                    else doc.text(`Silibing Inhalant Allergy: yes`);
                    if(JSON.stringify(anamnesis[0].anamnesis.familyMedicalNotes) != 'null') doc.text(`Family Medical Notes: ${anamnesis[0].anamnesis.familyMedicalNotes}`);
                    else doc.text(`Family Medical Notes: none`);
                }

                const symptomatology = await getManager().createQueryBuilder(Patient,'patient')
                                .select(['patient.fiscalCode', 'symptomatology.obstruction','symptomatology.hypoacusis','symptomatology.rhinorrhea','symptomatology.sneezing','symptomatology.olfactoryProblems','symptomatology.auricularPadding','symptomatology.tinnitus','symptomatology.vertiginousSyndrome','symptomatology.doctorNotes','symptomatology.tearing','symptomatology.photophobia','symptomatology.conjunctiveItching','symptomatology.conjunctivaBurning','symptomatology.nasalItching','symptomatology.medicineUse','symptomatology.fever'])
                                .innerJoin('patient.symptomatology','symptomatology')
                                .where(`patient.fiscalCode = '${req.query.fiscalcode}'`)
                                .getMany().catch((err)=> {console.log(err);});

                if(JSON.stringify(symptomatology) !== '[]'){
                    doc.moveDown()
                    doc.font('Helvetica-Bold').text('Symptomatology',{ width: 510,align: 'center'});
                    doc.moveDown(0.5)
                    if(symptomatology[0].symptomatology.obstruction != 1) doc.font('Helvetica').text(`Obstruction: no`);
                    else doc.font('Helvetica').text(`Obstruction: yes`);
                    if(symptomatology[0].symptomatology.hypoacusis != 1) doc.text(`Hypoacusis: no`);
                    else doc.text(`Hypoacusis: yes`);
                    if(symptomatology[0].symptomatology.rhinorrhea != 1) doc.text(`Rhinorrhea: no`);
                    else doc.text(`Rhinorrhea: yes`);
                    if(symptomatology[0].symptomatology.sneezing != 1) doc.text(`Sneezing: no`);
                    else doc.text(`Sneezing: yes`);
                    if(symptomatology[0].symptomatology.olfactoryProblems != 1) doc.text(`Olfactory Problems: no`);
                    else doc.text(`Olfactory Problems: yes`);
                    if(symptomatology[0].symptomatology.auricularPadding != 1) doc.text(`Auricular Padding: no`);
                    else doc.text(`Auricular Padding: yes`);
                    if(symptomatology[0].symptomatology.tinnitus != 1) doc.text(`Tinnitus: no`);
                    else doc.text(`Tinnitus: yes`);
                    if(symptomatology[0].symptomatology.vertiginousSyndrome != 1) doc.text(`Vertiginous Syndrome: no`);
                    else doc.text(`Vertiginous Syndrome: yes`);
                    if(JSON.stringify(symptomatology[0].symptomatology.doctorNotes) != 'null') doc.text(`Doctor Notes: ${symptomatology[0].symptomatology.doctorNotes}`);
                    else doc.text(`Doctor Notes: none`);
                    if(symptomatology[0].symptomatology.tearing != 1) doc.text(`Tearing: no`);
                    else doc.text(`Tearing: yes`);
                    if(symptomatology[0].symptomatology.photophobia != 1) doc.text(`Photophobia: no`);
                    else doc.text(`Photophobia: yes`);
                    if(symptomatology[0].symptomatology.conjunctiveItching != 1) doc.text(`Conjunctive Itching: no`);
                    else doc.text(`Conjunctive Itching: yes`);
                    if(symptomatology[0].symptomatology.conjunctivaBurning != 1) doc.text(`Conjunctiva Burning: no`);
                    else doc.text(`Conjunctiva Burning: yes`);
                    if(symptomatology[0].symptomatology.nasalItching != 1) doc.text(`Nasal Itching: no`);
                    else doc.text(`Nasal Itching: yes`);
                    if(symptomatology[0].symptomatology.medicineUse != 1) doc.text(`Medicine Use: no`);
                    else doc.text(`Medicine Use: yes`);
                    if(symptomatology[0].symptomatology.fever != 1) doc.text(`Fever: no`);
                    else doc.text(`Fever: yes`);
                }

                doc.moveDown()
                doc.moveDown()
                doc.font('Helvetica-Bold').text('Exams',{ width: 510,align: 'center'});
                doc.moveDown(0.5)
                for(let i = 0; i < e; i++){
                    doc.font('Helvetica').text(`Exam code: ${exams[i].examId}`)
                    doc.text(`Exam Date: ${exams[i].examDate}`)

                    const exam_field = await getManager().createQueryBuilder(FieldExam,'fieldexam')
                    .select(['fieldexam.fieldId'])
                    .innerJoin('fieldexam.exam','exam')
                    .where(`fieldexam.examId = '${exams[i].examId}'`)
                    .getMany().catch((err)=> {console.log(err);});

                    if(JSON.stringify(exam_field) !== '[]'){
                        const cells = await getManager().createQueryBuilder(Cells,'cells')
                        .select(['COUNT (celltype.type) AS cellNumber','celltype.type','cells.fieldId']);
                        cells.innerJoin('cells.cellType','celltype');
                        let first = false;
                        Object(exam_field).forEach(element => {
                            if(first == false) {
                                cells.where(`cells.fieldId = '${element.fieldId}'`);
                                first = true;
                            }
                            else cells.orWhere(`cells.fieldId = '${element.fieldId}'`);
                        });
                        cells.groupBy('celltype.type');
                        const results = await cells.getRawMany().catch((err)=> {console.log(err);});

                        if(JSON.stringify(results) !== '[]'){
                            let epithelium = 0;
                            let neutrophil = 0;
                            let eosinophil = 0;
                            let mastocyte = 0;
                            let lymphocyte = 0;
                            let mucipara = 0;
                            let other = 0;
                            Object(results).forEach(element => {
                                if(element.celltype_type == 'epithelium') epithelium = element.cellNumber;
                                if(element.celltype_type == 'neutrophil') neutrophil = element.cellNumber;
                                if(element.celltype_type == 'eosinophil') eosinophil = element.cellNumber;
                                if(element.celltype_type == 'mastocyte') mastocyte = element.cellNumber;
                                if(element.celltype_type == 'lymphocyte') lymphocyte = element.cellNumber;
                                if(element.celltype_type == 'mucipara') mucipara = element.cellNumber;
                                if(element.celltype_type == 'other') other = element.cellNumber;
                            });
                            const table = {
                                    headers: ['Cell Type', 'Number of cells'],
                                    rows: [
                                        ['Epithelium', `${epithelium}`],
                                        ['Neutrophil', `${neutrophil}`],
                                        ['Eosinophil', `${eosinophil}`],
                                        ['Mastocyte', `${mastocyte}`],
                                        ['Lymphocyte', `${lymphocyte}`],
                                        ['Mucipara', `${mucipara}`],
                                        ['Other', `${other}`]
                                    ]
                                };

                            doc.moveDown(0.5)
                            doc.table(table, {
                                prepareHeader: () => doc.font('Helvetica-Bold'),
                                prepareRow: (row, i) => doc.font('Helvetica').fontSize(12)
                            });
                            doc.moveDown(0.5)
                        }
                    }
                    if(JSON.stringify(exams[i].diagnosis) != 'null') doc.text(`Diagnosis: ${exams[i].diagnosis}`)
                    else doc.text(`Diagnosis: not indicated`)
                    doc.moveDown(4)
                }
                doc.moveDown(4)
                doc.text('Doctor signature',{align: 'right'})
                doc.text(`${exams[0].doctor.name} ${exams[0].doctor.surname}`,{align: 'right'})


                doc.end();
                res.setHeader('Content-Type', 'application/pdf');
                res.setHeader('Content-Disposition', `attachment; filename=${req.query.fiscalcode}_diagnosis_${req.query.examdate}.pdf`);
                doc.pipe(res)
            }
    }
    catch(error) {console.log(error)
        res.status(403).json({ 'message ': 'error' });
    }
}


export const generateDiagnosis = async (req: RequestWithUser, res: Response) => {
    try{
        const exam_field = await getManager().createQueryBuilder(FieldExam,'fieldexam')
        .select(['fieldexam.fieldId'])
        .innerJoin('fieldexam.exam','exam')
        .where(`fieldexam.examId = '${req.query.examid}'`)
        .getMany().catch((err)=> {console.log(err);});

        if(JSON.stringify(exam_field) !== '[]'){
            const cells = await getManager().createQueryBuilder(Cells,'cells')
            .select(['COUNT (celltype.type) AS cellNumber','celltype.type','cells.fieldId']);
            cells.leftJoin('cells.cellType','celltype');
            let first = false;
            Object(exam_field).forEach(element => {
                if(first == false) {
                    cells.where(`cells.fieldId = '${element.fieldId}'`);
                    first = true;
                }
                else cells.orWhere(`cells.fieldId = '${element.fieldId}'`);
            });
            cells.groupBy('celltype.type');
            const results = await cells.getRawMany().catch((err)=> {console.log(err);});

            if(JSON.stringify(results) !== '[]'){
                let cells = [{key: 'epithelium', value: '0'},
                             {key: 'neutrophil', value: '0'},
                             {key: 'eosinophil', value: '0'},
                             {key: 'mastocyte', value: '0'},
                             {key: 'lymphocyte', value: '0'},
                             {key: 'mucipara', value: '0'},
                             {key: 'other', value: '0'}];

                Object(results).forEach(element => {
                    cells.forEach(e => {
                        if(e['key'] == element.celltype_type) e['value'] = element.cellNumber;
                    })
                });

                const fiscal_code = await getManager().createQueryBuilder(Exam,'exam')
                .select(['exam.fiscalCode'])
                .where(`exam.examId = '${req.query.examid}'`)
                .getMany().catch((err)=> {console.log(err);});
                const fiscalcode = fiscal_code[0].fiscalCode;

                const result = await getManager().createQueryBuilder(Patient,'patient')
                .select(['patient.fiscalCode', 'familyanamnesis.parentsFoodAllergy', 'familyanamnesis.parentsInhalantAllergy', 'familyanamnesis.parentsPolyposis', 'familyanamnesis.siblingPolyposis',  'familyanamnesis.parentsAsthma', 'familyanamnesis.silibingAsthma', 'familyanamnesis.silibingFoodAllergy', 'familyanamnesis.silibingInhalantAllergy'])
                .addSelect(['symptomatology.obstruction',
                'symptomatology.rhinorrhea', 'symptomatology.nasalItching',
                'symptomatology.sneezing',  'symptomatology.olfactoryProblems',
                'symptomatology.auricularPadding', 'symptomatology.hypoacusis',
                'symptomatology.tinnitus', 'symptomatology.vertiginousSyndrome',
                'symptomatology.fever', 'symptomatology.medicineUse',
                'symptomatology.tearing', 'symptomatology.photophobia',
                'symptomatology.conjunctiveItching', 'symptomatology.conjunctivaBurning'])
                .addSelect(['medicalexam.nasalPyramid','medicalexam.nasalValve',
                'medicalexam.nasalSeptum','medicalexam.turbinates','medicalexam.nasalPolyposisSx',
                'medicalexam.nasalPolyposisDx','medicalexam.exudate'])
                .addSelect(['pricktest.positive', 'pricktest.perennialAllergen',
                'pricktest.poplar', 'pricktest.hazel', 'pricktest.commonAsh', 'pricktest.willow', 'pricktest.alder',
                'pricktest.cupressacee', 'pricktest.oakTree', 'pricktest.blackHornbeam', 'pricktest.planeTree', 'pricktest.grasses',
                'pricktest.floweringAsh', 'pricktest.pinaceae', 'pricktest.buckwheat', 'pricktest.urticaceae', 'pricktest.plantain',
                'pricktest.birch', 'pricktest.chestnut', 'pricktest.absinthe'])
                .leftJoin('patient.symptomatology','symptomatology')
                .leftJoin('patient.anamnesis','familyanamnesis')
                .leftJoin('patient.medicalExam','medicalexam')
                .leftJoin('patient.prickTest','pricktest')
                .where(`patient.fiscalCode = '${fiscalcode}'`)
                .getMany().catch((err)=> {console.log(err);})

                let responseData = '';
                const spawn = require('child_process').spawn;
                const process = spawn('python', ['./src/services/diagnosis_generation/diagnosis.py',
                './src/services/diagnosis_generation', JSON.stringify(cells),JSON.stringify(result[0].anamnesis),
                JSON.stringify(result[0].symptomatology),JSON.stringify(result[0].medicalExam),
                JSON.stringify(result[0].prickTest)], { encoding : 'utf8' });

                process.stdout.on('data', function (data){
                    responseData += data.toString();
                });
                process.stderr.on('data', function (data){
                    responseData += data.toString();
                });
                process.stdout.on('end',function(data){
                    let generated_possible_diagnosis = responseData.replace('[BLOAD2] File ./src/services/diagnosis_generation/fatti.clp is not a binary construct file.\n[BLOAD2] File ./src/services/diagnosis_generation/functions.clp is not a binary construct file.\n[BLOAD2] File ./src/services/diagnosis_generation/diagnosi.clp is not a binary construct file.\n','');
                    generated_possible_diagnosis = generated_possible_diagnosis.replace(new RegExp("'", "g"),'"')
                    res.status(200).json({
                        'message ': 'diagnosis generated',
                        'generated_possible_diagnosis' : JSON.parse(generated_possible_diagnosis)
                    });
                });
            }
            else res.status(403).json({'message ': `cells not extracted from fields yet`});
       }
       else res.status(403).json({'message ': `exam doesn't have fields`});
    }
    catch(error) {
        console.log(error)
        res.status(403).json({ 'message ': 'error' });
    }
}