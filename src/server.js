"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app_1 = require("./app");
require('dotenv').config();
var port = 3000;
// start express server
var server = new app_1.default(port);
server.listen();
