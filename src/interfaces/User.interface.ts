interface User {
    doctorId: number;
    name: string;
    surname: string;
    email: string;
    twoFactorAuthenticationCode: string;
  }

  export default User;