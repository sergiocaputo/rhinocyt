import { Request } from 'express';
import User from './User.interface';

interface RequestWithUser extends Request {
  user: User;
}

export default RequestWithUser;