export interface DataStoredInToken {
    _id: number;
    isSecondFactorAuthenticated: boolean;
  }
