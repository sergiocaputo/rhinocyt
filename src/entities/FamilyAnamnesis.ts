import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Patient } from "./Patient";

@Entity("family_anamnesis", { schema: "rhinocyt" })
export class FamilyAnamnesis {
  @PrimaryGeneratedColumn({ type: "int", name: "anamnesis_id" })
  anamnesisId: number;

  @Column("tinyint", { name: "parents_food_allergy", nullable: true, width: 1 })
  parentsFoodAllergy: boolean | null;

  @Column("tinyint", {
    name: "parents_inhalant_allergy",
    nullable: true,
    width: 1,
  })
  parentsInhalantAllergy: boolean | null;

  @Column("tinyint", { name: "parents_polyposis", nullable: true, width: 1 })
  parentsPolyposis: boolean | null;

  @Column("tinyint", { name: "sibling_polyposis", nullable: true, width: 1 })
  siblingPolyposis: boolean | null;

  @Column("tinyint", { name: "parents_asthma", nullable: true, width: 1 })
  parentsAsthma: boolean | null;

  @Column("tinyint", { name: "silibing_asthma", nullable: true, width: 1 })
  silibingAsthma: boolean | null;

  @Column("tinyint", {
    name: "silibing_food_allergy",
    nullable: true,
    width: 1,
  })
  silibingFoodAllergy: boolean | null;

  @Column("tinyint", {
    name: "silibing_inhalant_allergy",
    nullable: true,
    width: 1,
  })
  silibingInhalantAllergy: boolean | null;

  @Column("varchar", {
    name: "family_medical_notes",
    nullable: true,
    length: 500,
  })
  familyMedicalNotes: string | null;

  @OneToMany(() => Patient, (patient) => patient.anamnesis)
  patients: Patient[];
}
