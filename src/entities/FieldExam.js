"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FieldExam = void 0;
var typeorm_1 = require("typeorm");
var Cells_1 = require("./Cells");
var Exam_1 = require("./Exam");
var FieldExam = /** @class */ (function () {
    function FieldExam() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn({ type: "int", name: "field_id" }),
        __metadata("design:type", Number)
    ], FieldExam.prototype, "fieldId", void 0);
    __decorate([
        typeorm_1.Column("varchar", { name: "photo", length: 400 }),
        __metadata("design:type", String)
    ], FieldExam.prototype, "photo", void 0);
    __decorate([
        typeorm_1.Column("int", { name: "exam_id" }),
        __metadata("design:type", Number)
    ], FieldExam.prototype, "examId", void 0);
    __decorate([
        typeorm_1.OneToMany(function () { return Cells_1.Cells; }, function (cells) { return cells.field; }),
        __metadata("design:type", Array)
    ], FieldExam.prototype, "cells", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return Exam_1.Exam; }, function (exam) { return exam.fieldExams; }, {
            onDelete: "NO ACTION",
            onUpdate: "NO ACTION",
        }),
        typeorm_1.JoinColumn([{ name: "exam_id", referencedColumnName: "examId" }]),
        __metadata("design:type", Exam_1.Exam)
    ], FieldExam.prototype, "exam", void 0);
    FieldExam = __decorate([
        typeorm_1.Index("field_exam_ibfk_1", ["examId"], {}),
        typeorm_1.Entity("field_exam", { schema: "rhinocyt" })
    ], FieldExam);
    return FieldExam;
}());
exports.FieldExam = FieldExam;
