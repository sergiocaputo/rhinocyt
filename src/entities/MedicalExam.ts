import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Patient } from "./Patient";

@Entity("medical_exam", { schema: "rhinocyt" })
export class MedicalExam {
  @PrimaryGeneratedColumn({ type: "int", name: "medical_exam_id" })
  medicalExamId: number;

  @Column("tinyint", { name: "nasal_pyramid", nullable: true, width: 1 })
  nasalPyramid: boolean | null;

  @Column("tinyint", { name: "nasal_valve", nullable: true, width: 1 })
  nasalValve: boolean | null;

  @Column("tinyint", { name: "nasal_septum", nullable: true, width: 1 })
  nasalSeptum: boolean | null;

  @Column("tinyint", { name: "turbinates", nullable: true, width: 1 })
  turbinates: boolean | null;

  @Column("tinyint", { name: "nasal_polyposis_sx", nullable: true, width: 1 })
  nasalPolyposisSx: boolean | null;

  @Column("tinyint", { name: "nasal_polyposis_dx", nullable: true, width: 1 })
  nasalPolyposisDx: boolean | null;

  @Column("tinyint", { name: "exudate", nullable: true, width: 1 })
  exudate: boolean | null;

  @Column("tinyint", { name: "adenoid_hypertrophy", nullable: true, width: 1 })
  adenoidHypertrophy: boolean | null;

  @Column("varchar", { name: "alteration_notes", nullable: true, length: 500 })
  alterationNotes: string | null;

  @Column("varchar", { name: "ear_exam", nullable: true, length: 500 })
  earExam: string | null;

  @Column("float", { name: "rino_base_sx", nullable: true, precision: 12 })
  rinoBaseSx: number | null;

  @Column("float", { name: "rino_base_dx", nullable: true, precision: 12 })
  rinoBaseDx: number | null;

  @Column("float", { name: "rino_base_sx_dx", nullable: true, precision: 12 })
  rinoBaseSxDx: number | null;

  @Column("float", { name: "decong_base_sx", nullable: true, precision: 12 })
  decongBaseSx: number | null;

  @Column("float", { name: "decong_base_dx", nullable: true, precision: 12 })
  decongBaseDx: number | null;

  @Column("float", { name: "decong_base_sx_dx", nullable: true, precision: 12 })
  decongBaseSxDx: number | null;

  @Column("varchar", { name: "conclusions", nullable: true, length: 500 })
  conclusions: string | null;

  @OneToMany(() => Patient, (patient) => patient.medicalExam)
  patients: Patient[];
}
