"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CellType = void 0;
var typeorm_1 = require("typeorm");
var Cells_1 = require("./Cells");
var CellType = /** @class */ (function () {
    function CellType() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn({ type: "int", name: "cell_type_id" }),
        __metadata("design:type", Number)
    ], CellType.prototype, "cellTypeId", void 0);
    __decorate([
        typeorm_1.Column("varchar", { name: "type", length: 45 }),
        __metadata("design:type", String)
    ], CellType.prototype, "type", void 0);
    __decorate([
        typeorm_1.OneToMany(function () { return Cells_1.Cells; }, function (cells) { return cells.cellType; }),
        __metadata("design:type", Array)
    ], CellType.prototype, "cells", void 0);
    CellType = __decorate([
        typeorm_1.Entity("cell_type", { schema: "rhinocyt" })
    ], CellType);
    return CellType;
}());
exports.CellType = CellType;
