"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Exam = void 0;
var typeorm_1 = require("typeorm");
var Doctor_1 = require("./Doctor");
var Patient_1 = require("./Patient");
var FieldExam_1 = require("./FieldExam");
var Exam = /** @class */ (function () {
    function Exam() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn({ type: "int", name: "exam_id" }),
        __metadata("design:type", Number)
    ], Exam.prototype, "examId", void 0);
    __decorate([
        typeorm_1.Column("date", { name: "exam_date" }),
        __metadata("design:type", String)
    ], Exam.prototype, "examDate", void 0);
    __decorate([
        typeorm_1.Column("varchar", { name: "diagnosis", nullable: true, length: 500 }),
        __metadata("design:type", String)
    ], Exam.prototype, "diagnosis", void 0);
    __decorate([
        typeorm_1.Column("int", { name: "doctor_id" }),
        __metadata("design:type", Number)
    ], Exam.prototype, "doctorId", void 0);
    __decorate([
        typeorm_1.Column("varchar", { name: "fiscal_code", nullable: true, length: 16 }),
        __metadata("design:type", String)
    ], Exam.prototype, "fiscalCode", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return Doctor_1.Doctor; }, function (doctor) { return doctor.exams; }, {
            onDelete: "NO ACTION",
            onUpdate: "NO ACTION",
        }),
        typeorm_1.JoinColumn([{ name: "doctor_id", referencedColumnName: "doctorId" }]),
        __metadata("design:type", Doctor_1.Doctor)
    ], Exam.prototype, "doctor", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return Patient_1.Patient; }, function (patient) { return patient.exams; }, {
            onDelete: "NO ACTION",
            onUpdate: "NO ACTION",
        }),
        typeorm_1.JoinColumn([{ name: "fiscal_code", referencedColumnName: "fiscalCode" }]),
        __metadata("design:type", Patient_1.Patient)
    ], Exam.prototype, "fiscalCode2", void 0);
    __decorate([
        typeorm_1.OneToMany(function () { return FieldExam_1.FieldExam; }, function (fieldExam) { return fieldExam.exam; }),
        __metadata("design:type", Array)
    ], Exam.prototype, "fieldExams", void 0);
    Exam = __decorate([
        typeorm_1.Index("fiscal_code", ["fiscalCode"], {}),
        typeorm_1.Index("exam_ibfk_1", ["doctorId"], {}),
        typeorm_1.Entity("exam", { schema: "rhinocyt" })
    ], Exam);
    return Exam;
}());
exports.Exam = Exam;
