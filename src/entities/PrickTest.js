"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PrickTest = void 0;
var typeorm_1 = require("typeorm");
var Patient_1 = require("./Patient");
var PrickTest = /** @class */ (function () {
    function PrickTest() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn({ type: "int", name: "prick_test_id" }),
        __metadata("design:type", Number)
    ], PrickTest.prototype, "prickTestId", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "positive", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], PrickTest.prototype, "positive", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "perennial_allergen", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], PrickTest.prototype, "perennialAllergen", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "poplar", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], PrickTest.prototype, "poplar", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "hazel", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], PrickTest.prototype, "hazel", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "common_ash", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], PrickTest.prototype, "commonAsh", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "willow", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], PrickTest.prototype, "willow", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "alder", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], PrickTest.prototype, "alder", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "cupressacee", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], PrickTest.prototype, "cupressacee", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "oak_tree", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], PrickTest.prototype, "oakTree", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "black_hornbeam", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], PrickTest.prototype, "blackHornbeam", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "plane_tree", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], PrickTest.prototype, "planeTree", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "grasses", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], PrickTest.prototype, "grasses", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "flowering_ash", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], PrickTest.prototype, "floweringAsh", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "pinaceae", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], PrickTest.prototype, "pinaceae", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "buckwheat", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], PrickTest.prototype, "buckwheat", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "urticaceae", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], PrickTest.prototype, "urticaceae", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "plantain", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], PrickTest.prototype, "plantain", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "birch", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], PrickTest.prototype, "birch", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "chestnut", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], PrickTest.prototype, "chestnut", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "absinthe", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], PrickTest.prototype, "absinthe", void 0);
    __decorate([
        typeorm_1.OneToMany(function () { return Patient_1.Patient; }, function (patient) { return patient.prickTest; }),
        __metadata("design:type", Array)
    ], PrickTest.prototype, "patients", void 0);
    PrickTest = __decorate([
        typeorm_1.Entity("prick_test", { schema: "rhinocyt" })
    ], PrickTest);
    return PrickTest;
}());
exports.PrickTest = PrickTest;
