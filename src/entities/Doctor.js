"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Doctor = void 0;
var typeorm_1 = require("typeorm");
var Exam_1 = require("./Exam");
var Patient_1 = require("./Patient");
var Doctor = /** @class */ (function () {
    function Doctor() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn({ type: "int", name: "doctor_id" }),
        __metadata("design:type", Number)
    ], Doctor.prototype, "doctorId", void 0);
    __decorate([
        typeorm_1.Column("varchar", { name: "name", length: 30 }),
        __metadata("design:type", String)
    ], Doctor.prototype, "name", void 0);
    __decorate([
        typeorm_1.Column("varchar", { name: "surname", length: 30 }),
        __metadata("design:type", String)
    ], Doctor.prototype, "surname", void 0);
    __decorate([
        typeorm_1.Column("varchar", { name: "email", unique: true, length: 50 }),
        __metadata("design:type", String)
    ], Doctor.prototype, "email", void 0);
    __decorate([
        typeorm_1.Column("char", { name: "password", length: 128 }),
        __metadata("design:type", String)
    ], Doctor.prototype, "password", void 0);
    __decorate([
        typeorm_1.Column("varchar", {
            name: "twoFactorAuthenticationCode",
            nullable: true,
            length: 65,
        }),
        __metadata("design:type", String)
    ], Doctor.prototype, "twoFactorAuthenticationCode", void 0);
    __decorate([
        typeorm_1.OneToMany(function () { return Exam_1.Exam; }, function (exam) { return exam.doctor; }),
        __metadata("design:type", Array)
    ], Doctor.prototype, "exams", void 0);
    __decorate([
        typeorm_1.OneToMany(function () { return Patient_1.Patient; }, function (patient) { return patient.doctor; }),
        __metadata("design:type", Array)
    ], Doctor.prototype, "patients", void 0);
    Doctor = __decorate([
        typeorm_1.Index("email", ["email"], { unique: true }),
        typeorm_1.Entity("doctor", { schema: "rhinocyt" })
    ], Doctor);
    return Doctor;
}());
exports.Doctor = Doctor;
