import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Cells } from "./Cells";
import { Exam } from "./Exam";

@Index("field_exam_ibfk_1", ["examId"], {})
@Entity("field_exam", { schema: "rhinocyt" })
export class FieldExam {
  @PrimaryGeneratedColumn({ type: "int", name: "field_id" })
  fieldId: number;

  @Column("varchar", { name: "photo", length: 400 })
  photo: string;

  @Column("int", { name: "exam_id" })
  examId: number;

  @OneToMany(() => Cells, (cells) => cells.field)
  cells: Cells[];

  @ManyToOne(() => Exam, (exam) => exam.fieldExams, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "exam_id", referencedColumnName: "examId" }])
  exam: Exam;
}
