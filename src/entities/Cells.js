"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Cells = void 0;
var typeorm_1 = require("typeorm");
var FieldExam_1 = require("./FieldExam");
var CellType_1 = require("./CellType");
var Cells = /** @class */ (function () {
    function Cells() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn({ type: "int", name: "cell_id" }),
        __metadata("design:type", Number)
    ], Cells.prototype, "cellId", void 0);
    __decorate([
        typeorm_1.Column("varchar", { name: "cell_photo", length: 400 }),
        __metadata("design:type", String)
    ], Cells.prototype, "cellPhoto", void 0);
    __decorate([
        typeorm_1.Column("int", { name: "cell_type_id" }),
        __metadata("design:type", Number)
    ], Cells.prototype, "cellTypeId", void 0);
    __decorate([
        typeorm_1.Column("int", { name: "field_id" }),
        __metadata("design:type", Number)
    ], Cells.prototype, "fieldId", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return FieldExam_1.FieldExam; }, function (fieldExam) { return fieldExam.cells; }, {
            onDelete: "NO ACTION",
            onUpdate: "NO ACTION",
        }),
        typeorm_1.JoinColumn([{ name: "field_id", referencedColumnName: "fieldId" }]),
        __metadata("design:type", FieldExam_1.FieldExam)
    ], Cells.prototype, "field", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return CellType_1.CellType; }, function (cellType) { return cellType.cells; }, {
            onDelete: "NO ACTION",
            onUpdate: "NO ACTION",
        }),
        typeorm_1.JoinColumn([{ name: "cell_type_id", referencedColumnName: "cellTypeId" }]),
        __metadata("design:type", CellType_1.CellType)
    ], Cells.prototype, "cellType", void 0);
    Cells = __decorate([
        typeorm_1.Index("cells_ibfk_1_idx", ["fieldId"], {}),
        typeorm_1.Index("cells_ibfk_1_idx1", ["cellTypeId"], {}),
        typeorm_1.Entity("cells", { schema: "rhinocyt" })
    ], Cells);
    return Cells;
}());
exports.Cells = Cells;
