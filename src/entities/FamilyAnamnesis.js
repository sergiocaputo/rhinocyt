"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FamilyAnamnesis = void 0;
var typeorm_1 = require("typeorm");
var Patient_1 = require("./Patient");
var FamilyAnamnesis = /** @class */ (function () {
    function FamilyAnamnesis() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn({ type: "int", name: "anamnesis_id" }),
        __metadata("design:type", Number)
    ], FamilyAnamnesis.prototype, "anamnesisId", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "parents_food_allergy", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], FamilyAnamnesis.prototype, "parentsFoodAllergy", void 0);
    __decorate([
        typeorm_1.Column("tinyint", {
            name: "parents_inhalant_allergy",
            nullable: true,
            width: 1,
        }),
        __metadata("design:type", Boolean)
    ], FamilyAnamnesis.prototype, "parentsInhalantAllergy", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "parents_polyposis", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], FamilyAnamnesis.prototype, "parentsPolyposis", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "sibling_polyposis", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], FamilyAnamnesis.prototype, "siblingPolyposis", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "parents_asthma", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], FamilyAnamnesis.prototype, "parentsAsthma", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "silibing_asthma", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], FamilyAnamnesis.prototype, "silibingAsthma", void 0);
    __decorate([
        typeorm_1.Column("tinyint", {
            name: "silibing_food_allergy",
            nullable: true,
            width: 1,
        }),
        __metadata("design:type", Boolean)
    ], FamilyAnamnesis.prototype, "silibingFoodAllergy", void 0);
    __decorate([
        typeorm_1.Column("tinyint", {
            name: "silibing_inhalant_allergy",
            nullable: true,
            width: 1,
        }),
        __metadata("design:type", Boolean)
    ], FamilyAnamnesis.prototype, "silibingInhalantAllergy", void 0);
    __decorate([
        typeorm_1.Column("varchar", {
            name: "family_medical_notes",
            nullable: true,
            length: 500,
        }),
        __metadata("design:type", String)
    ], FamilyAnamnesis.prototype, "familyMedicalNotes", void 0);
    __decorate([
        typeorm_1.OneToMany(function () { return Patient_1.Patient; }, function (patient) { return patient.anamnesis; }),
        __metadata("design:type", Array)
    ], FamilyAnamnesis.prototype, "patients", void 0);
    FamilyAnamnesis = __decorate([
        typeorm_1.Entity("family_anamnesis", { schema: "rhinocyt" })
    ], FamilyAnamnesis);
    return FamilyAnamnesis;
}());
exports.FamilyAnamnesis = FamilyAnamnesis;
