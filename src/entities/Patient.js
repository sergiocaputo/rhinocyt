"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Patient = void 0;
var typeorm_1 = require("typeorm");
var Exam_1 = require("./Exam");
var FamilyAnamnesis_1 = require("./FamilyAnamnesis");
var Doctor_1 = require("./Doctor");
var Symptomatology_1 = require("./Symptomatology");
var PrickTest_1 = require("./PrickTest");
var MedicalExam_1 = require("./MedicalExam");
var Patient = /** @class */ (function () {
    function Patient() {
    }
    __decorate([
        typeorm_1.Column("varchar", { primary: true, name: "fiscal_code", length: 16 }),
        __metadata("design:type", String)
    ], Patient.prototype, "fiscalCode", void 0);
    __decorate([
        typeorm_1.Column("varchar", { name: "name", length: 30 }),
        __metadata("design:type", String)
    ], Patient.prototype, "name", void 0);
    __decorate([
        typeorm_1.Column("varchar", { name: "surname", length: 30 }),
        __metadata("design:type", String)
    ], Patient.prototype, "surname", void 0);
    __decorate([
        typeorm_1.Column("char", { name: "gender", length: 1 }),
        __metadata("design:type", String)
    ], Patient.prototype, "gender", void 0);
    __decorate([
        typeorm_1.Column("date", { name: "birth_date" }),
        __metadata("design:type", String)
    ], Patient.prototype, "birthDate", void 0);
    __decorate([
        typeorm_1.Column("int", { name: "anamnesis_id", nullable: true }),
        __metadata("design:type", Number)
    ], Patient.prototype, "anamnesisId", void 0);
    __decorate([
        typeorm_1.Column("int", { name: "doctor_id" }),
        __metadata("design:type", Number)
    ], Patient.prototype, "doctorId", void 0);
    __decorate([
        typeorm_1.Column("int", { name: "symptomatology_id", nullable: true }),
        __metadata("design:type", Number)
    ], Patient.prototype, "symptomatologyId", void 0);
    __decorate([
        typeorm_1.Column("int", { name: "prick_test_id", nullable: true }),
        __metadata("design:type", Number)
    ], Patient.prototype, "prickTestId", void 0);
    __decorate([
        typeorm_1.Column("int", { name: "medical_exam_id", nullable: true }),
        __metadata("design:type", Number)
    ], Patient.prototype, "medicalExamId", void 0);
    __decorate([
        typeorm_1.OneToMany(function () { return Exam_1.Exam; }, function (exam) { return exam.fiscalCode2; }),
        __metadata("design:type", Array)
    ], Patient.prototype, "exams", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return FamilyAnamnesis_1.FamilyAnamnesis; }, function (familyAnamnesis) { return familyAnamnesis.patients; }, { onDelete: "NO ACTION", onUpdate: "NO ACTION" }),
        typeorm_1.JoinColumn([{ name: "anamnesis_id", referencedColumnName: "anamnesisId" }]),
        __metadata("design:type", FamilyAnamnesis_1.FamilyAnamnesis)
    ], Patient.prototype, "anamnesis", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return Doctor_1.Doctor; }, function (doctor) { return doctor.patients; }, {
            onDelete: "NO ACTION",
            onUpdate: "NO ACTION",
        }),
        typeorm_1.JoinColumn([{ name: "doctor_id", referencedColumnName: "doctorId" }]),
        __metadata("design:type", Doctor_1.Doctor)
    ], Patient.prototype, "doctor", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return Symptomatology_1.Symptomatology; }, function (symptomatology) { return symptomatology.patients; }, { onDelete: "NO ACTION", onUpdate: "NO ACTION" }),
        typeorm_1.JoinColumn([
            { name: "symptomatology_id", referencedColumnName: "symptomatologyId" },
        ]),
        __metadata("design:type", Symptomatology_1.Symptomatology)
    ], Patient.prototype, "symptomatology", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return PrickTest_1.PrickTest; }, function (prickTest) { return prickTest.patients; }, {
            onDelete: "NO ACTION",
            onUpdate: "NO ACTION",
        }),
        typeorm_1.JoinColumn([{ name: "prick_test_id", referencedColumnName: "prickTestId" }]),
        __metadata("design:type", PrickTest_1.PrickTest)
    ], Patient.prototype, "prickTest", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return MedicalExam_1.MedicalExam; }, function (medicalExam) { return medicalExam.patients; }, {
            onDelete: "NO ACTION",
            onUpdate: "NO ACTION",
        }),
        typeorm_1.JoinColumn([
            { name: "medical_exam_id", referencedColumnName: "medicalExamId" },
        ]),
        __metadata("design:type", MedicalExam_1.MedicalExam)
    ], Patient.prototype, "medicalExam", void 0);
    Patient = __decorate([
        typeorm_1.Index("patient_ibfk_1", ["anamnesisId"], {}),
        typeorm_1.Index("patient_ibfk_2", ["doctorId"], {}),
        typeorm_1.Index("patient_ibfk_3", ["symptomatologyId"], {}),
        typeorm_1.Index("patient_ibfk_4", ["prickTestId"], {}),
        typeorm_1.Index("patient_ibfk_5", ["medicalExamId"], {}),
        typeorm_1.Entity("patient", { schema: "rhinocyt" })
    ], Patient);
    return Patient;
}());
exports.Patient = Patient;
