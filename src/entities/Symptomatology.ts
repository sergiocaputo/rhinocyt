import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Patient } from "./Patient";

@Entity("symptomatology", { schema: "rhinocyt" })
export class Symptomatology {
  @PrimaryGeneratedColumn({ type: "int", name: "symptomatology_id" })
  symptomatologyId: number;

  @Column("tinyint", { name: "obstruction", nullable: true, width: 1 })
  obstruction: boolean | null;

  @Column("tinyint", { name: "hypoacusis", nullable: true, width: 1 })
  hypoacusis: boolean | null;

  @Column("tinyint", { name: "rhinorrhea", nullable: true, width: 1 })
  rhinorrhea: boolean | null;

  @Column("tinyint", { name: "sneezing", nullable: true, width: 1 })
  sneezing: boolean | null;

  @Column("tinyint", { name: "olfactory_problems", nullable: true, width: 1 })
  olfactoryProblems: boolean | null;

  @Column("tinyint", { name: "auricular_padding", nullable: true, width: 1 })
  auricularPadding: boolean | null;

  @Column("tinyint", { name: "tinnitus", nullable: true, width: 1 })
  tinnitus: boolean | null;

  @Column("tinyint", {
    name: "vertiginous__syndrome",
    nullable: true,
    width: 1,
  })
  vertiginousSyndrome: boolean | null;

  @Column("varchar", { name: "doctor_notes", nullable: true, length: 500 })
  doctorNotes: string | null;

  @Column("tinyint", { name: "tearing", nullable: true, width: 1 })
  tearing: boolean | null;

  @Column("tinyint", { name: "photophobia_", nullable: true, width: 1 })
  photophobia: boolean | null;

  @Column("tinyint", { name: "conjunctive_itching", nullable: true, width: 1 })
  conjunctiveItching: boolean | null;

  @Column("tinyint", { name: "conjunctiva_burning", nullable: true, width: 1 })
  conjunctivaBurning: boolean | null;

  @Column("tinyint", { name: "nasal_itching", nullable: true, width: 1 })
  nasalItching: boolean | null;

  @Column("tinyint", { name: "medicine_use", nullable: true, width: 1 })
  medicineUse: boolean | null;

  @Column("tinyint", { name: "fever", nullable: true, width: 1 })
  fever: boolean | null;

  @OneToMany(() => Patient, (patient) => patient.symptomatology)
  patients: Patient[];
}
