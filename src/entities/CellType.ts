import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Cells } from "./Cells";

@Entity("cell_type", { schema: "rhinocyt" })
export class CellType {
  @PrimaryGeneratedColumn({ type: "int", name: "cell_type_id" })
  cellTypeId: number;

  @Column("varchar", { name: "type", length: 45 })
  type: string;

  @OneToMany(() => Cells, (cells) => cells.cellType)
  cells: Cells[];
}
