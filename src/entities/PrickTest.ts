import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Patient } from "./Patient";

@Entity("prick_test", { schema: "rhinocyt" })
export class PrickTest {
  @PrimaryGeneratedColumn({ type: "int", name: "prick_test_id" })
  prickTestId: number;

  @Column("tinyint", { name: "positive", nullable: true, width: 1 })
  positive: boolean | null;

  @Column("tinyint", { name: "perennial_allergen", nullable: true, width: 1 })
  perennialAllergen: boolean | null;

  @Column("tinyint", { name: "poplar", nullable: true, width: 1 })
  poplar: boolean | null;

  @Column("tinyint", { name: "hazel", nullable: true, width: 1 })
  hazel: boolean | null;

  @Column("tinyint", { name: "common_ash", nullable: true, width: 1 })
  commonAsh: boolean | null;

  @Column("tinyint", { name: "willow", nullable: true, width: 1 })
  willow: boolean | null;

  @Column("tinyint", { name: "alder", nullable: true, width: 1 })
  alder: boolean | null;

  @Column("tinyint", { name: "cupressacee", nullable: true, width: 1 })
  cupressacee: boolean | null;

  @Column("tinyint", { name: "oak_tree", nullable: true, width: 1 })
  oakTree: boolean | null;

  @Column("tinyint", { name: "black_hornbeam", nullable: true, width: 1 })
  blackHornbeam: boolean | null;

  @Column("tinyint", { name: "plane_tree", nullable: true, width: 1 })
  planeTree: boolean | null;

  @Column("tinyint", { name: "grasses", nullable: true, width: 1 })
  grasses: boolean | null;

  @Column("tinyint", { name: "flowering_ash", nullable: true, width: 1 })
  floweringAsh: boolean | null;

  @Column("tinyint", { name: "pinaceae", nullable: true, width: 1 })
  pinaceae: boolean | null;

  @Column("tinyint", { name: "buckwheat", nullable: true, width: 1 })
  buckwheat: boolean | null;

  @Column("tinyint", { name: "urticaceae", nullable: true, width: 1 })
  urticaceae: boolean | null;

  @Column("tinyint", { name: "plantain", nullable: true, width: 1 })
  plantain: boolean | null;

  @Column("tinyint", { name: "birch", nullable: true, width: 1 })
  birch: boolean | null;

  @Column("tinyint", { name: "chestnut", nullable: true, width: 1 })
  chestnut: boolean | null;

  @Column("tinyint", { name: "absinthe", nullable: true, width: 1 })
  absinthe: boolean | null;

  @OneToMany(() => Patient, (patient) => patient.prickTest)
  patients: Patient[];
}
