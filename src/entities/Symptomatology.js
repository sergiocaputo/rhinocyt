"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Symptomatology = void 0;
var typeorm_1 = require("typeorm");
var Patient_1 = require("./Patient");
var Symptomatology = /** @class */ (function () {
    function Symptomatology() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn({ type: "int", name: "symptomatology_id" }),
        __metadata("design:type", Number)
    ], Symptomatology.prototype, "symptomatologyId", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "obstruction", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], Symptomatology.prototype, "obstruction", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "hypoacusis", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], Symptomatology.prototype, "hypoacusis", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "rhinorrhea", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], Symptomatology.prototype, "rhinorrhea", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "sneezing", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], Symptomatology.prototype, "sneezing", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "olfactory_problems", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], Symptomatology.prototype, "olfactoryProblems", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "auricular_padding", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], Symptomatology.prototype, "auricularPadding", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "tinnitus", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], Symptomatology.prototype, "tinnitus", void 0);
    __decorate([
        typeorm_1.Column("tinyint", {
            name: "vertiginous__syndrome",
            nullable: true,
            width: 1,
        }),
        __metadata("design:type", Boolean)
    ], Symptomatology.prototype, "vertiginousSyndrome", void 0);
    __decorate([
        typeorm_1.Column("varchar", { name: "doctor_notes", nullable: true, length: 500 }),
        __metadata("design:type", String)
    ], Symptomatology.prototype, "doctorNotes", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "tearing", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], Symptomatology.prototype, "tearing", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "photophobia_", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], Symptomatology.prototype, "photophobia", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "conjunctive_itching", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], Symptomatology.prototype, "conjunctiveItching", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "conjunctiva_burning", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], Symptomatology.prototype, "conjunctivaBurning", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "nasal_itching", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], Symptomatology.prototype, "nasalItching", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "medicine_use", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], Symptomatology.prototype, "medicineUse", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "fever", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], Symptomatology.prototype, "fever", void 0);
    __decorate([
        typeorm_1.OneToMany(function () { return Patient_1.Patient; }, function (patient) { return patient.symptomatology; }),
        __metadata("design:type", Array)
    ], Symptomatology.prototype, "patients", void 0);
    Symptomatology = __decorate([
        typeorm_1.Entity("symptomatology", { schema: "rhinocyt" })
    ], Symptomatology);
    return Symptomatology;
}());
exports.Symptomatology = Symptomatology;
