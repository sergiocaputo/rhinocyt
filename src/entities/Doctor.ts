import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Exam } from "./Exam";
import { Patient } from "./Patient";

@Index("email", ["email"], { unique: true })
@Entity("doctor", { schema: "rhinocyt" })
export class Doctor {
  @PrimaryGeneratedColumn({ type: "int", name: "doctor_id" })
  doctorId: number;

  @Column("varchar", { name: "name", length: 30 })
  name: string;

  @Column("varchar", { name: "surname", length: 30 })
  surname: string;

  @Column("varchar", { name: "email", unique: true, length: 50 })
  email: string;

  @Column("char", { name: "password", length: 128 })
  password: string;

  @Column("varchar", {
    name: "twoFactorAuthenticationCode",
    nullable: true,
    length: 65,
  })
  twoFactorAuthenticationCode: string | null;

  @OneToMany(() => Exam, (exam) => exam.doctor)
  exams: Exam[];

  @OneToMany(() => Patient, (patient) => patient.doctor)
  patients: Patient[];
}
