import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from "typeorm";
import { Exam } from "./Exam";
import { FamilyAnamnesis } from "./FamilyAnamnesis";
import { Doctor } from "./Doctor";
import { Symptomatology } from "./Symptomatology";
import { PrickTest } from "./PrickTest";
import { MedicalExam } from "./MedicalExam";

@Index("patient_ibfk_1", ["anamnesisId"], {})
@Index("patient_ibfk_2", ["doctorId"], {})
@Index("patient_ibfk_3", ["symptomatologyId"], {})
@Index("patient_ibfk_4", ["prickTestId"], {})
@Index("patient_ibfk_5", ["medicalExamId"], {})
@Entity("patient", { schema: "rhinocyt" })
export class Patient {
  @Column("varchar", { primary: true, name: "fiscal_code", length: 16 })
  fiscalCode: string;

  @Column("varchar", { name: "name", length: 30 })
  name: string;

  @Column("varchar", { name: "surname", length: 30 })
  surname: string;

  @Column("char", { name: "gender", length: 1 })
  gender: string;

  @Column("date", { name: "birth_date" })
  birthDate: string;

  @Column("int", { name: "anamnesis_id", nullable: true })
  anamnesisId: number | null;

  @Column("int", { name: "doctor_id" })
  doctorId: number;

  @Column("int", { name: "symptomatology_id", nullable: true })
  symptomatologyId: number | null;

  @Column("int", { name: "prick_test_id", nullable: true })
  prickTestId: number | null;

  @Column("int", { name: "medical_exam_id", nullable: true })
  medicalExamId: number | null;

  @OneToMany(() => Exam, (exam) => exam.fiscalCode2)
  exams: Exam[];

  @ManyToOne(
    () => FamilyAnamnesis,
    (familyAnamnesis) => familyAnamnesis.patients,
    { onDelete: "NO ACTION", onUpdate: "NO ACTION" }
  )
  @JoinColumn([{ name: "anamnesis_id", referencedColumnName: "anamnesisId" }])
  anamnesis: FamilyAnamnesis;

  @ManyToOne(() => Doctor, (doctor) => doctor.patients, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "doctor_id", referencedColumnName: "doctorId" }])
  doctor: Doctor;

  @ManyToOne(
    () => Symptomatology,
    (symptomatology) => symptomatology.patients,
    { onDelete: "NO ACTION", onUpdate: "NO ACTION" }
  )
  @JoinColumn([
    { name: "symptomatology_id", referencedColumnName: "symptomatologyId" },
  ])
  symptomatology: Symptomatology;

  @ManyToOne(() => PrickTest, (prickTest) => prickTest.patients, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "prick_test_id", referencedColumnName: "prickTestId" }])
  prickTest: PrickTest;

  @ManyToOne(() => MedicalExam, (medicalExam) => medicalExam.patients, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([
    { name: "medical_exam_id", referencedColumnName: "medicalExamId" },
  ])
  medicalExam: MedicalExam;
}
