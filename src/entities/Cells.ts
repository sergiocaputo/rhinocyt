import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { FieldExam } from "./FieldExam";
import { CellType } from "./CellType";

@Index("cells_ibfk_1_idx", ["fieldId"], {})
@Index("cells_ibfk_1_idx1", ["cellTypeId"], {})
@Entity("cells", { schema: "rhinocyt" })
export class Cells {
  @PrimaryGeneratedColumn({ type: "int", name: "cell_id" })
  cellId: number;

  @Column("varchar", { name: "cell_photo", length: 400 })
  cellPhoto: string;

  @Column("int", { name: "cell_type_id" })
  cellTypeId: number;

  @Column("int", { name: "field_id" })
  fieldId: number;

  @ManyToOne(() => FieldExam, (fieldExam) => fieldExam.cells, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "field_id", referencedColumnName: "fieldId" }])
  field: FieldExam;

  @ManyToOne(() => CellType, (cellType) => cellType.cells, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "cell_type_id", referencedColumnName: "cellTypeId" }])
  cellType: CellType;
}
