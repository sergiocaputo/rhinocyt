"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MedicalExam = void 0;
var typeorm_1 = require("typeorm");
var Patient_1 = require("./Patient");
var MedicalExam = /** @class */ (function () {
    function MedicalExam() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn({ type: "int", name: "medical_exam_id" }),
        __metadata("design:type", Number)
    ], MedicalExam.prototype, "medicalExamId", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "nasal_pyramid", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], MedicalExam.prototype, "nasalPyramid", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "nasal_valve", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], MedicalExam.prototype, "nasalValve", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "nasal_septum", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], MedicalExam.prototype, "nasalSeptum", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "turbinates", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], MedicalExam.prototype, "turbinates", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "nasal_polyposis_sx", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], MedicalExam.prototype, "nasalPolyposisSx", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "nasal_polyposis_dx", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], MedicalExam.prototype, "nasalPolyposisDx", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "exudate", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], MedicalExam.prototype, "exudate", void 0);
    __decorate([
        typeorm_1.Column("tinyint", { name: "adenoid_hypertrophy", nullable: true, width: 1 }),
        __metadata("design:type", Boolean)
    ], MedicalExam.prototype, "adenoidHypertrophy", void 0);
    __decorate([
        typeorm_1.Column("varchar", { name: "alteration_notes", nullable: true, length: 500 }),
        __metadata("design:type", String)
    ], MedicalExam.prototype, "alterationNotes", void 0);
    __decorate([
        typeorm_1.Column("varchar", { name: "ear_exam", nullable: true, length: 500 }),
        __metadata("design:type", String)
    ], MedicalExam.prototype, "earExam", void 0);
    __decorate([
        typeorm_1.Column("float", { name: "rino_base_sx", nullable: true, precision: 12 }),
        __metadata("design:type", Number)
    ], MedicalExam.prototype, "rinoBaseSx", void 0);
    __decorate([
        typeorm_1.Column("float", { name: "rino_base_dx", nullable: true, precision: 12 }),
        __metadata("design:type", Number)
    ], MedicalExam.prototype, "rinoBaseDx", void 0);
    __decorate([
        typeorm_1.Column("float", { name: "rino_base_sx_dx", nullable: true, precision: 12 }),
        __metadata("design:type", Number)
    ], MedicalExam.prototype, "rinoBaseSxDx", void 0);
    __decorate([
        typeorm_1.Column("float", { name: "decong_base_sx", nullable: true, precision: 12 }),
        __metadata("design:type", Number)
    ], MedicalExam.prototype, "decongBaseSx", void 0);
    __decorate([
        typeorm_1.Column("float", { name: "decong_base_dx", nullable: true, precision: 12 }),
        __metadata("design:type", Number)
    ], MedicalExam.prototype, "decongBaseDx", void 0);
    __decorate([
        typeorm_1.Column("float", { name: "decong_base_sx_dx", nullable: true, precision: 12 }),
        __metadata("design:type", Number)
    ], MedicalExam.prototype, "decongBaseSxDx", void 0);
    __decorate([
        typeorm_1.Column("varchar", { name: "conclusions", nullable: true, length: 500 }),
        __metadata("design:type", String)
    ], MedicalExam.prototype, "conclusions", void 0);
    __decorate([
        typeorm_1.OneToMany(function () { return Patient_1.Patient; }, function (patient) { return patient.medicalExam; }),
        __metadata("design:type", Array)
    ], MedicalExam.prototype, "patients", void 0);
    MedicalExam = __decorate([
        typeorm_1.Entity("medical_exam", { schema: "rhinocyt" })
    ], MedicalExam);
    return MedicalExam;
}());
exports.MedicalExam = MedicalExam;
