import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Doctor } from "./Doctor";
import { Patient } from "./Patient";
import { FieldExam } from "./FieldExam";

@Index("fiscal_code", ["fiscalCode"], {})
@Index("exam_ibfk_1", ["doctorId"], {})
@Entity("exam", { schema: "rhinocyt" })
export class Exam {
  @PrimaryGeneratedColumn({ type: "int", name: "exam_id" })
  examId: number;

  @Column("date", { name: "exam_date" })
  examDate: string;

  @Column("varchar", { name: "diagnosis", nullable: true, length: 500 })
  diagnosis: string | null;

  @Column("int", { name: "doctor_id" })
  doctorId: number;

  @Column("varchar", { name: "fiscal_code", nullable: true, length: 16 })
  fiscalCode: string | null;

  @ManyToOne(() => Doctor, (doctor) => doctor.exams, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "doctor_id", referencedColumnName: "doctorId" }])
  doctor: Doctor;

  @ManyToOne(() => Patient, (patient) => patient.exams, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "fiscal_code", referencedColumnName: "fiscalCode" }])
  fiscalCode2: Patient;

  @OneToMany(() => FieldExam, (fieldExam) => fieldExam.exam)
  fieldExams: FieldExam[];
}
