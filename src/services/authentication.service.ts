import * as speakeasy from 'speakeasy';
import * as QRCode from 'qrcode';
import {Response} from 'express';
import User from '../interfaces/User.interface';
import { DataStoredInToken } from '../interfaces/DataStoredInToken.interface';
import { TokenData } from '../interfaces/TokenData.interface';

let jwt = require('jsonwebtoken');

export const respondWithQRCode = (data: string, response: Response) => {
  QRCode.toDataURL(data,function(err, dataURL){
    response.send('<img src="' + dataURL + '">')
  });
}

export const getTwoFactorAuthenticationCode = () => {
  const secretCode = speakeasy.generateSecret({
    name: process.env.TWO_FACTOR_AUTHENTICATION_APP_NAME,
  });
  return {
    otpauthUrl : secretCode.otpauth_url,
    base32: secretCode.base32,
  };
}

export const verifyTwoFactorAuthenticationCode = (twoFactorAuthenticationCode: string, user: User) => {
  return speakeasy.totp.verify({
    secret: user.twoFactorAuthenticationCode,
    encoding: 'base32',
    token: twoFactorAuthenticationCode,
    window: 3 // 2FAcode from Google Authenticator can be used within 180 seconds from its generation
  });
}

export const createToken = (doctorId: number, isSecondFactorAuthenticated = false): TokenData => {
  const expiresIn = 60 * 60 * 8; // 8 hours
  const secret = process.env.JWT_SECRET;

  const dataStoredInToken: DataStoredInToken = {
    _id: doctorId,
    isSecondFactorAuthenticated,
  };
  return {
    expiresIn,
    token: jwt.sign(dataStoredInToken, secret, { expiresIn }),
  };
}

export const createCookie = (tokenData: TokenData) =>{
  return `Authorization=${tokenData.token}; HttpOnly; Max-Age=${tokenData.expiresIn}`;
}