"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createCookie = exports.createToken = exports.verifyTwoFactorAuthenticationCode = exports.getTwoFactorAuthenticationCode = exports.respondWithQRCode = void 0;
var speakeasy = require("speakeasy");
var QRCode = require("qrcode");
var jwt = require('jsonwebtoken');
exports.respondWithQRCode = function (data, response) {
    QRCode.toDataURL(data, function (err, dataURL) {
        response.send('<img src="' + dataURL + '">');
    });
};
exports.getTwoFactorAuthenticationCode = function () {
    var secretCode = speakeasy.generateSecret({
        name: process.env.TWO_FACTOR_AUTHENTICATION_APP_NAME,
    });
    return {
        otpauthUrl: secretCode.otpauth_url,
        base32: secretCode.base32,
    };
};
exports.verifyTwoFactorAuthenticationCode = function (twoFactorAuthenticationCode, user) {
    return speakeasy.totp.verify({
        secret: user.twoFactorAuthenticationCode,
        encoding: 'base32',
        token: twoFactorAuthenticationCode,
        window: 3 // 2FAcode from Google Authenticator can be used within 180 seconds from its generation
    });
};
exports.createToken = function (doctorId, isSecondFactorAuthenticated) {
    if (isSecondFactorAuthenticated === void 0) { isSecondFactorAuthenticated = false; }
    var expiresIn = 60 * 60 * 8; // 8 hours
    var secret = process.env.JWT_SECRET;
    var dataStoredInToken = {
        _id: doctorId,
        isSecondFactorAuthenticated: isSecondFactorAuthenticated,
    };
    return {
        expiresIn: expiresIn,
        token: jwt.sign(dataStoredInToken, secret, { expiresIn: expiresIn }),
    };
};
exports.createCookie = function (tokenData) {
    return "Authorization=" + tokenData.token + "; HttpOnly; Max-Age=" + tokenData.expiresIn;
};
