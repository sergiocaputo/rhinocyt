# Import libraries and modules
import cv2
import os
import numpy as np
import shutil
import sys
import shutil

from keras.models import load_model


def get_class_name(classNum):
    # convert the cell-class number into its name string.
    if classNum == 0:
        return "epithelium"
    if classNum == 1:
        return "neutrophil"
    if classNum == 2:
        return "eosinophil"
    if classNum == 3:
        return "mastocyte"
    if classNum == 4:
        return "lymphocyte"
    if classNum == 5:
        return "mucipara"
    if classNum == 6:
        return "other"

def load_data(data_directory):
    """
    Loading images and labels from directory
    :param data_directory:
    :return: images, labels
    """
 #   directories = [d for d in os.listdir(data_directory)
 #                  if os.path.isdir(os.path.join(data_directory, d))]
    images = []
    
#    for d in directories:
#        label_directory = os.path.join(data_directory, d)
#        file_names = [os.path.join(label_directory, f)
#                      for f in os.listdir(label_directory)
#                      if f.endswith('.png') or f.endswith('.PNG')]
    file_names = [os.path.join(data_directory, f)
                  for f in os.listdir(data_directory)
                  if f.endswith('.png') or f.endswith('.PNG')]

    for f in file_names:
        img = cv2.imread(f)
        (b, g, r) = cv2.split(img)
        img = cv2.merge([r, g, b])
        img = cv2.resize(img, (50, 50))
        images.append(img)

    return images,file_names


# load the trained model.
model=load_model(sys.argv[1] + "/" + 'my_model.h5')
# load the best weights for the model.
model.load_weights(sys.argv[1] + "/" + 'weights.best.hdf5')
model.compile(optimizer='rmsprop', loss='categorical_crossentropy')

ROOT_PATH = sys.argv[1] + "/"
data_directory = os.path.join(ROOT_PATH, "cells/cell_examid_"+ sys.argv[2] + "/")

img_list,file_names = load_data(data_directory)

img_list = np.array(img_list)
img_list = img_list.astype("float32")
img_list /= 255
img_list = img_list.reshape(img_list.shape[0], 50, 50, 3)

i = 0 
if (img_list.size == 0):
	print("No images found")
for img in img_list:
    img = img.reshape((1,)+img.shape)
    img_class = model.predict_classes(img)
#    print(get_class_name(img_class))

    # define the path of its folder-class.
    class_path = os.path.join(ROOT_PATH,"Classified_Cells/") + get_class_name(img_class) + "/"
    # move it to the correct destination.
    shutil.move(file_names[i], class_path)
    i += 1

# move images in cell dir
cell_dir = os.path.join(ROOT_PATH,"Classified_Cells/")
cell_types = [d for d in os.listdir(cell_dir)
              if os.path.isdir(os.path.join(cell_dir, d))]
for dir in cell_types:
    d = os.path.join(cell_dir, dir)
    if(os.path.exists(ROOT_PATH + "cells/cell_examid_"+ sys.argv[2] + "/"+dir)):
        shutil.rmtree(ROOT_PATH + "cells/cell_examid_"+ sys.argv[2] + "/"+dir)
    shutil.move(d, ROOT_PATH + "cells/cell_examid_"+ sys.argv[2] + "/")