# Import libraries and modules

import numpy as np
import sys
import time
import os
import json

from datetime import timedelta
'''
def save(self, *args, **kwargs):
    #control if diagnosis has already been made with the combination
    #of cell extraction and anamnesis ids
    if DiagnosisExtraction.objects.filter(cell_extraction=self.cell_extraction, anamnesis=self.anamnesis).count() > 0:
        raise Exception('The diagnosis has already been made')
    #assert facts for diagnosis calculation
    super().save(*args, **kwargs);
    self.assert_facts()
'''

def assert_facts():
    
    #defining fact templates
    cellula_template = env.find_template("cellula")
    famiglia_template = env.find_template("famiglia")
    sintomo_template = env.find_template("sintomo")
    scoperta_template = env.find_template("scoperta")
    rinomanometria_template = env.find_template("rinomanometria")
    diagnosi_template = env.find_template("diagnosi")
    prick_test_template = env.find_template("prick-test")
      
    
    #assert facts cellule   
    i = 0
    for cell_category in cells_for_type:
        cellula_fact = cellula_template.new_fact()
        cellula_fact['nome'] = clips.Symbol(cells_for_type[i]['key'].capitalize())
        cellula_fact['grado'] = int(cells_for_type[i]['value'])
        i = i + 1
        cellula_fact.assertit()

    if anamnesis != None:
        if (anamnesis['parentsFoodAllergy'] != None and anamnesis['parentsFoodAllergy'] != 0) or (anamnesis['parentsInhalantAllergy'] != None and anamnesis['parentsInhalantAllergy'] != 0):
            famiglia_fact = famiglia_template.new_fact()
            famiglia_fact['soggetto'] = clips.Symbol("genitore")
            famiglia_fact['disturbo'] = clips.Symbol("allergia")
    #       famiglia_fact['tipo'] = clips.Symbol(self.anamnesis.get_allergy_gen_display())
            famiglia_fact.assertit()
            
        if (anamnesis['silibingFoodAllergy'] != None and anamnesis['silibingFoodAllergy'] != 0) or (anamnesis['silibingInhalantAllergy'] != None and anamnesis['silibingInhalantAllergy'] != 0):
            famiglia_fact = famiglia_template.new_fact()
            famiglia_fact['soggetto'] = clips.Symbol("fratello")
            famiglia_fact['disturbo'] = clips.Symbol("allergia")
    #       famiglia_fact['tipo'] = clips.Symbol(self.anamnesis.get_allergy_fra_display())
            famiglia_fact.assertit()
            
        if anamnesis['parentsPolyposis'] == 1:
            famiglia_fact = famiglia_template.new_fact()
            famiglia_fact['soggetto'] = clips.Symbol("genitore")
            famiglia_fact['disturbo'] = clips.Symbol("poliposi")
            famiglia_fact.assertit()
            
        if anamnesis['siblingPolyposis'] == 1:
            famiglia_fact = famiglia_template.new_fact()
            famiglia_fact['soggetto'] = clips.Symbol("fratello")
            famiglia_fact['disturbo'] = clips.Symbol("poliposi")
            famiglia_fact.assertit()
            
        if anamnesis['parentsAsthma'] == 1:
            famiglia_fact = famiglia_template.new_fact()
            famiglia_fact['soggetto'] = clips.Symbol("genitore")
            famiglia_fact['disturbo'] = clips.Symbol("asma")
            famiglia_fact.assertit()
            
        if anamnesis['silibingAsthma'] == 1:
            famiglia_fact = famiglia_template.new_fact()
            famiglia_fact['soggetto'] = clips.Symbol("fratello")
            famiglia_fact['disturbo'] = clips.Symbol("asma")
            famiglia_fact.assertit()
            
    if symptomatology != None:
        if symptomatology['obstruction'] != None:
            sintomo_fact = sintomo_template.new_fact()
            if symptomatology['obstruction'] == 1:
                sintomo_fact['nome'] = clips.Symbol("Ostruzione nasale sinistra")
            elif symptomatology['obstruction'] == 2:
                sintomo_fact['nome'] = clips.Symbol("Ostruzione nasale destra")
            else:
                sintomo_fact['nome'] = clips.Symbol("Ostruzione nasale bilaterale")
            sintomo_fact.assertit()
            
        if symptomatology['rhinorrhea'] != None:
            sintomo_fact = sintomo_template.new_fact()
            if symptomatology['rhinorrhea'] == 1:
                sintomo_fact['nome'] = clips.Symbol("Rinorrea nasale sieroso")
            elif symptomatology['rhinorrhea'] == 2:
                sintomo_fact['nome'] = clips.Symbol("Rinorrea nasale mucoso")
            elif symptomatology['rhinorrhea'] == 3:
                sintomo_fact['nome'] = clips.Symbol("Rinorrea nasale purulento")
            else:
                sintomo_fact['nome'] = clips.Symbol("Rinorrea nasale ematico")
            sintomo_fact.assertit()
    #        sintomo_fact = sintomo_template.new_fact()
    #        sintomo_fact['nome'] = clips.Symbol("Espansione rinorrea: " + self.anamnesis.get_rinorrea_espans_display())
    #        sintomo_fact.assertit()
            
        if symptomatology['nasalItching'] == 1:
            sintomo_fact = sintomo_template.new_fact()
            sintomo_fact['nome'] = clips.Symbol("Prurito nasale")
            sintomo_fact.assertit()
            
        if symptomatology['sneezing'] != None:
            sintomo_fact = sintomo_template.new_fact()
            if symptomatology['sneezing'] == 1:
                sintomo_fact['nome'] = clips.Symbol("Starnutazione sporadica")
            else: 
                sintomo_fact['nome'] = clips.Symbol("Starnutazione a salve")
            sintomo_fact.assertit()
            
        if symptomatology['olfactoryProblems'] != None:
            sintomo_fact = sintomo_template.new_fact()
            if symptomatology['olfactoryProblems'] == 1:
                sintomo_fact['nome'] = clips.Symbol("Problemi olfattivi dovuti a iposmia")
            elif symptomatology['olfactoryProblems'] == 2:
                sintomo_fact['nome'] = clips.Symbol("Problemi olfattivi dovuti a anosmia")
            else:
                sintomo_fact['nome'] = clips.Symbol("Problemi olfattivi dovuti a cacosimia")
            sintomo_fact.assertit()
            
        if symptomatology['auricularPadding'] != None:
            sintomo_fact = sintomo_template.new_fact()
            if symptomatology['auricularPadding'] == 1:
                sintomo_fact['nome'] = clips.Symbol("Ovattamento a sinistra")
            elif symptomatology['auricularPadding'] == 2:
                sintomo_fact['nome'] = clips.Symbol("Ovattamento a destra")
            else:
                sintomo_fact['nome'] = clips.Symbol("Ovattamento bilaterale")
            sintomo_fact.assertit()
            
        if symptomatology['hypoacusis'] != None:
            sintomo_fact = sintomo_template.new_fact()
            if symptomatology['hypoacusis'] == 1:
                sintomo_fact['nome'] = clips.Symbol("Ipoacusi sinistra")
            elif symptomatology['hypoacusis'] == 2:
                sintomo_fact['nome'] = clips.Symbol("Ipoacusi destra")
            else:
                sintomo_fact['nome'] = clips.Symbol("Ipoacusi bilaterale")
            sintomo_fact.assertit()
            
        if symptomatology['tinnitus'] != None:
            sintomo_fact = sintomo_template.new_fact()
            if symptomatology['tinnitus'] == 1:
                sintomo_fact['nome'] = clips.Symbol("Acufeni sinistra")
            elif symptomatology['tinnitus'] == 2:
                sintomo_fact['nome'] = clips.Symbol("Acufeni destra")
            else: 
                sintomo_fact['nome'] = clips.Symbol("Acufeni bilaterale")
            sintomo_fact.assertit()
            
        if symptomatology['vertiginousSyndrome'] != None:
            sintomo_fact = sintomo_template.new_fact()
            if symptomatology['vertiginousSyndrome'] == 1:
                sintomo_fact['nome'] = clips.Symbol("Sindrome vertiginosa soggettiva")
            else: 
                sintomo_fact['nome'] = clips.Symbol("Sindrome vertiginosa oggettiva")
            sintomo_fact.assertit()
            
        if symptomatology['fever'] == 1:
            sintomo_fact = sintomo_template.new_fact()
            sintomo_fact['nome'] = clips.Symbol("Febbre")
            sintomo_fact.assertit()
            
        if symptomatology['medicineUse'] == 1:
            sintomo_fact = sintomo_template.new_fact()
            sintomo_fact['nome'] = clips.Symbol("Uso eccessivo di farmaci")
            sintomo_fact.assertit()
            
        if symptomatology['tearing'] == 1:
            sintomo_fact = sintomo_template.new_fact()
            sintomo_fact['nome'] = clips.Symbol("Lacrimazione")
            sintomo_fact.assertit()
            
        if symptomatology['photophobia'] == 1:
            sintomo_fact = sintomo_template.new_fact()
            sintomo_fact['nome'] = clips.Symbol("Fotofobia")
            sintomo_fact.assertit()
            
        if symptomatology['conjunctiveItching'] == 1:
            sintomo_fact = sintomo_template.new_fact()
            sintomo_fact['nome'] = clips.Symbol("Prurito occhio")
            sintomo_fact.assertit()
            
        if symptomatology['conjunctivaBurning'] == 1:
            sintomo_fact = sintomo_template.new_fact()
            sintomo_fact['nome'] = clips.Symbol("Bruciore")
            sintomo_fact.assertit()
            
    if medical_exam != None:
        if medical_exam['nasalPyramid'] != None and medical_exam['nasalPyramid'] != 1:
            scoperta_fact = scoperta_template.new_fact()
            scoperta_fact['parte-anatomica'] = clips.Symbol("piramide-nasale")
            if medical_exam['nasalPyramid'] == 2:
                scoperta_fact['caratteristica'] = clips.Symbol("Gibbo")
            elif  medical_exam['nasalPyramid'] == 3:
                scoperta_fact['caratteristica'] = clips.Symbol("Scoiosi")
            else:
                scoperta_fact['caratteristica'] = clips.Symbol("Defkrmazioni varie")
            scoperta_fact.assertit()
            
        if medical_exam['nasalValve'] != None and medical_exam['nasalValve'] != 1:
            scoperta_fact = scoperta_template.new_fact()
            scoperta_fact['parte-anatomica'] = clips.Symbol("valvola-nasale")
            if medical_exam['nasalValve'] == 2:
                scoperta_fact['caratteristica'] = clips.Symbol("insufficienza sinistra")
            elif medical_exam['nasalValve'] == 3:
                scoperta_fact['caratteristica'] = clips.Symbol("insufficienza destra")
            else: 
                scoperta_fact['caratteristica'] = clips.Symbol("insufficienza bilaterale")            
            scoperta_fact.assertit()
            
        if medical_exam['nasalSeptum'] != None and medical_exam['nasalSeptum'] != 1:
            scoperta_fact = scoperta_template.new_fact()
            scoperta_fact['parte-anatomica'] = clips.Symbol("setto-nasale")
            if medical_exam['nasalSeptum'] == 2:
                scoperta_fact['caratteristica'] = clips.Symbol("deviato a sinistra")
            elif medical_exam['nasalSeptum'] == 3:
                scoperta_fact['caratteristica'] = clips.Symbol("deviato a destra")
            else:
                scoperta_fact['caratteristica'] = clips.Symbol("esse italica")
            scoperta_fact.assertit()
            
        if medical_exam['turbinates'] != None and medical_exam['turbinates'] != 1:
            scoperta_fact = scoperta_template.new_fact()
            scoperta_fact['parte-anatomica'] = clips.Symbol("turbinati")
            if medical_exam['turbinates'] == 2:
                scoperta_fact['caratteristica'] = clips.Symbol("ipertrofici")
            elif medical_exam['turbinates'] == 3:
                scoperta_fact['caratteristica'] = clips.Symbol("iperemici")
            else:
                scoperta_fact['caratteristica'] = clips.Symbol("ematosi")
            scoperta_fact.assertit()
            
        if medical_exam['nasalPolyposisSx'] != None:
            scoperta_fact = scoperta_template.new_fact()
            scoperta_fact['parte-anatomica'] = clips.Symbol("poliposi-sinistra")
            scoperta_fact['caratteristica'] = clips.Symbol(str(medical_exam['nasalPolyposisSx']))
            scoperta_fact.assertit()
            
        if medical_exam['nasalPolyposisDx'] != None:
            scoperta_fact = scoperta_template.new_fact()
            scoperta_fact['parte-anatomica'] = clips.Symbol("poliposi-destra")
            scoperta_fact['caratteristica'] = clips.Symbol(str(medical_exam['nasalPolyposisDx']))
            scoperta_fact.assertit()
            
        if medical_exam['exudate'] != None:
            scoperta_fact = scoperta_template.new_fact()
            scoperta_fact['parte-anatomica'] = clips.Symbol("essudato")
            scoperta_fact['caratteristica'] = clips.Symbol(str(medical_exam['exudate']))
            scoperta_fact.assertit()
            
    if prick_test != None:    
        #allergy assert
        if prick_test['positive'] != None:   
            prick_test_fact = prick_test_template.new_fact()
            if prick_test['positive'] == 1:
                prick_test_fact["esito"] = clips.Symbol("positivo")
            else:
                prick_test_fact["esito"] = clips.Symbol("negativo")
    #        prick_test_fact["periodo"] = clips.Symbol(prick_test.period)
            if prick_test['perennialAllergen'] == 1:
                prick_test_fact["allergene"] = clips.Symbol("perenne")
            else:
                prick_test_fact["allergene"] = clips.Symbol("stagionale")
            prick_test_fact.assertit()
            
    #run the diagnosis calculation
    env.run()
    #evalutate all diagnosis
    extracted_diagnosis = env.eval("(find-all-facts((?f diagnosi)) TRUE)")
    '''
    for fact in env.facts():
        print(fact)
    '''
    json_res = {}
    #loop through diagnosis extracted from run
    for diagnosis_extr in extracted_diagnosis:
        json_res[diagnosis_extr["nome"]] = diagnosis_extr['informazioni']
    print(json_res)
    #clear the environment
    env.clear()
    
# Main execution
if __name__ == "__main__":   
    cells_for_type = json.loads(sys.argv[2])
    anamnesis = json.loads(sys.argv[3])
    symptomatology = json.loads(sys.argv[4])
    medical_exam = json.loads(sys.argv[5])
    prick_test = json.loads(sys.argv[6])
    
    import clips
    #initialize clips Environment
    env = clips.Environment()
    #load clips files
    env.load(sys.argv[1]+'/fatti.clp')
    env.load(sys.argv[1]+'/functions.clp')
    env.load(sys.argv[1]+'/diagnosi.clp')

    assert_facts()        

