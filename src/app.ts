import * as express from 'express';
import * as cookieParser from 'cookie-parser';
import {createConnection, getConnection, AdvancedConsoleLogger} from 'typeorm';
import authMiddleware from './middleware/auth.middleware';
import { AuthRoutes } from './routes/auth.routes';
import { PatientRoutes } from './routes/patient.routes';
import { DoctorRoutes } from './routes/doctor.routes';
import { ExamRoutes } from './routes/exam.routes';
import { DiagnosisRoutes } from './routes/diagnosis.routes';
import { AnamnesisRoutes } from './routes/anamnesis.routes';
import { SymptomatologyRoutes } from './routes/symptomatology.routes';
import { MedicalExamRoutes } from './routes/medical_exam.routes';
import { PrickTestRoutes } from './routes/pricktest.routes';
import { FieldExamRoutes } from './routes/fieldexam.routes';
import { CellsRoutes } from './routes/cells.routes';
import { CellTypeRoutes } from './routes/celltype.routes';

import { SwaggerRoutes } from './routes/swagger.routes';
import path = require('path');


const bodyParser = require('body-parser');
const helmet = require('helmet');
const swaggerUi = require('swagger-ui-express');
const multer = require('multer');
const storage = multer.diskStorage({

    destination: function (req, file, cb) {

      cb(null, 'src/services/cell_extraction/tmp')
    },

    filename: function (req, file, cb) {

      let filename = 'field_' +  path.basename(file.originalname);
       req.body.file = filename
      cb(null, filename)
    }
  })

const upload = multer({storage: storage});

// create and setup express app
class App {
    public app: express.Application;
    public port: number;
    public router: express.Router;

    constructor(port) {
        this.app = express();
        this.port = port;
        this.router = express.Router();
        this.setTmpDirectory();
        this.initializeMiddlewares();
        this.setHeader();
        this.initializeRoutes();
        this.initializeDBConnection();

      }

    private initializeMiddlewares(){
        // helmet sets HTTP response header
        this.app.use(helmet());
        // middleware that parses only json requests
        this.app.use(bodyParser.json());
        // middleware that parses only urlenconded
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(express.json());
        // middleware that transforms cookie string in a object
        this.app.use(cookieParser());
        // middleware that parses form-data requests
        this.app.use(upload.array('photo'));  // "photo" is the value of the attribute "name" to set in the upload forms (newfieldexam and newcell)
        this.app.use(express.static('public'));

    }

    // set http response header
    private async setHeader() {
        this.app.use((req, res, next) => {
            res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
            res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept');
            next();
        });
    }

    private initializeRoutes(){
        // routes working with doctor authentication
        this.router.use('/generate2FAcode', authMiddleware(true));
        this.router.use('/verify2FAcode', authMiddleware(true));
        // routes working with patients
        this.router.use('/patients', authMiddleware());
        this.router.use('/patientdetails', authMiddleware());
        this.router.use('/searchpatient', authMiddleware());
        this.router.use('/newpatient', authMiddleware());
        this.router.use('/deletepatient', authMiddleware());
        // routes working with doctors
        this.router.use('/updatedoctor', authMiddleware());
        this.router.use('/logout', authMiddleware());
        // routes working with exams
        this.router.use('/exams', authMiddleware());
        this.router.use('/searchexams',authMiddleware());
        this.router.use('/newexam',authMiddleware());
        this.router.use('/deleteexam',authMiddleware());
        // routes working with diagnosis
        this.router.use('/diagnosis',authMiddleware());
        this.router.use('/newdiagnosis',authMiddleware());
        this.router.use('/deletediagnosis',authMiddleware());
        this.router.use('/downloadreport',authMiddleware());
        this.router.use('/generatediagnosis',authMiddleware());
        // routes working with family anamnesis
        this.router.use('/familyanamnesis',authMiddleware());
        this.router.use('/newfamilyanamnesis',authMiddleware());
        // routes working with symptomatology
        this.router.use('/symptomatology',authMiddleware());
        this.router.use('/newsymptomatology',authMiddleware());
        // routes working with medical exam
        this.router.use('/medicalexam',authMiddleware());
        this.router.use('/newmedicalexam',authMiddleware());
        // routes working with prick test
        this.router.use('/pricktest',authMiddleware());
        this.router.use('/newpricktest',authMiddleware());
        // routes working with field exam
        this.router.use('/fieldexam',authMiddleware());
        this.router.use('/newfieldexam', authMiddleware());
        this.router.use('/deletefieldexam',authMiddleware());
        this.router.use('/extractcells',authMiddleware());
        this.router.use('/classifycells',authMiddleware());
        // routes working with cells
        this.router.use('/cells',authMiddleware());
        this.router.use('/cellstable',authMiddleware());
        this.router.use('/deletecell',authMiddleware());
        this.router.use('/changecelltype',authMiddleware());
        // routes working with cell type
        this.router.use('/celltype',authMiddleware());
        this.router.use('/newcelltype',authMiddleware());
        this.router.use('/deletecelltype',authMiddleware());

        // route for API documentation
        this.router.use('/api-docs',swaggerUi.serve)
    }

    // create typeorm connection
    private initializeDBConnection(){
    createConnection().then(connection => {
        console.log('Connection with the DB Rhinocyt created');

        // register all controllers' routes
        AuthRoutes.forEach(route => this.router[route.method](route.path, route.action));
        PatientRoutes.forEach(route => this.router[route.method](route.path, route.action));
        DoctorRoutes.forEach(route => this.router[route.method](route.path, route.action));
        ExamRoutes.forEach(route => this.router[route.method](route.path, route.action));
        DiagnosisRoutes.forEach(route => this.router[route.method](route.path, route.action));
        AnamnesisRoutes.forEach(route => this.router[route.method](route.path, route.action));
        SymptomatologyRoutes.forEach(route => this.router[route.method](route.path, route.action));
        MedicalExamRoutes.forEach(route => this.router[route.method](route.path, route.action));
        PrickTestRoutes.forEach(route => this.router[route.method](route.path, route.action));
        FieldExamRoutes.forEach(route => this.router[route.method](route.path, route.action));
        CellsRoutes.forEach(route => this.router[route.method](route.path, route.action));
        CellTypeRoutes.forEach(route => this.router[route.method](route.path, route.action));

        SwaggerRoutes.forEach(route => this.router[route.method](route.path, route.action));

        this.app.use(this.router);


    }).catch(error => console.log('TypeORM connection error: ', error));
    };

    private setTmpDirectory(){
        const fs = require('fs');
        const tmp = './src/services/cell_extraction/tmp';
        fs.mkdir(tmp,{recursive: true},function(err){if(err)console.log(err)});
    }

    public listen(){
        this.app.listen(this.port, (err) => {
            if (err) {
                throw err;
            }
            console.log('Server working on port : ' +this.port);
        });
    }
}
export default App;
