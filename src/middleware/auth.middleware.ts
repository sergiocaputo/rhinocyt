import { NextFunction, Response, RequestHandler } from 'express';
import * as jwt from 'jsonwebtoken';
import { DataStoredInToken } from '../interfaces/DataStoredInToken.interface';
import RequestWithUser from '../interfaces/RequestWithUser.interface';
import { getConnection} from 'typeorm';
import { Doctor } from '../entities/Doctor';

 /*
async function authMiddleware(request: RequestWithUser, response: Response, next: NextFunction) {
  const cookies = request.cookies;
  if (cookies && cookies.Authorization) {
    const secret = process.env.JWT_SECRET;
    try {
      const verificationResponse = jwt.verify(cookies.Authorization, secret) as DataStoredInToken;
      const id = verificationResponse._id;
      const doctorRepository =  getConnection().getRepository(Doctor);
      const user = await doctorRepository.find( {select: ["doctorId", "name", "surname", "email"],
            where: {doctorId : id}}).catch((err)=> {console.log(err);});

      if (user) {
           request.user = user[0];
           next();
      } else {
        console.log("Wrong authentication token");
        response.status(403).json({ 'message': 'doctorid authentication error ' });
      }
    } catch (error) {
        console.log("Time out token");
        response.status(403).json({ 'message': 'Time out token' });
    }
  } else {
    console.log("Login error");
    response.status(403).json({ 'message': 'Login needed' });
  }
}
 */

// omitSecondFactor set true when 2FA is required
function authMiddleware(omitSecondFactor = false): RequestHandler {
  return async (request: RequestWithUser, response: Response, next: NextFunction) => {
    const cookies = request.cookies;
    if (cookies && cookies.Authorization) {
      const secret = process.env.JWT_SECRET;
      try {
        const verificationResponse = jwt.verify(cookies.Authorization, secret) as DataStoredInToken;
        const { _id: id, isSecondFactorAuthenticated } = verificationResponse;

        const doctorRepository =  getConnection().getRepository(Doctor);
        const user = await doctorRepository.find( {select: ['doctorId', 'name', 'surname', 'email','twoFactorAuthenticationCode'],
            where: {doctorId : id}}).catch((err)=> {console.log(err);});
        if (user) {
          if (!omitSecondFactor && !isSecondFactorAuthenticated) {
            console.log('Second factor not authenticated');
            response.status(403).json({ 'message ': 'second factor of 2FA not authenticated' });
          } else {
            request.user = user[0];
            next();
          }
        } else {
          console.log('Wrong authentication token');
          response.status(403).json({ 'message ': 'doctorid authentication error' });
        }
      } catch (error) {
        console.log('Time out token');
        response.status(403).json({ 'message ': 'Time out token' });
      }
    } else {
      console.log('Login error');
      response.status(403).json({ 'message ': 'Login needed' });
    }
  };
}
export default authMiddleware;