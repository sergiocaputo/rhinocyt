import { getFieldExam, newFieldExam, deleteFieldExam, extractCells, classificateCells } from '../controllers/fieldexam';

const ROOT = ''
const POST = 'post'
const GET = 'get'
const DELETE = 'delete'

// --- field exam routes ---

export const FieldExamRoutes = [

    {   // get field exam using examId
        // /fieldexam?examid=inputGivenExamId
        path: ROOT + '/fieldexam',
        method: GET,
        action: getFieldExam
    },
    {   // save new field(s) exam
        path: ROOT + '/newfieldexam',
        method: POST,
        action: newFieldExam
    },
    {   // delete a field exam using fieldId
        // /deletefieldexam?fieldid=inputGivenExamId
        path: ROOT + '/deletefieldexam',
        method: DELETE,
        action: deleteFieldExam
    },
    {   // get field exam cells using examId
        // /extractcells?examid=inputGivenExamId
        path: ROOT + '/extractcells',
        method: GET,
        action: extractCells
    },
    {   // get cells type using examId
        // /classifycells?examid=inputGivenExamId
        path: ROOT + '/classifycells',
        method: GET,
        action: classificateCells
    },
]