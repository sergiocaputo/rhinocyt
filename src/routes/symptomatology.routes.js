"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SymptomatologyRoutes = void 0;
var symptomatology_1 = require("../controllers/symptomatology");
var ROOT = '';
var POST = 'post';
var GET = 'get';
// --- symptomatology routes ---
exports.SymptomatologyRoutes = [
    {
        // /symptomatology?fiscalcode=inputGivenFiscalcode
        path: ROOT + '/symptomatology',
        method: GET,
        action: symptomatology_1.getSymptomatology
    },
    {
        path: ROOT + '/newsymptomatology',
        method: POST,
        action: symptomatology_1.newSymptomatology
    },
];
