import { getDiagnosis, newDiagnosis, deleteDiagnosis, downloadReport, generateDiagnosis } from '../controllers/diagnosis';

const ROOT = ''
const PUT = 'put'
const GET = 'get'

 // --- diagnosis routes ---

export const DiagnosisRoutes = [

    {   // get diagnosis using fiscalcode and exam date
        // /diagnosis?fiscalcode=inputGivenFiscalcode&examdate=inputGivenExamDate
        path: ROOT + '/diagnosis',
        method: GET,
        action: getDiagnosis
    },
    {   // save diagnosis for an existing exam
        path: ROOT + '/newdiagnosis',
        method: PUT,
        action: newDiagnosis
    },
    {   // delete diagnosis for an existing exam
        // /deletediagnosis?examid=inputGivenExamId
        path: ROOT + '/deletediagnosis',
        method: PUT,
        action: deleteDiagnosis
    },
    {   // download diagnosis using fiscalcode and exam date
        // /downloadreport?fiscalcode=inputGivenFiscalcode&examdate=inputGivenExamDate
        path: ROOT + '/downloadreport',
        method: GET,
        action: downloadReport
    },
    {   // generate diagnosis for an existing exam
        // /generatediagnosis?examid=inputGivenExamId
        path: ROOT + '/generatediagnosis',
        method: GET,
        action: generateDiagnosis
    },
]