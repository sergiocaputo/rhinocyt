"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PatientRoutes = void 0;
var patient_1 = require("../controllers/patient");
var ROOT = '';
var POST = 'post';
var GET = 'get';
var DELETE = 'delete';
// --- patient routes ---
exports.PatientRoutes = [
    {
        path: ROOT + '/patients',
        method: GET,
        action: patient_1.getPatientByDoctorId
    },
    {
        // /patientdetails?fiscalcode=inputGivenFiscalCode
        path: ROOT + '/patientdetails',
        method: GET,
        action: patient_1.getPatientDetails
    },
    {
        // /searchpatient?name=inputGivenName&surname=inputGivenSurname
        path: ROOT + '/searchpatient',
        method: GET,
        action: patient_1.searchPatient
    },
    {
        path: ROOT + '/newpatient',
        method: POST,
        action: patient_1.newPatient,
    },
    {
        // /deletepatient?fiscalcode=inputGivenFiscalCode
        path: ROOT + '/deletepatient',
        method: DELETE,
        action: patient_1.deletePatient
    },
];
