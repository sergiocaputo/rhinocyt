"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MedicalExamRoutes = void 0;
var medicalexam_1 = require("../controllers/medicalexam");
var ROOT = '';
var POST = 'post';
var GET = 'get';
// --- medical exam routes ---
exports.MedicalExamRoutes = [
    {
        // /medicalexam?fiscalcode=inputGivenFiscalcode
        path: ROOT + '/medicalexam',
        method: GET,
        action: medicalexam_1.getMedicalExam
    },
    {
        path: ROOT + '/newmedicalexam',
        method: POST,
        action: medicalexam_1.newMedicalExam
    },
];
