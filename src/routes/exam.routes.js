"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExamRoutes = void 0;
var exam_1 = require("../controllers/exam");
var ROOT = '';
var POST = 'post';
var GET = 'get';
var DELETE = 'delete';
// --- exam routes ---
exports.ExamRoutes = [
    {
        // /exams?fiscalcode=inputGivenFiscalcode
        path: ROOT + '/exams',
        method: GET,
        action: exam_1.getPatientExams
    },
    {
        // /searchexams?name=inputGivenName&surname=inputGivenSurname
        path: ROOT + '/searchexams',
        method: GET,
        action: exam_1.searchExams
    },
    {
        path: ROOT + '/newexam',
        method: POST,
        action: exam_1.newExam
    },
    {
        // /deleteexam?examid=inputGivenExamId
        path: ROOT + '/deleteexam',
        method: DELETE,
        action: exam_1.deleteExam
    },
];
