"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DoctorRoutes = void 0;
var doctor_1 = require("../controllers/doctor");
var ROOT = '';
var POST = 'post';
var GET = 'get';
var PUT = 'put';
// --- doctor routes ---
exports.DoctorRoutes = [
    {
        path: ROOT + '/doctor/:iddoctor',
        method: GET,
        action: doctor_1.getDoctorById
    },
    {
        path: ROOT + '/updatedoctor',
        method: PUT,
        action: doctor_1.updateDoctor
    },
    {
        path: ROOT + '/login',
        method: POST,
        action: doctor_1.login
    },
    {
        path: ROOT + '/logout',
        method: GET,
        action: doctor_1.logout
    },
    {
        path: ROOT + '/signup',
        method: POST,
        action: doctor_1.newDoctor
    },
];
