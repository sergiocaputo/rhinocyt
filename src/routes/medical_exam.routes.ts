import { getMedicalExam, newMedicalExam } from '../controllers/medicalexam';

const ROOT = ''
const POST = 'post'
const GET = 'get'

// --- medical exam routes ---

export const MedicalExamRoutes = [

    {   // get medical exam using fiscalcode
        // /medicalexam?fiscalcode=inputGivenFiscalcode
        path: ROOT + '/medicalexam',
        method: GET,
        action: getMedicalExam
    },
    {   // save/modify medical exam
        path: ROOT + '/newmedicalexam',
        method: POST,
        action: newMedicalExam
    },

]