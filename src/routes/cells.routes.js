"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CellsRoutes = void 0;
var cells_1 = require("../controllers/cells");
var ROOT = '';
var PUT = 'put';
var GET = 'get';
var DELETE = 'delete';
// --- cells routes ---
exports.CellsRoutes = [
    {
        // /cells?fieldid=inputGivenFieldId
        path: ROOT + '/cells',
        method: GET,
        action: cells_1.getCells
    },
    {
        // /cellstable?fieldid=inputGivenFieldId
        path: ROOT + '/cellstable',
        method: GET,
        action: cells_1.getTable
    },
    {
        // /changecelltype?cellid=inputGivenCellId
        path: ROOT + '/changecelltype',
        method: PUT,
        action: cells_1.changeCellClass
    },
    {
        // /deletecell?cellid=inputGivenCellId
        path: ROOT + '/deletecell',
        method: DELETE,
        action: cells_1.deleteCell
    },
];
