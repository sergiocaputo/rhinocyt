import { swaggerDocs } from '../../doc/swagger.config';
const swaggerUi = require('swagger-ui-express');

const ROOT = ''
const GET = 'get'

// --- swagger routes ---

export const SwaggerRoutes =
[
    {
        path: ROOT + '/api-docs',
        method: GET,
        action: swaggerUi.setup(swaggerDocs)
    }
]