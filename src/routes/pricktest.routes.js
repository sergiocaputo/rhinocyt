"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PrickTestRoutes = void 0;
var pricktest_1 = require("../controllers/pricktest");
var ROOT = '';
var POST = 'post';
var GET = 'get';
// --- prick test routes ---
exports.PrickTestRoutes = [
    {
        // /pricktest?fiscalcode=inputGivenFiscalcode
        path: ROOT + '/pricktest',
        method: GET,
        action: pricktest_1.getPrickTest
    },
    {
        path: ROOT + '/newpricktest',
        method: POST,
        action: pricktest_1.newPrickTest
    },
];
