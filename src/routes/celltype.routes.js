"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CellTypeRoutes = void 0;
var celltype_1 = require("../controllers/celltype");
var ROOT = '';
var POST = 'post';
var GET = 'get';
var DELETE = 'delete';
// --- cell type routes ---
exports.CellTypeRoutes = [
    {
        // /celltype?celltypeid=inputGivenCellTypeId
        path: ROOT + '/celltype',
        method: GET,
        action: celltype_1.getCellTypes
    },
    {
        path: ROOT + '/newcelltype',
        method: POST,
        action: celltype_1.newCellType
    },
    {
        // /deletecelltype?celltypeid=inputGivenCellTypeId
        path: ROOT + '/deletecelltype',
        method: DELETE,
        action: celltype_1.deleteCellType
    },
];
