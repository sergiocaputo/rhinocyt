import { getPrickTest, newPrickTest } from '../controllers/pricktest';

const ROOT = ''
const POST = 'post'
const GET = 'get'

// --- prick test routes ---

export const PrickTestRoutes = [

    {   // get prick test using fiscalcode
        // /pricktest?fiscalcode=inputGivenFiscalcode
        path: ROOT + '/pricktest',
        method: GET,
        action: getPrickTest
    },
    {   // save/modify prick test
        path: ROOT + '/newpricktest',
        method: POST,
        action: newPrickTest
    },
]