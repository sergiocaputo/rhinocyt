import { getPatientExams, searchExams, newExam, deleteExam} from '../controllers/exam';

const ROOT = ''
const POST = 'post'
const GET = 'get'
const DELETE = 'delete'

// --- exam routes ---

export const ExamRoutes = [

    {   // get patient exams info using fiscalcode if provided, otherwise get all the exams of the logged doctor's patients
        // /exams?fiscalcode=inputGivenFiscalcode
        path: ROOT + '/exams',
        method: GET,
        action: getPatientExams
    },
    {   // search exam using patient name and surname
        // /searchexams?name=inputGivenName&surname=inputGivenSurname
        path: ROOT + '/searchexams',
        method: GET,
        action: searchExams
    },
    {   // save a new exam
        path: ROOT + '/newexam',
        method: POST,
        action: newExam
    },
    {   // delete an exam
        // /deleteexam?examid=inputGivenExamId
        path: ROOT + '/deleteexam',
        method: DELETE,
        action: deleteExam
    },
]