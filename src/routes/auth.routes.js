"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthRoutes = void 0;
var auth_1 = require("../controllers/auth");
var ROOT = '';
var POST = 'post';
var GET = 'get';
// --- authentication routes ---
exports.AuthRoutes = [
    {
        path: ROOT + '/generate2FAcode',
        method: GET,
        action: auth_1.generateTwoFactorAuthenticationCode
    },
    {
        path: ROOT + '/verify2FAcode',
        method: POST,
        action: auth_1.secondFactorAuthentication
    },
];
