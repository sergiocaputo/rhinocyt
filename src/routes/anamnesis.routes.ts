import { getFamilyAnamnesis, newFamilyAnamnesis } from '../controllers/anamnesis';

const ROOT = ''
const POST = 'post'
const GET = 'get'

// --- family_anamnesis routes ---

export const AnamnesisRoutes = [

    {
        path: ROOT + '/familyanamnesis',
        method: GET,
        action: getFamilyAnamnesis
    },
    {   // save/modify family diagnosis
        path: ROOT + '/newfamilyanamnesis',
        method: POST,
        action: newFamilyAnamnesis
    },

]