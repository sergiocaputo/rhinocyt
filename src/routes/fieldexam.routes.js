"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FieldExamRoutes = void 0;
var fieldexam_1 = require("../controllers/fieldexam");
var ROOT = '';
var POST = 'post';
var GET = 'get';
var DELETE = 'delete';
// --- field exam routes ---
exports.FieldExamRoutes = [
    {
        // /fieldexam?examid=inputGivenExamId
        path: ROOT + '/fieldexam',
        method: GET,
        action: fieldexam_1.getFieldExam
    },
    {
        path: ROOT + '/newfieldexam',
        method: POST,
        action: fieldexam_1.newFieldExam
    },
    {
        // /deletefieldexam?fieldid=inputGivenExamId
        path: ROOT + '/deletefieldexam',
        method: DELETE,
        action: fieldexam_1.deleteFieldExam
    },
    {
        // /extractcells?examid=inputGivenExamId
        path: ROOT + '/extractcells',
        method: GET,
        action: fieldexam_1.extractCells
    },
    {
        // /classifycells?examid=inputGivenExamId
        path: ROOT + '/classifycells',
        method: GET,
        action: fieldexam_1.classificateCells
    },
];
