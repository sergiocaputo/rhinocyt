"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SwaggerRoutes = void 0;
var swagger_config_1 = require("../../doc/swagger.config");
var swaggerUi = require('swagger-ui-express');
var ROOT = '';
var GET = 'get';
// --- swagger routes ---
exports.SwaggerRoutes = [
    {
        path: ROOT + '/api-docs',
        method: GET,
        action: swaggerUi.setup(swagger_config_1.swaggerDocs)
    }
];
