import { getCellTypes, newCellType, deleteCellType } from '../controllers/celltype'

const ROOT = ''
const POST = 'post'
const GET = 'get'
const DELETE = 'delete'

// --- cell type routes ---

export const CellTypeRoutes = [

    {   // get cell type info using cellTypeId if provided, otherwise get all the cell types saved
        // /celltype?celltypeid=inputGivenCellTypeId
        path: ROOT + '/celltype',
        method: GET,
        action: getCellTypes
    },
    {   // save new celltype
        path: ROOT + '/newcelltype',
        method: POST,
        action: newCellType
    },
    {   // delete a celltype using cellTypeId
        // /deletecelltype?celltypeid=inputGivenCellTypeId
        path: ROOT + '/deletecelltype',
        method: DELETE,
        action: deleteCellType
    },

]