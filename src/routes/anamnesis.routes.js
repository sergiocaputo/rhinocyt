"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AnamnesisRoutes = void 0;
var anamnesis_1 = require("../controllers/anamnesis");
var ROOT = '';
var POST = 'post';
var GET = 'get';
// --- family_anamnesis routes ---
exports.AnamnesisRoutes = [
    {
        path: ROOT + '/familyanamnesis',
        method: GET,
        action: anamnesis_1.getFamilyAnamnesis
    },
    {
        path: ROOT + '/newfamilyanamnesis',
        method: POST,
        action: anamnesis_1.newFamilyAnamnesis
    },
];
