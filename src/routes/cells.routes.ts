import { getCells, deleteCell, getTable, changeCellClass } from '../controllers/cells'

const ROOT = ''
const PUT = 'put'
const GET = 'get'
const DELETE = 'delete'

// --- cells routes ---

export const CellsRoutes = [

    {   // get cells using fieldId
        // /cells?fieldid=inputGivenFieldId
        path: ROOT + '/cells',
        method: GET,
        action: getCells
    },
    {   // get count cells table
        // /cellstable?fieldid=inputGivenFieldId
        path: ROOT + '/cellstable',
        method: GET,
        action: getTable
    },
    {   // change cell type
        // /changecelltype?cellid=inputGivenCellId
        path: ROOT + '/changecelltype',
        method: PUT,
        action: changeCellClass
    },
    {   // delete a cell using cellId
        // /deletecell?cellid=inputGivenCellId
        path: ROOT + '/deletecell',
        method: DELETE,
        action: deleteCell
    },

]