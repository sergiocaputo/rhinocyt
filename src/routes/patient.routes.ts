import {getPatientByDoctorId,getPatientDetails, newPatient, deletePatient, searchPatient} from '../controllers/patient';

const ROOT = ''
const POST = 'post'
const GET = 'get'
const DELETE = 'delete'

// --- patient routes ---

export const PatientRoutes = [

    {   // gets patients of the logged doctor
        path: ROOT + '/patients',
        method: GET,
        action: getPatientByDoctorId
    },
    {   // gets patient details using fiscal code
        // /patientdetails?fiscalcode=inputGivenFiscalCode
        path: ROOT + '/patientdetails',
        method: GET,
        action: getPatientDetails
    },
    {   // search patient using name and surname
        // /searchpatient?name=inputGivenName&surname=inputGivenSurname
        path: ROOT + '/searchpatient',
        method: GET,
        action: searchPatient
    },
    {   // save a new patient
        path: ROOT + '/newpatient',
        method: POST,
        action: newPatient,
    },
    {   // delete a patient
        // /deletepatient?fiscalcode=inputGivenFiscalCode
        path: ROOT + '/deletepatient',
        method: DELETE,
        action: deletePatient
    },

]