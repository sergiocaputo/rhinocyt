import { getDoctorById, login, newDoctor, logout, updateDoctor } from '../controllers/doctor';

const ROOT = ''
const POST = 'post'
const GET = 'get'
const PUT = 'put'

// --- doctor routes ---

export const DoctorRoutes = [

    {   // get doctor info using iddoctor
        path: ROOT + '/doctor/:iddoctor',
        method: GET,
        action: getDoctorById
    },
    {   // update doctor password or email
        path: ROOT + '/updatedoctor',
        method: PUT,
        action: updateDoctor
    },
    {
        path: ROOT + '/login',
        method: POST,
        action: login
    },
    {
        path: ROOT + '/logout',
        method: GET,
        action: logout
    },
    {
        path: ROOT + '/signup',
        method: POST,
        action: newDoctor
    },

]