import { getSymptomatology, newSymptomatology } from '../controllers/symptomatology';

const ROOT = ''
const POST = 'post'
const GET = 'get'

// --- symptomatology routes ---

export const SymptomatologyRoutes = [

    {   // get symptomatology using fiscalcode
        // /symptomatology?fiscalcode=inputGivenFiscalcode
        path: ROOT + '/symptomatology',
        method: GET,
        action: getSymptomatology
    },
    {   // save/modify symptomatology
        path: ROOT + '/newsymptomatology',
        method: POST,
        action: newSymptomatology
    },
]