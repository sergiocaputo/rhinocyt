import { generateTwoFactorAuthenticationCode, secondFactorAuthentication } from '../controllers/auth';

const ROOT = ''
const POST = 'post'
const GET = 'get'

// --- authentication routes ---

export const AuthRoutes = [

    {
        path: ROOT + '/generate2FAcode',
        method: GET,
        action: generateTwoFactorAuthenticationCode
    },
    {
        path: ROOT + '/verify2FAcode',
        method: POST,
        action: secondFactorAuthentication
    },

]