"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DiagnosisRoutes = void 0;
var diagnosis_1 = require("../controllers/diagnosis");
var ROOT = '';
var PUT = 'put';
var GET = 'get';
// --- diagnosis routes ---
exports.DiagnosisRoutes = [
    {
        // /diagnosis?fiscalcode=inputGivenFiscalcode&examdate=inputGivenExamDate
        path: ROOT + '/diagnosis',
        method: GET,
        action: diagnosis_1.getDiagnosis
    },
    {
        path: ROOT + '/newdiagnosis',
        method: PUT,
        action: diagnosis_1.newDiagnosis
    },
    {
        // /deletediagnosis?examid=inputGivenExamId
        path: ROOT + '/deletediagnosis',
        method: PUT,
        action: diagnosis_1.deleteDiagnosis
    },
    {
        // /downloadreport?fiscalcode=inputGivenFiscalcode&examdate=inputGivenExamDate
        path: ROOT + '/downloadreport',
        method: GET,
        action: diagnosis_1.downloadReport
    },
    {
        // /generatediagnosis?examid=inputGivenExamId
        path: ROOT + '/generatediagnosis',
        method: GET,
        action: diagnosis_1.generateDiagnosis
    },
];
