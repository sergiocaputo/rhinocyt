import App from './app';
require('dotenv').config()

const port = 3000;

// start express server
const server = new App(port);

server.listen();