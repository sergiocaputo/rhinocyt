"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var cookieParser = require("cookie-parser");
var typeorm_1 = require("typeorm");
var auth_middleware_1 = require("./middleware/auth.middleware");
var auth_routes_1 = require("./routes/auth.routes");
var patient_routes_1 = require("./routes/patient.routes");
var doctor_routes_1 = require("./routes/doctor.routes");
var exam_routes_1 = require("./routes/exam.routes");
var diagnosis_routes_1 = require("./routes/diagnosis.routes");
var anamnesis_routes_1 = require("./routes/anamnesis.routes");
var symptomatology_routes_1 = require("./routes/symptomatology.routes");
var medical_exam_routes_1 = require("./routes/medical_exam.routes");
var pricktest_routes_1 = require("./routes/pricktest.routes");
var fieldexam_routes_1 = require("./routes/fieldexam.routes");
var cells_routes_1 = require("./routes/cells.routes");
var celltype_routes_1 = require("./routes/celltype.routes");
var swagger_routes_1 = require("./routes/swagger.routes");
var path = require("path");
var bodyParser = require('body-parser');
var helmet = require('helmet');
var swaggerUi = require('swagger-ui-express');
var multer = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'src/services/cell_extraction/tmp');
    },
    filename: function (req, file, cb) {
        var filename = 'field_' + path.basename(file.originalname);
        req.body.file = filename;
        cb(null, filename);
    }
});
var upload = multer({ storage: storage });
// create and setup express app
var App = /** @class */ (function () {
    function App(port) {
        this.app = express();
        this.port = port;
        this.router = express.Router();
        this.setTmpDirectory();
        this.initializeMiddlewares();
        this.setHeader();
        this.initializeRoutes();
        this.initializeDBConnection();
    }
    App.prototype.initializeMiddlewares = function () {
        // helmet sets HTTP response header
        this.app.use(helmet());
        // middleware that parses only json requests
        this.app.use(bodyParser.json());
        // middleware that parses only urlenconded
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(express.json());
        // middleware that transforms cookie string in a object
        this.app.use(cookieParser());
        // middleware that parses form-data requests
        this.app.use(upload.array('photo')); // "photo" is the value of the attribute "name" to set in the upload forms (newfieldexam and newcell)
        this.app.use(express.static('public'));
    };
    // set http response header
    App.prototype.setHeader = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.app.use(function (req, res, next) {
                    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');
                    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
                    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept');
                    next();
                });
                return [2 /*return*/];
            });
        });
    };
    App.prototype.initializeRoutes = function () {
        // routes working with doctor authentication
        this.router.use('/generate2FAcode', auth_middleware_1.default(true));
        this.router.use('/verify2FAcode', auth_middleware_1.default(true));
        // routes working with patients
        this.router.use('/patients', auth_middleware_1.default());
        this.router.use('/patientdetails', auth_middleware_1.default());
        this.router.use('/searchpatient', auth_middleware_1.default());
        this.router.use('/newpatient', auth_middleware_1.default());
        this.router.use('/deletepatient', auth_middleware_1.default());
        // routes working with doctors
        this.router.use('/updatedoctor', auth_middleware_1.default());
        this.router.use('/logout', auth_middleware_1.default());
        // routes working with exams
        this.router.use('/exams', auth_middleware_1.default());
        this.router.use('/searchexams', auth_middleware_1.default());
        this.router.use('/newexam', auth_middleware_1.default());
        this.router.use('/deleteexam', auth_middleware_1.default());
        // routes working with diagnosis
        this.router.use('/diagnosis', auth_middleware_1.default());
        this.router.use('/newdiagnosis', auth_middleware_1.default());
        this.router.use('/deletediagnosis', auth_middleware_1.default());
        this.router.use('/downloadreport', auth_middleware_1.default());
        this.router.use('/generatediagnosis', auth_middleware_1.default());
        // routes working with family anamnesis
        this.router.use('/familyanamnesis', auth_middleware_1.default());
        this.router.use('/newfamilyanamnesis', auth_middleware_1.default());
        // routes working with symptomatology
        this.router.use('/symptomatology', auth_middleware_1.default());
        this.router.use('/newsymptomatology', auth_middleware_1.default());
        // routes working with medical exam
        this.router.use('/medicalexam', auth_middleware_1.default());
        this.router.use('/newmedicalexam', auth_middleware_1.default());
        // routes working with prick test
        this.router.use('/pricktest', auth_middleware_1.default());
        this.router.use('/newpricktest', auth_middleware_1.default());
        // routes working with field exam
        this.router.use('/fieldexam', auth_middleware_1.default());
        this.router.use('/newfieldexam', auth_middleware_1.default());
        this.router.use('/deletefieldexam', auth_middleware_1.default());
        this.router.use('/extractcells', auth_middleware_1.default());
        this.router.use('/classifycells', auth_middleware_1.default());
        // routes working with cells
        this.router.use('/cells', auth_middleware_1.default());
        this.router.use('/cellstable', auth_middleware_1.default());
        this.router.use('/deletecell', auth_middleware_1.default());
        this.router.use('/changecelltype', auth_middleware_1.default());
        // routes working with cell type
        this.router.use('/celltype', auth_middleware_1.default());
        this.router.use('/newcelltype', auth_middleware_1.default());
        this.router.use('/deletecelltype', auth_middleware_1.default());
        // route for API documentation
        this.router.use('/api-docs', swaggerUi.serve);
    };
    // create typeorm connection
    App.prototype.initializeDBConnection = function () {
        var _this = this;
        typeorm_1.createConnection().then(function (connection) {
            console.log('Connection with the DB Rhinocyt created');
            // register all controllers' routes
            auth_routes_1.AuthRoutes.forEach(function (route) { return _this.router[route.method](route.path, route.action); });
            patient_routes_1.PatientRoutes.forEach(function (route) { return _this.router[route.method](route.path, route.action); });
            doctor_routes_1.DoctorRoutes.forEach(function (route) { return _this.router[route.method](route.path, route.action); });
            exam_routes_1.ExamRoutes.forEach(function (route) { return _this.router[route.method](route.path, route.action); });
            diagnosis_routes_1.DiagnosisRoutes.forEach(function (route) { return _this.router[route.method](route.path, route.action); });
            anamnesis_routes_1.AnamnesisRoutes.forEach(function (route) { return _this.router[route.method](route.path, route.action); });
            symptomatology_routes_1.SymptomatologyRoutes.forEach(function (route) { return _this.router[route.method](route.path, route.action); });
            medical_exam_routes_1.MedicalExamRoutes.forEach(function (route) { return _this.router[route.method](route.path, route.action); });
            pricktest_routes_1.PrickTestRoutes.forEach(function (route) { return _this.router[route.method](route.path, route.action); });
            fieldexam_routes_1.FieldExamRoutes.forEach(function (route) { return _this.router[route.method](route.path, route.action); });
            cells_routes_1.CellsRoutes.forEach(function (route) { return _this.router[route.method](route.path, route.action); });
            celltype_routes_1.CellTypeRoutes.forEach(function (route) { return _this.router[route.method](route.path, route.action); });
            swagger_routes_1.SwaggerRoutes.forEach(function (route) { return _this.router[route.method](route.path, route.action); });
            _this.app.use(_this.router);
        }).catch(function (error) { return console.log('TypeORM connection error: ', error); });
    };
    ;
    App.prototype.setTmpDirectory = function () {
        var fs = require('fs');
        var tmp = './src/services/cell_extraction/tmp';
        fs.mkdir(tmp, { recursive: true }, function (err) { if (err)
            console.log(err); });
    };
    App.prototype.listen = function () {
        var _this = this;
        this.app.listen(this.port, function (err) {
            if (err) {
                throw err;
            }
            console.log('Server working on port : ' + _this.port);
        });
    };
    return App;
}());
exports.default = App;
