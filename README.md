# Rhinocyt #

System to support medic specialist in rhinitis and cytology using API endpoints.

### Features ###

* Cells recognition and extraction
* Cells classification through AI model
* Patient management
    * Patient's anagraphic data
	* Patient's history with exams and diagnosis
	* Reports in PDF about exams  
* Session handler
* Two factor authentication with QR code
* Diagnosis generation 


### Main dependencies ###

* [Node.js](https://nodejs.org/it/about/)
    * Framework used server-side to handle client requests using JavaScript
* [Express](https://expressjs.com)
    * Web framework for Node.js applications
* [TypeOrm](https://typeorm.io/#/)
    * ORM (Object-Relational Mapping) used to integrate database operations in the application API endpoints
* [JSON Web Token](https://www.npmjs.com/package/jsonwebtoken)
    * Internet standard used to manage authentication
* [Speakeasy](https://www.npmjs.com/package/speakeasy)
    * One-time passcode generator used in two-factor authentication that supports Google Authenticator


### Installation ###

1. Clone the repository
     
    >`git clone https://sergiocaputo@bitbucket.org/sergiocaputo/rhinocyt.git`  
    
2. Run SQL script in MySQL command line to create the DB 
   
    >`mysql> source C:\Users\Admin\Desktop\rhinocyt\src\database\db.sql`
    
3. Open `ormconfig.json` in Rhinocyt root directory and set your password used for MySQL
              
4. Downlaod and install [npm](https://www.npmjs.com/get-npm) 

5. Install all the dependencies in package.json
   
    >`npm install`
    
6. Downlaod and install [Python 3.8.x](https://www.python.org/downloads/)

7. Install all the dependencies needed for Python

    >`pip install scipy`
    
    >`pip install numpy`
    
    >`pip install matplotlib`
    
    >`pip install scikit_image`
    
    >`pip install opencv_python`
    
    >`pip install -upgrade tensorflow`
    
    >`pip install keras`
    
    >`pip install clipspy`


### Usage ###
     
1. Open the cmd, change directory by entering the path where you saved Rhinocyt clone and run: `npm run dev`
2. Open the browser and type `http://localhost:3000/api-docs/` in the address bar, all the API can be used here