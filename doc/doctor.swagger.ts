/**
 * @swagger
 * definitions:
 *   Doctor:
 *        type: object
 *        properties:
 *            doctorId:
 *               type: number
 *
 *            name:
 *               type: string
 *
 *            surname:
 *               type: string
 *
 *            email:
 *               type: string
 *
 *            password:
 *               type: string
 *
 *            twoFactorAuthenticationCode:
 *               type: string
 *
 * /doctor/{iddoctor}:
 *   get:
 *       tags: ["Doctor"]
 *       summary: Gets doctor associated to doctorId
 *       description: Returns doctor informations using doctorId
 *       parameters:
 *         - name: iddoctor
 *           in: path
 *           required: true
 *           description: DoctorId
 *           type: number
 *       responses:
 *           '200':
 *               description: Doctor info returned
 *               schema:
 *                   "$ref": "#/definitions/Doctor"
 *               examples:
 *                   application/json:
 *                    [{
 *                        name: best,
 *                        surname: doctor,
 *                        email: best@libero.it
 *                   }]
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : error
 *
 *
 *
 * /updatedoctor:
 *   put:
 *       tags: ["Doctor"]
 *       summary: Update doctor's password or email using doctorId
 *       parameters:
 *           - name: body
 *             in: body
 *             required: true
 *             description: "Password or email to update"
 *             schema:
 *               type: object
 *               properties:
 *
 *                   password:
 *                       type: string
 *
 *                   email:
 *                       type: string
 *
 *       produces:
 *           application/json
 *       responses:
 *           '200':
 *               description: Doctor's info updated
 *               examples:
 *                   application/json:
 *                       message : password updated / empty set / email updated
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : wrong parameters given / error
 * /login:
 *   post:
 *       tags: ["Doctor"]
 *       summary: Login to Rhinocyt
 *       parameters:
 *           - name: body
 *             in: body
 *             required: true
 *             description: "Email and password to login"
 *             schema:
 *               type: object
 *               properties:
 *                   email:
 *                       type: string
 *
 *                   password:
 *                       type: string
 *
 *       produces:
 *           application/json
 *       responses:
 *           '200':
 *               description: Doctor logged in
 *               examples:
 *                   application/json:
 *                       message : doctor logged in
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : user not registered / incorrect password / error
 *
 * /logout:
 *   get:
 *       tags: ["Doctor"]
 *       summary: Logout from Rhinocyt
 *       produces:
 *           application/json
 *       responses:
 *           '200':
 *               description: Doctor logged out
 *               examples:
 *                   application/json:
 *                       message : logged out
 *
 *
 * /signup:
 *   post:
 *       tags: ["Doctor"]
 *       summary: Create a new doctor account
 *       parameters:
 *           - name: body
 *             in: body
 *             required: true
 *             description: Doctor informations used to create a new account
 *             schema:
 *                 type: object
 *                 properties:
 *                     name:
 *                       type: string
 *
 *                     surname:
 *                       type: string
 *
 *                     email:
 *                       type: string
 *
 *                     password:
 *                       type: string
 *       produces:
 *           application/json
 *       responses:
 *           '200':
 *               description: New doctor account created
 *               examples:
 *                   application/json:
 *                       {
 *                          name: Mike,
 *                          surname: Neri,
 *                          email: mikeneri@gmail.com,
 *                          password: $2b$10$dk1OIhbUnvPVq1J0h..Bo.fTiUdLge4gx1slqbpRdUYfH91q.nhey,
 *                          twoFactorAuthenticationCode: null,
 *                          doctorId: 107
 *                        }
 *           '403':
 *               description: Error saving informations
 *               examples:
 *                   application/json:
 *                       message : doctor already registered / error
 *
 */

