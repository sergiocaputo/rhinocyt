/**
 * @swagger
 * definitions:
 *   FieldExam:
 *        type: object
 *        properties:
 *            fieldId:
 *               type: number
 *
 *            photo:
 *               type: array
 *               items:
 *                   type: string
 *                   format: binary
 *
 *            examId:
 *               type: number
 *
 *
 * /fieldexam:
 *   get:
 *       tags: ["Field Exam"]
 *       summary: Gets exam fields using examId
 *       description: Returns fields about exam using examId
 *       parameters:
 *         - name: examid
 *           in: query
 *           description: examId
 *           type: number
 *       responses:
 *           '200':
 *               description: Exam field returned
 *               schema:
 *                   "$ref": "#/definitions/FieldExam"
 *               examples:
 *                   application/json:
 *                      [
 *                      {
 *                          "fieldId": 105,
 *                          "photo": "./src/services/cell_extraction/fields/examid_7/fieldid_105_field_sm-000001.png"
 *                      },
 *                      {
 *                          "fieldId": 106,
 *                          "photo": "./src/services/cell_extraction/fields/examid_7/fieldid_106_field_sm-000006.png"
 *                      },
 *                      {
 *                          "fieldId": 107,
 *                          "photo": "./src/services/cell_extraction/fields/examid_7/fieldid_107_field_sm-000002.png"
 *                      },
 *                      {
 *                          "fieldId": 108,
 *                          "photo": "./src/services/cell_extraction/fields/examid_7/fieldid_108_field_sm-000005.png"
 *                      }
 *                      ]
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : is not allowed to view other patient's field exam / examid not provided / error
 *
 *
 *
 *
 * /newfieldexam:
 *   post:
 *       tags: ["Field Exam"]
 *       summary: Save one or more new fields exam
 *       consumes:
 *           - multipart/form-data
 *       parameters:
 *           - name: examId
 *             in: formData
 *             required: true
 *             type: number
 *           - name: photo
 *             in: formData
 *             required: true
 *             type: array
 *             items:
 *               type: file
 *             description: Fields to save
 *
 *       produces:
 *           application/json
 *       responses:
 *           '200':
 *               description: New exam field saved
 *               examples:
 *                   application/json:
 *                       message: new field/fields added
 *           '403':
 *               description: Error saving informations
 *               examples:
 *                   application/json:
 *                       message : exam not registered / is not allowed to save other patient's fields exam / wrong data sent / error
 *
 * /deletefieldexam:
 *   delete:
 *       tags: ["Field Exam"]
 *       summary: Deletes exam field associated to fieldId
 *       parameters:
 *         - name: fieldid
 *           in: query
 *           required: true
 *           description: fieldId
 *           type: number
 *       responses:
 *           '200':
 *               description: Exam field deleted
 *               examples:
 *                   application/json:
 *                       message : field deleted
 *           '403':
 *               description: Error getting informations from others doctors' patients
 *               examples:
 *                   application/json:
 *                       message : field exam provided doesn't exist / deleting other patient's fields exam is not allowed / error
 *
 *
 *
 *
 * /extractcells:
 *   get:
 *       tags: ["Field Exam"]
 *       summary: Extracts cells from exam associated to examId provided
 *       description: Extracts cells using examId
 *       parameters:
 *         - name: examid
 *           in: query
 *           description: examId
 *           type: number
 *       responses:
 *           '200':
 *               description: Exam cells extracted
 *               examples:
 *                  application/json:
 *                      message : cells extracted
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : exam provided doesn't exist / saving other patient's fields exam cells is not allowed / field not registered for this exam / error
 *
 * /classifycells:
 *   get:
 *       tags: ["Field Exam"]
 *       summary: Classifies and saves cells from exam associated to examId provided
 *       description: Classifies cells using examId
 *       parameters:
 *         - name: examid
 *           in: query
 *           description: examId
 *           type: number
 *       responses:
 *           '200':
 *               description: Cells classified and saved
 *               examples:
 *                  application/json:
 *                      message : cells saved
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : exam provided doesn't exist or doesn't have extracted cells / saving other patient's fields exam cells is not allowed / wrong data sent / exam provided doesn't have any cell extracted / error
 *
 *
 *
 *
 */
