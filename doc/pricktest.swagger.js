/**
 * @swagger
 * definitions:
 *   PrickTest:
 *        type: object
 *        properties:
 *            prickTestId:
 *               type: number
 *
 *            positive:
 *               type: boolean
 *
 *            perennialAllergen:
 *               type: boolean
 *
 *            poplar:
 *               type: boolean
 *
 *            hazel:
 *               type: boolean
 *
 *            commonAsh:
 *               type: boolean
 *
 *            willow:
 *               type: boolean
 *
 *            alder:
 *               type: boolean
 *
 *            cupressacee:
 *               type: boolean
 *
 *            oakTree:
 *               type: boolean
 *
 *            blackHornbeam:
 *               type: boolean
 *
 *            planeTree:
 *               type: boolean
 *
 *            grasses:
 *               type: boolean
 *
 *            floweringAsh:
 *               type: boolean
 *
 *            pinaceae:
 *               type: boolean
 *
 *            buckwheat:
 *               type: boolean
 *
 *            urticaceae:
 *               type: boolean
 *
 *            plantain:
 *               type: boolean
 *
 *            birch:
 *               type: boolean
 *
 *            chestnut:
 *               type: boolean
 *
 *            absinthe:
 *               type: boolean
 *
 * /pricktest:
 *   get:
 *       tags: ["Prick Test"]
 *       summary: Gets prick test informations using fiscalcode
 *       description: Returns prick test informations using fiscalcode
 *       parameters:
 *         - name: fiscalcode
 *           in: query
 *           description: fiscalcode
 *           type: string
 *       responses:
 *           '200':
 *               description: Prick test returned
 *               schema:
 *                   "$ref": "#/definitions/PrickTest"
 *               examples:
 *                   application/json:
 *                    [{
 *                      prickTestId: 3,
 *                      positive: 1,
 *                      perennialAllergen: 1,
 *                      poplar: null,
 *                      hazel: null,
 *                      commonAsh: null,
 *                      willow: null,
 *                      alder: null,
 *                      cupressacee: null,
 *                      oakTree: null,
 *                      blackHornbeam: null,
 *                      planeTree: null,
 *                      grasses: null,
 *                      floweringAsh: null,
 *                      pinaceae: null,
 *                      buckwheat: null,
 *                      urticaceae: null,
 *                      plantain: null,
 *                      birch: null,
 *                      chestnut: null,
 *                      absinthe: null,
 *                      patients:
 *                       [{
 *                           name: Marco,
 *                           surname: Bianchi
 *                       }]
 *                     }
 *                    ]
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : error
 *
 *
 *
 *
 * /newpricktest:
 *   post:
 *       tags: ["Prick Test"]
 *       summary: Save or modify prick test
 *       parameters:
 *           - name: body
 *             in: body
 *             required: true
 *             description: Informations to save
 *             schema:
 *                 type: object
 *                 properties:
 *                   fiscalCode:
 *                       type: string
 *
 *                   positive:
 *                       type: boolean
 *
 *                   perennialAllergen:
 *                       type: boolean
 *
 *                   poplar:
 *                       type: boolean
 *
 *                   hazel:
 *                        type: boolean
 *
 *                   commonAsh:
 *                        type: boolean
 *
 *                   willow:
 *                       type: boolean
 *
 *                   alder:
 *                       type: boolean
 *
 *                   cupressacee:
 *                       type: boolean
 *
 *                   oakTree:
 *                       type: boolean
 *
 *                   blackHornbeam:
 *                       type: boolean
 *
 *                   planeTree:
 *                       type: boolean
 *
 *                   grasses:
 *                       type: boolean
 *
 *                   floweringAsh:
 *                       type: boolean
 *
 *                   pinaceae:
 *                       type: boolean
 *
 *                   buckwheat:
 *                       type: boolean
 *
 *                   urticaceae:
 *                       type: boolean
 *
 *                   plantain:
 *                       type: boolean
 *
 *                   birch:
 *                       type: boolean
 *
 *                   chestnut:
 *                       type: boolean
 *
 *                   absinthe:
 *                       type: boolean
 *
 *       produces:
 *           application/json
 *       responses:
 *           '200':
 *               description: New prick test saved or modified
 *               examples:
 *                   application/json:
 *                       message: new prick test added / prick test modified
 *           '403':
 *               description: Error saving informations
 *               examples:
 *                   application/json:
 *                       message : patient doesn't exist / saving other patient's prick test is not allowed / error
 *
 *
 */
