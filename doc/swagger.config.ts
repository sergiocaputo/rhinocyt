export const swaggerJsDoc = require('swagger-jsdoc');
export const swaggerUi = require('swagger-ui-express');

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Rhinocyt API',
            description: 'Rhinocyt API documentation',
            contact: {
                name: 'Sergio Caputo',
                email: 's.caputo34@studenti.uniba.it',
            },
            server: ['http://localhost:3000'],
        }
    },

        apis: ['**/*.ts'],

};

export const swaggerDocs = swaggerJsDoc(swaggerOptions);