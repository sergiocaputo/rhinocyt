/**
 * @swagger
 * definitions:
 *   Exam:
 *        type: object
 *        properties:
 *            examId:
 *               type: number
 *
 *            examDate:
 *               type: string
 *
 *            diagnosis:
 *               type: string
 *
 *            doctorId:
 *               type: number
 *
 *            fiscalCode:
 *               type: string
 *
 * /exams:
 *   get:
 *       tags: ["Exam"]
 *       summary: Gets patient's exams informations using fiscalcode
 *       description: Returns patient's exams informations using fiscalcode if provided, otherwise get all the exams of the logged doctor's patients
 *       parameters:
 *         - name: fiscalcode
 *           in: query
 *           description: fiscalcode
 *           type: string
 *       responses:
 *           '200':
 *               description: Exam info returned
 *               schema:
 *                   "$ref": "#/definitions/Exam"
 *               examples:
 *                   application/json:
 *                    [
 *                    {
 *                        examId: 44,
 *                        examDate: 2020-05-25,
 *                        diagnosis: colera
 *                    }
 *                    ]
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : error
 *
 *
 *
 * /searchexams:
 *   get:
 *       tags: ["Exam"]
 *       summary: Gets patient's exams informations using name and surname
 *       description: Returns patient's exams informations using name and surname
 *       parameters:
 *         - name: name
 *           in: query
 *           description: name
 *           type: string
 *         - name: surname
 *           in: query
 *           description: surname
 *           type: string
 *       responses:
 *           '200':
 *               description: Exam info returned
 *               schema:
 *                   "$ref": "#/definitions/Exam"
 *               examples:
 *                   application/json:
 *                    [
 *                    {
 *                        examId: 44,
 *                        examDate: 2020-05-25,
 *                        diagnosis: colera,
 *                        fiscalCode: BNCMRC74A03H501L
 *                    }
 *                    ]
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : error
 *
 * /newexam:
 *   post:
 *       tags: ["Exam"]
 *       summary: Create a new exam
 *       parameters:
 *           - name: body
 *             in: body
 *             required: true
 *             description: Exam to save
 *             schema:
 *                 type: object
 *                 properties:
 *                     examDate:
 *                       type: string
 *
 *                     diagnosis:
 *                       type: string
 *
 *                     fiscalCode:
 *                       type: string
 *
 *       produces:
 *           application/json
 *       responses:
 *           '200':
 *               description: New exam saved
 *               examples:
 *                   application/json:
 *                       message: new exam added
 *           '403':
 *               description: Error saving informations
 *               examples:
 *                   application/json:
 *                       message : fiscal code not registered / is not allowed to save other patient's exams / wrong data sent / error
 *
 * /deleteexam:
 *   delete:
 *       tags: ["Exam"]
 *       summary: Deletes exam associated to examId
 *       parameters:
 *         - name: examid
 *           in: query
 *           required: true
 *           description: examId
 *           type: number
 *       responses:
 *           '200':
 *               description: Exam deleted
 *               examples:
 *                   application/json:
 *                       message : exam deleted
 *           '403':
 *               description: Error getting informations from others doctors' patients
 *               examples:
 *                   application/json:
 *                       message : exam doesn't exist / deleting other patient's exams is not allowed / error
 *
 */ 
