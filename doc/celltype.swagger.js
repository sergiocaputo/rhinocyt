/**
 * @swagger
 * definitions:
 *   CellType:
 *        type: object
 *        properties:
 *            cellTypeId:
 *                type: number
 *
 *            type:
 *                type: string
 *
 * /celltype:
 *   get:
 *       tags: ["Cell Type"]
 *       summary: Gets cell type informations using cellTypeId
 *       description: Returns cell type informations using cellTypeId if provided, otherwise gets all the cell types saved
 *       parameters:
 *         - name: celltypeid
 *           in: query
 *           description: cellTypeId
 *           type: number
 *       responses:
 *           '200':
 *               description: Cell type returned
 *               schema:
 *                   "$ref": "#/definitions/CellType"
 *               examples:
 *                   application/json:
 *                    [
 *                     {
 *                       cellTypeId: 1,
 *                       type: epithelium
 *                     }
 *                    ]
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : cell types not registered yet / cellTypeId not registered / error
 *
 * /newcelltype:
 *   post:
 *       tags: ["Cell Type"]
 *       summary: Saves new cell types
 *       parameters:
 *           - name: body
 *             in: body
 *             required: true
 *             description: "Cell types to save"
 *             schema:
 *               type: object
 *               properties:
 *                   type:
 *                       type: string
 *       produces:
 *           application/json
 *       responses:
 *           '200':
 *               description: Cell type saved
 *               examples:
 *                   application/json:
 *                       message : new cell type saved
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : cell type not provided / wrong data sent / cell type already saved / error
 * /deletecelltype:
 *   delete:
 *       tags: ["Cell Type"]
 *       summary: Deletes cell type associated to cellTypeId
 *       parameters:
 *         - name: celltypeid
 *           in: query
 *           required: true
 *           description: cellTypeId
 *           type: number
 *       responses:
 *           '200':
 *               description: Cell type deleted
 *               examples:
 *                   application/json:
 *                       message : celltype deleted
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : cell type not provided / celltype doesn't exist / error
 *
 */
