
/**
 * @swagger
 * definitions:
 *   Patient:
 *        type: object
 *        properties:
 *            fiscalCode:
 *               type: string
 *
 *            name:
 *               type: string
 *
 *            surname:
 *               type: string
 *
 *            gender:
 *               type: string
 *
 *            birthDate:
 *               type: string
 *
 *            anamnesisId:
 *               type: number
 *
 *            doctorId:
 *               type: number
 *
 *            symptomatologyId:
 *               type: number
 *
 *            prickTestId:
 *               type: number
 *
 *            medicalExamId:
 *               type: number
 *
 * /patients:
 *   get:
 *       tags: ["Patient"]
 *       summary: Gets patients informations of the logged doctor
 *       description: Returns patients'informations using doctorId
 *       responses:
 *           '200':
 *               description: Patients info returned
 *               schema:
 *                   "$ref": "#/definitions/Patient"
 *               examples:
 *                   application/json:
 *                    [
 *                    {
 *                        fiscalCode: RSSMRA80D11L328U,
 *                        name: Mario,
 *                        surname: Rossi
 *                    },
 *                    {
 *                        fiscalCode: BNCMRC74A03H501L,
 *                        name: Marco,
 *                        surname: Bianchi
 *                    },
 *                    ]
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : error
 *
 *
 *
 * /patientdetails:
 *   get:
 *       tags: ["Patient"]
 *       summary: Gets patient details using fiscal code
 *       description: Returns patient's informations using fiscalcode
 *       parameters:
 *         - name: fiscalcode
 *           in: query
 *           description: fiscalcode
 *           type: string
 *       responses:
 *           '200':
 *               description: Patient info returned
 *               schema:
 *                   "$ref": "#/definitions/Patient"
 *               examples:
 *                   application/json:
 *                    [
 *                     {
 *                       fiscalCode: BNCMRC74A03H501L,
 *                       name: Marco,
 *                       surname: Bianchi,
 *                       gender: m,
 *                       birthDate: 1974-01-03
 *                    }
 *                   ]
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : error
 *
 * /searchpatient:
 *   get:
 *       tags: ["Patient"]
 *       summary: Search patient using name and surname
 *       description: Returns patient's information using name and surname
 *       parameters:
 *         - name: name
 *           in: query
 *           description: name
 *           type: string
 *         - name: surname
 *           in: query
 *           description: surname
 *           type: string
 *       responses:
 *           '200':
 *               description: Patient info returned
 *               schema:
 *                   "$ref": "#/definitions/Patient"
 *               examples:
 *                   application/json:
 *                    [
 *                     {
 *                       fiscalCode: BNCMRC74A03H501L,
 *                       name: Marco,
 *                       surname: Bianchi,
 *                       gender: m,
 *                       birthDate: 1974-01-03
 *                    }
 *                   ]
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : error
 *
 * /newpatient:
 *   post:
 *       tags: ["Patient"]
 *       summary: Save a new patient
 *       parameters:
 *           - name: body
 *             in: body
 *             required: true
 *             description: Patient to save
 *             schema:
 *                 type: object
 *                 properties:
 *                   fiscalCode:
 *                       type: string
 *
 *                   name:
 *                       type: string
 *
 *                   surname:
 *                       type: string
 *
 *                   gender:
 *                       type: string
 *
 *                   birthDate:
 *                       type: string
 *       produces:
 *           application/json
 *       responses:
 *           '200':
 *               description: New patient saved
 *               examples:
 *                   application/json:
 *                       message: new patient added
 *           '403':
 *               description: Error saving informations
 *               examples:
 *                   application/json:
 *                       message : fiscal code lenght error / patient already registered / error
 *
 * /deletepatient:
 *   delete:
 *       tags: ["Patient"]
 *       summary: Deletes patient associated to fiscalcode
 *       parameters:
 *         - name: fiscalcode
 *           in: query
 *           required: true
 *           description: fiscalcode
 *           type: string
 *       responses:
 *           '200':
 *               description: Patient deleted or warning
 *               examples:
 *                   application/json:
 *                       message : patient deleted
 *           '403':
 *               description: Error getting informations from others doctors' patients
 *               examples:
 *                   application/json:
 *                       message : patient doesn't exist / deleting other doctors' patients is not allowed / error
 *
 */