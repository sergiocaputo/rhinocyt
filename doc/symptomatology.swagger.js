/**
 * @swagger
 * definitions:
 *   Symptomatology:
 *        type: object
 *        properties:
 *            symptomatologyId:
 *               type: number
 *
 *            obstruction:
 *               type: boolean
 *
 *            hypoacusis:
 *               type: boolean
 *
 *            rhinorrhea:
 *               type: boolean
 *
 *            sneezing:
 *               type: boolean
 *
 *            olfactoryProblems:
 *               type: boolean
 *
 *            auricularPadding:
 *               type: boolean
 *
 *            tinnitus:
 *               type: boolean
 *
 *            vertiginousSyndrome:
 *               type: boolean
 *
 *            doctorNotes:
 *               type: string
 *
 *            tearing:
 *               type: boolean
 *
 *            photophobia:
 *               type: boolean
 *
 *            conjunctiveItching:
 *               type: boolean
 *
 *            conjunctivaBurning:
 *               type: boolean
 *
 *            nasalItching:
 *               type: boolean
 *
 *            medicineUse:
 *               type: boolean
 *
 *            fever:
 *               type: boolean
 *
 *
 *
 * /symptomatology:
 *   get:
 *       tags: ["Symptomatology"]
 *       summary: Gets symptomatology informations using fiscalcode
 *       description: Returns symptomatology informations using fiscalcode
 *       parameters:
 *         - name: fiscalcode
 *           in: query
 *           description: fiscalcode
 *           type: string
 *       responses:
 *           '200':
 *               description: Symptomatology returned
 *               schema:
 *                   "$ref": "#/definitions/Symptomatology"
 *               examples:
 *                   application/json:
 *
 *                       [{
 *                           symptomatologyId: 1,
 *                           obstruction: 1,
 *                           hypoacusis: 0,
 *                           rhinorrhea: null,
 *                           sneezing: 2,
 *                           olfactoryProblems: null,
 *                           auricularPadding: 0,
 *                           tinnitus: null,
 *                           vertiginousSyndrome: null,
 *                           doctorNotes: null,
 *                           tearing: null,
 *                           photophobia: null,
 *                           conjunctiveItching: null,
 *                           conjunctivaBurning: null,
 *                           nasalItching: 1,
 *                           medicineUse: 1,
 *                           fever: null,
 *                           patients: [
 *                                           {
 *                                               name: Marco,
 *                                               surname: Bianchi
 *                                           }
 *                                       ]
 *                       }]
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : error
 *
 *
 *
 *
 * /newsymptomatology:
 *   post:
 *       tags: ["Symptomatology"]
 *       summary: Save or modify symptomatology
 *       parameters:
 *           - name: body
 *             in: body
 *             required: true
 *             description: Informations to save
 *             schema:
 *                 type: object
 *                 properties:
 *                   fiscalCode:
 *                       type: string
 *
 *                   obstruction:
 *                       type: boolean
 *
 *                   hypoacusis:
 *                       type: boolean
 *
 *                   rhinorrhea:
 *                       type: boolean
 *
 *                   sneezing:
 *                       type: boolean
 *
 *                   olfactoryProblems:
 *                       type: boolean
 *
 *                   auricularPadding:
 *                       type: boolean
 *
 *                   tinnitus:
 *                       type: boolean
 *
 *                   vertiginousSyndrome:
 *                       type: boolean
 *
 *                   doctorNotes:
 *                       type: string
 *
 *                   tearing:
 *                       type: boolean
 *
 *                   photophobia:
 *                       type: boolean
 *
 *                   conjunctiveItching:
 *                       type: boolean
 *
 *                   conjunctivaBurning:
 *                       type: boolean
 *
 *                   nasalItching:
 *                       type: boolean
 *
 *                   medicineUse:
 *                       type: boolean
 *
 *                   fever:
 *                       type: boolean
 *
 *
 *       produces:
 *           application/json
 *       responses:
 *           '200':
 *               description: New symptomatology saved or modified
 *               examples:
 *                   application/json:
 *                       message: new symptomatology added / symptomatology modified
 *           '403':
 *               description: Error saving informations
 *               examples:
 *                   application/json:
 *                       message : patient doesn't exist / saving other patient's symptomatology is not allowed / error
 *
 *
 */
