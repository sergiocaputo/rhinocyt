/**
 * @swagger
 * definitions:
 *   MedicalExam:
 *        type: object
 *        properties:
 *            medicalExamId:
 *               type: number
 *
 *            nasalPyramid:
 *               type: boolean
 *
 *            nasalValve:
 *               type: boolean
 *
 *            nasalSeptum:
 *               type: boolean
 *
 *            turbinates:
 *               type: boolean
 *
 *            nasalPolyposisSx:
 *               type: boolean
 *
 *            nasalPolyposisDx:
 *               type: boolean
 *
 *            exudate:
 *               type: boolean
 *
 *            adenoidHypertrophy:
 *               type: boolean
 *
 *            alterationNotes:
 *               type: string
 *
 *            earExam:
 *               type: string
 *
 *            rinoBaseSx:
 *               type: number
 *
 *            rinoBaseDx:
 *               type: number
 *
 *            rinoBaseSxDx:
 *               type: number
 *
 *            decongBaseSx:
 *               type: number
 *
 *            decongBaseDx:
 *               type: number
 *
 *            decongBaseSxDx:
 *               type: number
 *
 *            conclusions:
 *               type: string
 *
 * /medicalexam:
 *   get:
 *       tags: ["Medical Exam"]
 *       summary: Gets medical exam using fiscalcode
 *       description: Returns medical exam informations using examId
 *       parameters:
 *         - name: fiscalcode
 *           in: query
 *           description: fiscalcode
 *           type: strjng
 *       responses:
 *           '200':
 *               description: Medical exam returned
 *               schema:
 *                   "$ref": "#/definitions/MedicalExam"
 *               examples:
 *                   application/json:
 *                    [
 *                        {
 *                            medicalExamId: 4,
 *                            nasalPyramid: 0,
 *                            nasalValve: 0,
 *                            nasalSeptum: 1,
 *                            turbinates: null,
 *                            nasalPolyposisSx: null,
 *                            nasalPolyposisDx: null,
 *                            exudate: null,
 *                            adenoidHypertrophy: null,
 *                            alterationNotes: null,
 *                            earExam: null,
 *                            rinoBaseSx: null,
 *                            rinoBaseDx: null,
 *                            rinoBaseSxDx: 0.4,
 *                            decongBaseSx: 0.4,
 *                            decongBaseDx: null,
 *                            decongBaseSxDx: null,
 *                            conclusions: null,
 *                            patients: [
 *                            {
 *                                name: Marco,
 *                                surname: Bianchi
 *                            }
 *                            ]
 *                        }
 *                        ]
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : error
 *
 *
 *
 *
 * /newmedicalexam:
 *   post:
 *       tags: ["Medical Exam"]
 *       summary: Save or modify medical exam
 *       parameters:
 *           - name: body
 *             in: body
 *             required: true
 *             description: Informations to save
 *             schema:
 *                 type: object
 *                 properties:
 *                     fiscalCode:
 *                       type: string
 *
 *                     nasalPyramid:
 *                       type: boolean
 *
 *                     nasalValve:
 *                       type: boolean
 *
 *                     nasalSeptum:
 *                       type: boolean
 *
 *                     turbinates:
 *                       type: boolean
 *
 *                     nasalPolyposisSx:
 *                       type: boolean
 *
 *                     nasalPolyposisDx:
 *                       type: boolean
 *
 *                     exudate:
 *                       type: boolean
 *
 *                     adenoidHypertrophy:
 *                       type: boolean
 *
 *                     alterationNotes:
 *                       type: string
 *
 *                     earExam:
 *                       type: string
 *
 *                     rinoBaseSx:
 *                       type: number
 *
 *                     rinoBaseDx:
 *                       type: number
 *
 *                     rinoBaseSxDx:
 *                       type: number
 *
 *                     decongBaseSx:
 *                       type: number
 *
 *                     decongBaseDx:
 *                       type: number
 *
 *                     decongBaseSxDx:
 *                       type: number
 *
 *                     conclusions:
 *                       type: string
 *
 *
 *       produces:
 *           application/json
 *       responses:
 *           '200':
 *               description: New medical exam saved or modified
 *               examples:
 *                   application/json:
 *                       message: new medical exam added / medical exam modified
 *           '403':
 *               description: Error saving informations
 *               examples:
 *                   application/json:
 *                       message : patient doesn't exist / saving other patient's symptomatology is not allowed / error
 *
 *
 */

