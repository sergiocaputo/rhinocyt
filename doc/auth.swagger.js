/**
 * @swagger
 * definitions:
 *   Auth:
 *        type: object
 *        properties:
 *            twoFactorAuthenticationCode:
 *                type: string
 * /generate2FAcode:
 *   get:
 *       tags: ["Auth"]
 *       summary: Gets QR code where is saved the authentication code for Google Authenticator to verify 2FA
 *       description: Returns QR code
 *       produces:
 *           - text/html
 *       responses:
 *           '200':
 *               description: QR code returned
 *               examples:
 *                   text/html:
 *                       <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAC0CAYAAAA9zQYyAAAAAklEQVR4AewaftIAAAduSURBVO3BQY4cy5LAQDLR978yR0tfBZCoas1TfDezP1jrEg9rXeRhrYs8rHWRh7Uu8rDWRR7WusjDWhd5WOsiD2td5GGtizysdZGHtS7ysNZFHta6yMNaF/nhQyp/U8WkclIxqZxU/CaVNyomlaliUvlExaTyN1V84mGtizysdZGHtS7yw5dVfJPKGxVvVEwqJxXfVDGpTCpvVEwqJxVvVHyTyjc9rHWRh7Uu8rDWRX74ZSpvVHyTyicqJpWp4r9EZao4UZkq3lB5o+I3Pax1kYe1LvKw1kV++MdV/E0Vk8obFScVb6i8oTJV3ORhrYs8rHWRh7Uu8sM/TmWqmCreqPhExaQyqbxRMalMFScqJypTxb/sYa2LPKx1kYe1LvLDL6v4TRWTyknFicobFScVn1A5UZkqTiq+qeK/5GGtizysdZGHtS7yw5ep/E0qU8WkcqIyVUwqU8WkMlVMKlPFpDJVnFRMKp9QmSpOVP7LHta6yMNaF3lY6yL2BxdROamYVKaKE5WpYlJ5o2JS+aaKSeWk4l/2sNZFHta6yMNaF7E/+IDKVHGi8jdVTCpvVHyTylQxqUwVb6icVJyoTBUnKlPFpPJGxSce1rrIw1oXeVjrIvYHX6QyVZyoTBWfUHmj4hMqU8WkMlV8QmWqmFQ+UTGpnFRMKm9UfNPDWhd5WOsiD2tdxP7gAypTxaQyVUwqU8Wk8r+sYlI5qZhU3qg4UTmpmFSmikllqvjEw1oXeVjrIg9rXeSHD1WcVEwqb1ScqJxUTCpTxSdUpooTlZOKNypOKiaVT1ScqEwVk8pveljrIg9rXeRhrYv88GUqn1A5qXhD5Q2VNypOVKaKN1S+qWJSmVTeqJgqJpWpYlL5poe1LvKw1kUe1rrIDx9SOamYVKaKSWWqOKn4hMpJxaTyX6IyVUwqJxVvqEwqU8VUcVLxTQ9rXeRhrYs8rHWRHz5UcaLyCZWpYlKZKiaVNyreUJkqTlROKk4qJpWTikllUpkqPqHyRsU3Pax1kYe1LvKw1kXsDz6gMlWcqJxUnKhMFZPKGxUnKicV/zKVqeITKicVk8pU8YmHtS7ysNZFHta6yA8fqphUTireUJkqPlExqXyTyknFiconKiaV36TyX/Kw1kUe1rrIw1oX+eGXVUwqJxVTxYnKVHGiMlX8poq/SWWqeEPljYoTlZOKb3pY6yIPa13kYa2L2B98QGWqmFROKk5UpooTlU9UTCpTxaRyUjGpTBVvqHxTxRsqb1ScqEwVn3hY6yIPa13kYa2L/PDLKt5QmSomlanim1ROVE4qJpUTlaliUvlExaRyojJVnFRMKpPKScU3Pax1kYe1LvKw1kV++DKVqeITKicqn6j4hMpJxaQyVZxUfELlEypTxaQyVUwqf9PDWhd5WOsiD2td5IcPVUwqk8pUcVJxojJVvKEyqUwVn1CZKr5JZaqYVL6p4l/ysNZFHta6yMNaF7E/+CKVqeINlW+qOFE5qfhNKp+oOFE5qZhUTiomlaniROWk4hMPa13kYa2LPKx1kR8+pDJVTCpTxRsVJypvqLyhclJxonJScaJyovJGxUnFpPKGyhsV3/Sw1kUe1rrIw1oX+eHLVKaKSWWqmComlanipGJSmSomlW9SmSomlUllqjipmFSmihOVk4o3Kt5QmVSmik88rHWRh7Uu8rDWRewPPqByUnGiclIxqbxRMalMFZPKScWJyhsVv0llqphU3qiYVKaKSWWq+E0Pa13kYa2LPKx1kR/+MpWTikllqvhNFZPKpDJVvFHxhspU8QmVqWJSOVE5UZkqJpWTik88rHWRh7Uu8rDWRX74sopJ5Q2VqWJSOamYVE5U3qiYVKaKE5Wp4qTib6qYVKaKSeWNit/0sNZFHta6yMNaF/nhl1VMKlPFicpUcaIyVUwqb1RMKlPFpDJVfEJlqjhRmSomlUnlDZVPqJxUfOJhrYs8rHWRh7Uu8sOHKt6oeKPiROUTFScqU8U3qUwVn6j4RMUbKicqJxXf9LDWRR7WusjDWhf54UMqf1PFb1KZKj6hclLxhspU8UbFpHKiMlWcVEwqJypTxSce1rrIw1oXeVjrIj98WcU3qXxCZaqYVKaKE5WpYqp4Q2WqeEPlpGJSeaPiDZWpYlL5TQ9rXeRhrYs8rHWRH36ZyhsVn6iYVCaVE5WpYqqYVKaKSeWkYlI5qfhNKp+oOKn4TQ9rXeRhrYs8rHWRHy6jclLxTRUnFScqU8WkMql8U8WkclJxonJS8Zse1rrIw1oXeVjrIj/84yomlROVk4pPqEwVb6h8U8WJyknFpPIJlZOKTzysdZGHtS7ysNZFfvhlFX9TxaTyhsobFVPFGxWfUJkqJpVPqEwVJyr/nx7WusjDWhd5WOsiP3yZyt+kclJxojJVTCpTxaQyVZyonFScqLxRMalMFZPKVHGiMlVMKlPFpPJND2td5GGtizysdRH7g7Uu8bDWRR7WusjDWhd5WOsiD2td5GGtizysdZGHtS7ysNZFHta6yMNaF3lY6yIPa13kYa2LPKx1kf8DrNHLjILOdc4AAAAASUVORK5CYII=">
 *           '403':
 *               description: Error getting QR code
 *
 * /verify2FAcode:
 *   post:
 *       tags: ["Auth"]
 *       summary: Sets the user session if the authentication code provided is valid
 *       parameters:
 *           - name: body
 *             in: body
 *             required: true
 *             type: string
 *             schema:
 *                "$ref": "#/definitions/Auth"
 *       produces:
 *           application/json
 *       responses:
 *           '200':
 *               description: Session started
 *               examples:
 *                   application/json:
 *                       user: {
 *                           doctorId: 103,
 *                           name: best,
 *                           surname: doctor,
 *                           email: best@libero.it,
 *                           twoFactorAuthenticationCode: GAXVOKKHOYQXIIKREU7HMYZDHRNXCL2RHZ3UESZPJF5WE4SOKQXA,
 *                       }
 *           '403':
 *               description: Wrong authentication code sent
 *               examples:
 *                   application/json:
 *                       message: Invalid authentication code
 *
 */ 
