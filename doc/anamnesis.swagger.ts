/**
 * @swagger
 * definitions:
 *   FamilyAnamnesis:
 *        type: object
 *        properties:
 *            anamnesisId:
 *                type: number
 *
 *            parentsFoodAllergy:
 *                type: boolean
 *
 *            parentsInhalantAllergy:
 *                type: boolean
 *
 *            parentsPolyposis:
 *                type: boolean
 *
 *            siblingPolyposis:
 *                type: boolean
 *
 *            parentsAsthma:
 *                type: boolean
 *
 *            silibingAsthma:
 *                type: boolean
 *
 *            silibingFoodAllergy:
 *                type: boolean
 *
 *            silibingInhalantAllergy:
 *                type: boolean
 *
 *            familyMedicalNotes:
 *                type: string
 *
 * /familyanamnesis:
 *   get:
 *       tags: ["Family Anamnesis"]
 *       summary: Gets family anamnesis using fiscalcode
 *       description: Returns family anamnesis informations
 *       parameters:
 *         - name: fiscalcode
 *           in: query
 *           required: true
 *           description: fiscalcode of the patient
 *           type: string
 *       responses:
 *           '200':
 *               description: Family anamnesis returned
 *               schema:
 *                   $ref": "#/definitions/FamilyAnamnesis"
 *               examples:
 *                   application/json:
 *                        [{
 *                           anamnesisId: 2,
 *                           parentsFoodAllergy: 1,
 *                           parentsInhalantAllergy: 1,
 *                           parentsPolyposis: 1,
 *                           siblingPolyposis: null,
 *                           parentsAsthma: null,
 *                           silibingAsthma: null,
 *                           silibingFoodAllergy: null,
 *                           silibingInhalantAllergy: null,
 *                           familyMedicalNotes: null,
 *                           patients: [{
 *                               name: Marco,
 *                               surname: Bianchi
 *                            }]
 *                         }]
 *           '403':
 *               description: Error getting informations
 *               schema:
 *                   $ref": "#/definitions/FamilyAnamnesis"
 *               examples:
 *                   application/json:
 *                       message: error
 *
 * /newfamilyanamnesis:
 *   post:
 *       tags: ["Family Anamnesis"]
 *       summary: Saves a new family anamnesis or modify it if already saved
 *       parameters:
 *           - name: body
 *             in: body
 *             required: true
 *             type: string
 *             description: "Family Anamnesis to create"
 *             schema:
 *               type: object
 *               properties:
 *                   fiscalCode:
 *                       type: string
 *
 *                   parentsFoodAllergy:
 *                       type: boolean
 *
 *                   parentsInhalantAllergy:
 *                       type: boolean
 *
 *                   parentsPolyposis:
 *                       type: boolean
 *
 *                   siblingPolyposis:
 *                       type: boolean
 *
 *                   parentsAsthma:
 *                       type: boolean
 *
 *                   silibingAsthma:
 *                       type: boolean
 *
 *                   silibingFoodAllergy:
 *                       type: boolean
 *
 *                   silibingInhalantAllergy:
 *                       type: boolean
 *
 *                   familyMedicalNotes:
 *                       type: string
 *
 *       produces:
 *           application/json
 *       responses:
 *           '200':
 *               description: Family anamnesis saved or modified
 *               schema:
 *                   $ref": "#/definitions/FamilyAnamnesis"
 *               examples:
 *                   application/json:
 *                       message: new family anamnesis added / family anamnesis modified
 *           '403':
 *               description: Error saving informations
 *               schema:
 *                   $ref": "#/definitions/FamilyAnamnesis"
 *               examples:
 *                   application/json:
 *                       message: error / patient doesn't exist / saving other patient's anamnesis is not allowed
 *
 */
