/**
 * @swagger
 * definitions:
 *   Cells:
 *        type: object
 *        properties:
 *            cellId:
 *                type: number
 *
 *            cellPhoto:
 *                type: array
 *                items:
 *                   type: string
 *                   format: binary
 *
 *            cellTypeId:
 *                type: array
 *                items:
 *                   type: number
 *
 *            fieldId:
 *                type: number
 *
 * /cells:
 *   get:
 *       tags: ["Cells"]
 *       summary: Gets cells associated to fieldId
 *       description: Returns cells using fieldId
 *       parameters:
 *         - name: fieldid
 *           in: query
 *           required: true
 *           description: fieldId
 *           type: number
 *       responses:
 *           '200':
 *               description: Cells returned
 *               schema:
 *                   "$ref": "#/definitions/Cells"
 *               examples:
 *                   application/json:
 *                    [
 *                      {
 *                          "cellId": 140,
 *                          "cellPhoto": "./src/services/cell_extraction/cells/cell_examid_7/neutrophil/Img_0_cell5_fieldid_105.png",
 *                          "cellType": {
 *                              "type": "neutrophil"
 *                          }
 *                      },
 *                      {
 *                          "cellId": 126,
 *                          "cellPhoto": "./src/services/cell_extraction/cells/cell_examid_7/eosinophil/Img_0_cell1_fieldid_105.png",
 *                          "cellType": {
 *                              "type": "eosinophil"
 *                          }
 *                      },
 *                      {
 *                          "cellId": 127,
 *                          "cellPhoto": "./src/services/cell_extraction/cells/cell_examid_7/eosinophil/Img_0_cell2_fieldid_105.png",
 *                          "cellType": {
 *                          "type": "eosinophil"
 *                      }
 *                      },
 *                      {
 *                          "cellId": 128,
 *                          "cellPhoto": "./src/services/cell_extraction/cells/cell_examid_7/eosinophil/Img_0_cell3_fieldid_105.png",
 *                          "cellType": {
 *                          "type": "eosinophil"
 *                      }
 *                      },
 *                    ]
 *
 *           '403':
 *               description: Error getting informations from others doctors' patients
 *               schema:
 *                  "$ref": "#/definitions/Cells"
 *               examples:
 *                  application/json:
 *                      message : is not allowed to view other patient's field cells / field not registered / fieldid not provided / error
 *
 * /cellstable:
 *   get:
 *       tags: ["Cells"]
 *       summary: Counts cells associated to fieldId for each cell type
 *       description: Returns cells' countings
 *       parameters:
 *         - name: examid
 *           in: query
 *           required: true
 *           description: examId
 *           type: number
 *       responses:
 *           '200':
 *               description: Cells' countings for type
 *               schema:
 *                   Results:
 *                       type: object
 *                       properties:
 *                           celltype_type:
 *                               type: string
 *
 *                           cellNumber:
 *                               type: number
 *               examples:
 *                   application/json:
 *                       {
 *                           "results": [
 *                               {
 *                               "cells_field_id": 118,
 *                               "celltype_type": "eosinophil",
 *                               "cellNumber": "4"
 *                               },
 *                               {
 *                               "cells_field_id": 118,
 *                               "celltype_type": "epithelium",
 *                               "cellNumber": "1"
 *                               },
 *                               {
 *                               "cells_field_id": 118,
 *                               "celltype_type": "neutrophil",
 *                               "cellNumber": "2"
 *                              },
 *                               {
 *                               "cells_field_id": 118,
 *                               "celltype_type": "other",
 *                               "cellNumber": "2"
 *                               }
 *                           ]
 *                           }
 *           '403':
 *               description: Error getting informations from others doctors' patients
 *               schema:
 *                   "$ref": "#/definitions/Cells"
 *               examples:
 *                   application/json:
 *                       message : field not registered / is not allowed to view other patient's field cells / fieldid not provided / error
 *
 * /deletecell:
 *   delete:
 *       tags: ["Cells"]
 *       summary: Deletes cell associated to cellId
 *       parameters:
 *         - name: cellid
 *           in: query
 *           required: true
 *           description: cellId
 *           type: number
 *       responses:
 *           '200':
 *               description: Cell deleted
 *               examples:
 *                   application/json:
 *                       message : cell deleted
 *           '403':
 *               description: Error getting informations from others doctors' patients
 *               examples:
 *                   application/json:
 *                       message : cell provided doesn't exist / deleting other patient's cells is not allowed / error
 *
 *
 * /changecelltype:
 *   put:
 *       tags: ["Cells"]
 *       summary: Change cell type associated to cellId
 *       parameters:
 *         - name: body
 *           in: body
 *           required: true
 *           description: "New celltype for cellId to save"
 *           schema:
 *              type: object
 *              properties:
 *                  cellId:
 *                      type: number
 *                  cellType:
 *                      type: string
 *       responses:
 *           '200':
 *               description: Cell type updated
 *               examples:
 *                   application/json:
 *                       message : cell type updated
 *           '403':
 *               description: Error getting informations from others doctors' patients
 *               examples:
 *                   application/json:
 *                       message : cell provided doesn't exist / changing other patient's cells type is not allowed / celltype given doesn't exist / error
 *
 */
