/**
 * @swagger
 * definitions:
 *   Diagnosis:
 *        type: object
 *        properties:
 *           examId:
 *                type: number
 *
 *           examDate:
 *                type: string
 *
 *           diagnosis:
 *                type: string
 *
 *           fieldId:
 *                type: number
 *
 *           doctorId:
 *                type: number
 *
 *           fiscalCode:
 *                type: string
 *
 * /diagnosis:
 *   get:
 *       tags: ["Diagnosis"]
 *       summary : Gets diagnosis associated to fiscalcode and exam date
 *       description: Returns diagnosis using fiscalcode and exam date
 *       parameters:
 *         - name: fiscalcode
 *           in: query
 *           required: true
 *           description: fiscal Code
 *           type: string
 *         - name: examdate
 *           in: query
 *           required: true
 *           description: date of the exam
 *           type: string
 *       responses:
 *           '200':
 *               description: Diagnosis returned
 *               schema:
 *                   "$ref": "#/definitions/Diagnosis"
 *               examples:
 *                   application/json:
 *                       [{
 *                           examId: 44,
 *                           fiscalCode: BNCMRC74A03H501L,
 *                           examDate: 2020-05-25,
 *                           diagnosis: colera
 *                       }]
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : error
 * /newdiagnosis:
 *   put:
 *       tags: ["Diagnosis"]
 *       summary: Saves new diagnosis for an existing exam or updates diagnosis if already exists
 *       parameters:
 *           - name: body
 *             in: body
 *             required: true
 *             description: "Diagnosis to save associated to examId"
 *             schema:
 *               type: object
 *               properties:
 *                   examId:
 *                       type: number
 *
 *                   diagnosis:
 *                       type: string
 *
 *       produces:
 *           application/json
 *       responses:
 *           '200':
 *               description: Diagnosis saved
 *               examples:
 *                   application/json:
 *                       message : new diagnosis added
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : exam doesn't exist / saving other patient's diagnosis is not allowed / error
 * /deletediagnosis:
 *   put:
 *       tags: ["Diagnosis"]
 *       summary: Deletes diagnosis associated to examId
 *       parameters:
 *         - name: examid
 *           in: query
 *           required: true
 *           description: examId
 *           type: number
 *       responses:
 *           '200':
 *               description: Diagnosis deleted
 *               examples:
 *                   application/json:
 *                       message : diagnosis deleted
 *           '403':
 *               description: Error getting informations from others doctors' patients
 *               examples:
 *                   application/json:
 *                       message : deleting other patient's diagnosis is not allowed / exam doesn't exist  / error
 *
 * /downloadreport:
 *   get:
 *       tags: ["Diagnosis"]
 *       summary : Download diagnosis associated to fiscalcode and exam date
 *       description: Returns diagnosis using fiscalcode and exam date
 *       parameters:
 *         - name: fiscalcode
 *           in: query
 *           required: true
 *           description: fiscal Code
 *           type: string
 *         - name: examdate
 *           in: query
 *           required: true
 *           description: date of the exam
 *           type: string
 *       responses:
 *           '200':
 *               description: Diagnosis PDF returned
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : error
 *
 * /generatediagnosis:
 *   get:
 *       tags: ["Diagnosis"]
 *       summary : Generate a new diagnosis associated to examId
 *       description: Returns diagnosis using examId
 *       parameters:
 *         - name: examid
 *           in: query
 *           required: true
 *           description: examId
 *           type: string
 *       responses:
 *           '200':
 *               description: Diagnosis generated returned
 *               examples:
 *                  application/json:
 *                      {
 *                           "message ": "diagnosis generated",
 *                           "generated_possible_diagnosis": {
 *                            "rinosinusite": [
 *                              "Grado di  Eosinophil: 0",
 *                              "Grado di  Mastocyte: 0",
 *                              "Grado di Neutrophil: 1",
 *                            "Prurito nasale",
 *                            "Prurito occhio",
 *                            "Ostruzione nasale sinistra",
 *                            "Rinorrea nasale sieroso",
 *                            "Febbre"
 *                            ],
 *                            "rinite micotica": [
 *                            "Grado di  Eosinophil: 0",
 *                            "Grado di  Mastocyte: 0",
 *                            "Grado di Neutrophil: 1",
 *                            "Ostruzione nasale sinistra",
 *                            "Rinorrea nasale sieroso"
 *                            ],
 *                            "rinite medicamentosa": [
 *                            "Grado di  Eosinophil: 0",
 *                            "Grado di  Mastocyte: 0",
 *                            "Grado di  Neutrophil: 0-1",
 *                            "Ostruzione nasale sinistra",
 *                            "Uso eccessivo di farmaci"
 *                            ],
 *                            "NARNE": [
 *                            "Grado di Neutrophil: 1",
 *                            "Ostruzione nasale sinistra"
 *                            ],
 *                            "rinite irritativa": [
 *                            "Grado di  Eosinophil: 0",
 *                            "Grado di  Mastocyte: 0",
 *                            "Grado di Neutrophil: 1",
 *                            "Ostruzione nasale sinistra"
 *                            ],
 *                            "poliposi antrocoanale": [
 *                            "Grado di Eosinophil: 0",
 *                            "Grado di Mastocyte: 0",
 *                            "Grado di Neutrophil: 1",
 *                            "Ostruzione nasale sinistra"
 *                            ],
 *                            "condizione normale": [
 *                            "Grado di  Eosinophil: 0",
 *                            "Grado di  Mastocyte: 0",
 *                            "Grado di  Neutrophil: 0-1"
 *                            ]
 *                           }
 *                          }
 *           '403':
 *               description: Error getting informations
 *               examples:
 *                   application/json:
 *                       message : cells not extracted from fields yet / exam doesn't have fields / error
 *
 *
 *
 */
