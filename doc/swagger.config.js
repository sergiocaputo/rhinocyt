"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.swaggerDocs = exports.swaggerUi = exports.swaggerJsDoc = void 0;
exports.swaggerJsDoc = require('swagger-jsdoc');
exports.swaggerUi = require('swagger-ui-express');
var swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Rhinocyt API',
            description: 'Rhinocyt API documentation',
            contact: {
                name: 'Sergio Caputo',
                email: 's.caputo34@studenti.uniba.it',
            },
            server: ['http://localhost:3000'],
        }
    },
    apis: ['**/*.ts'],
};
exports.swaggerDocs = exports.swaggerJsDoc(swaggerOptions);
